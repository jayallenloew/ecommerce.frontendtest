package fieldvalidations.contactinfopage;

import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import common.FieldValidationUtilities;
import common.ProgressiveLeasingConstants;
import pages.ContactInfoPage;
import utilities.saucelabs.TestBaseSauce;
/**
 * Field validation test: Contact Info page: city values.
 * <p>
 * In this test class: negative scenarios, static setup.
 * <p>
 * Full coverage for the TestLink case below:
 * <p>
 * http://slc-qaswebtli01/testlink/linkto.php?tprojectPrefix=G&item=testcase&id=G-3858
 * <p>
 * Version 1.0 2019-02-28
 * <p>
 * Version 2.0 2019-04-03:
 * <ul>
 * <li>Remove deprecated setup code.</li>
 * <li>Default to Sauce drivers.</li>
 * <li>Update changed elements.</li>
 * <li>Enhance performance.</li>
 * <li>Re-test.</li>
 * </ul>
 * Version 2.1 2019-04-19 abstract out common behavior and tune for performance
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 2.1
 * @see ContactInfoPage
 * @see G_3858_City_A
 * @see G_3858_City_C
 */
public class G_3858_City_B extends TestBaseSauce {

	private WebDriver driver;

	private ContactInfoPage pageUnderTest;
		
	private static final String NOT_BEGIN_SPACE		= "Your home city can't begin with a space.";
	private static final String NOT_NUMBERS			= "Your home city cannot contain any numbers.";
	private static final String NOT_ILLEGAL_CHARS	= "Your home city cannot contain special characters other than dash ( - ), period ( . ), or space.";
	

	/**
	 * Ensure that city name cannot contain any number.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testNotContainsNumbers(Method method) {
		
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
						
		driver = getPreparedDriver(testNameLocal);
		
		driver.navigate().to(FV_ContactInfoTests.getStartingURL());
		
		FV_ContactInfoTests.commonWaitInSetup(driver);
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		jse = null;

		pageUnderTest = new ContactInfoPage(driver);

		pageUnderTest.getFieldAddressLine1().sendKeys(ProgressiveLeasingConstants.ADDRESS_DRAPER_HQ[0]);
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldCity().sendKeys("Dr9per"); // illegal: contains number
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldState().sendKeys(ProgressiveLeasingConstants.ADDRESS_DRAPER_HQ[2]);
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldZIP().sendKeys(ProgressiveLeasingConstants.ADDRESS_DRAPER_HQ[3]);
		
		pageUnderTest.acceptSpam();
		
		WebElement elementErrorTemp = null;
		
		try { 
			elementErrorTemp = driver.findElement(By.xpath("//pg-address-wrapper/div[2]/span[8]"));
		}catch(NoSuchElementException nSEE) { 
			throw new AssertionError(testNameLocal, nSEE);
		}
		
		String captured = elementErrorTemp.getText();

		try { 
			Assert.assertTrue(captured.contains(G_3858_City_B.NOT_NUMBERS)); 
			System.out.println("\n" + testNameLocal + ":\tPASS");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			System.out.println(testNameLocal + ":\texpected \'" + G_3858_City_B.NOT_NUMBERS + "\'");
			System.out.println(testNameLocal + ":\tcaptured \'" + captured + "\'");
			throw aE;
		}finally { 
			FieldValidationUtilities.refreshBetweenTest(testNameLocal, driver);
			captured = null;
			elementErrorTemp = null;
			testNameLocal = null;
			pageUnderTest = null;
		}		
	}

	
	
	/**
	 * Ensure that city name cannot contain some illegal character.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testNotIllegalChars(Method method) {

		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		
		driver = getPreparedDriver(testNameLocal);
		
		driver.navigate().to(FV_ContactInfoTests.getStartingURL());
		
		FV_ContactInfoTests.commonWaitInSetup(driver);
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		jse = null;

		pageUnderTest = new ContactInfoPage(driver);

		jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		jse = null;

		pageUnderTest.getFieldAddressLine1().sendKeys(ProgressiveLeasingConstants.ADDRESS_DRAPER_HQ[0]);
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		pageUnderTest.getFieldCity().sendKeys("Salt Lake C!ty"); // contains illegal character: exclamation point
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		pageUnderTest.getFieldState().sendKeys(ProgressiveLeasingConstants.ADDRESS_DRAPER_HQ[2]);
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		pageUnderTest.getFieldZIP().sendKeys(ProgressiveLeasingConstants.ADDRESS_DRAPER_HQ[3]);
		
		pageUnderTest.acceptSpam();

		WebElement elementErrorTemp = null;
		
		try { 
			elementErrorTemp = driver.findElement(By.cssSelector(".pg-address-error:nth-child(9)"));
		}catch(NoSuchElementException nSEE) { 
			throw new AssertionError(method.getName(), nSEE);
		}
		
		String captured = elementErrorTemp.getText();

		try { 
			Assert.assertTrue(captured.contains(G_3858_City_B.NOT_ILLEGAL_CHARS)); 
			System.out.println("\n" + testNameLocal + ":\tPASS");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			System.out.println(testNameLocal + ":\texpected \'" + G_3858_City_B.NOT_ILLEGAL_CHARS + "\'");
			System.out.println(testNameLocal + ":\tcaptured \'" + captured + "\'");
			throw aE;
		}finally { 
			FieldValidationUtilities.refreshBetweenTest(testNameLocal, driver);
			captured = null;
			elementErrorTemp = null;
			testNameLocal = null;
			pageUnderTest = null;
		}		
	}
	
	


	
	/**
	 * Ensure that city name cannot begin with a space character.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testNotBeginSpace(Method method) {

		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		
		driver = getPreparedDriver(testNameLocal);
		
		driver.navigate().to(FV_ContactInfoTests.getStartingURL());
		
		FV_ContactInfoTests.commonWaitInSetup(driver);
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		jse = null;

		pageUnderTest = new ContactInfoPage(driver);

		jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		jse = null;

		pageUnderTest.getFieldAddressLine1().sendKeys(ProgressiveLeasingConstants.ADDRESS_DRAPER_HQ[0]);
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldCity().sendKeys(" Draper"); // begins with a space
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldState().sendKeys(ProgressiveLeasingConstants.ADDRESS_DRAPER_HQ[2]);
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldZIP().sendKeys(ProgressiveLeasingConstants.ADDRESS_DRAPER_HQ[3]);

		pageUnderTest.acceptSpam();
		
		WebElement elementErrorTemp = null;
		
		try { 
			elementErrorTemp = driver.findElement(By.xpath("//span[10]"));
		}catch(NoSuchElementException nSEE) { 
			throw new AssertionError(testNameLocal, nSEE);
		}
		
		String captured = elementErrorTemp.getText();

		try { 
			Assert.assertTrue(captured.contains(G_3858_City_B.NOT_BEGIN_SPACE)); 
			System.out.println("\n" + testNameLocal + ":\tPASS");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			System.out.println(testNameLocal + ":\texpected \'" + G_3858_City_B.NOT_BEGIN_SPACE + "\'");
			System.out.println(testNameLocal + ":\tcaptured \'" + captured + "\'");
			throw aE;
		}finally { 
			FieldValidationUtilities.refreshBetweenTest(testNameLocal, driver);
			captured = null;
			elementErrorTemp = null;
			testNameLocal = null;
			pageUnderTest = null;
		}		
	}
	
	/**
	 * If subclass of TestBaseSauce, pass the test result up to the Sauce Labs Dashboard.
	 * <p>
	 * Quit driver.
	 * <p>
	 * Delete the cookie this test just created.
	 */
	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {
		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		if(this.getClass().getSuperclass().getSimpleName().equals("TestBaseSauce")) { 
			System.out.println(methodNameLocal + ":\tSauce driver type detected; Sauce-specific teardown up next...");
			((JavascriptExecutor) driver).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
		} else { 
			System.out.println(methodNameLocal + ":\tLocal driver type detected; Sauce-specific teardown skipped intentionally.\n");
		}
		driver.quit();
		System.out.println(methodNameLocal + ":\tEND tearDown.");
	}

}
