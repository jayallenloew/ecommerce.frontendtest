package fieldvalidations.contactinfopage;

import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import common.FieldValidationUtilities;
import pages.ContactInfoPage;
import utilities.saucelabs.DriverTypeSauce;
import utilities.saucelabs.TestBaseSauce;
import waiter.Waiter;

/**
 * Field validation test: Contact Info page: email values.
 * <p>
 * Postive and negative cases.
 * <p>
 * Full coverage for the TestLink case below:
 * <p>
 * http://slc-qaswebtli01/testlink/linkto.php?tprojectPrefix=G&item=testcase&id=G-4115
 * <p>
 * Version 1.0 2019-01-23
 * <p>
 * Version 1.1 2019-04-19 abstract out common behavior and tune for performance
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.1
 * @see ContactInfoPage
 */
public class G_4115_Email_D extends TestBaseSauce {

	private WebDriver driver;

	/**
	 * Expected text after an illegal email address is entered: missing ampersand.
	 * <p>
	 * @return Expected text of a field label.
	 */
	public static final String getHintEmailMissingAmpersand() { 
		return "Your email address is missing the @ symbol.";
	}

	/**
	 * Expected text after an illegal email address is entered: multiple ampersands.
	 * <p>
	 * @return Expected text of a field label.
	 */
	public static final String getHintEmailContainsMultipleAmpersands() { 
		return "Your email address can only have one at ( @ ) symbol.";
	}
	


	/**
	 * Test the hint text for the email field as expected 
	 * after a missing ampersand. Negative case.
	 * <p>
	 */
	@Test(enabled=true)
	public void testEmailMissingAmpersand(Method method) {
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		setDriverTypeForTestClass(DriverTypeSauce.WINDOWS_CHROME);
		driver = getPreparedDriver(testNameLocal);
		Waiter waiter = new Waiter();
		waiter.get(FV_ContactInfoTests.getStartingURL(), driver, 60);
		StringBuilder sBuild = new StringBuilder(driver.getPageSource());
		try { 
			Assert.assertTrue(sBuild.toString().contains("How can we contact you"));
		}catch(AssertionError aE) { 
			System.out.println(testNameLocal + ":\tfail-fast; check contact info page");
			throw aE;
		}finally {
			sBuild.delete(0, sBuild.length());
			sBuild = null;
		}
		ContactInfoPage pageUnderTest = new ContactInfoPage(driver);
		pageUnderTest.getFieldEmail_address().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		String emailToUse = "testtest.com"; // illegal
		pageUnderTest.getFieldEmail_address().sendKeys(emailToUse);
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldAreaCode().click(); // click-out
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		WebElement temp = driver.findElement(By.xpath("//pg-email-address[@id='email']/div/pg-field/div[2]/span[3]"));
		Assert.assertNotNull(temp); // fast-fail
		String captured = temp.getText().trim();
		String expected = G_4115_Email_D.getHintEmailMissingAmpersand();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		try { 
			Assert.assertEquals(captured, expected);
			System.out.println("\n" + testNameLocal + ":\tPASS");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			System.out.println(testNameLocal + ":\tExpected '" + expected + "\'");
			System.out.println(testNameLocal + ":\tCaptured '" + captured + "\'");
			throw aE;
		}finally { 
			captured = null;
			expected = null;
			emailToUse = null;
			testNameLocal = null;
			pageUnderTest = null;
			waiter = null;
		}
	}



	/**
	 * Test the hint text for the email field as expected after an
	 * address containing more than one ampersand is entered. 
	 * Negative case.
	 * <p>
	 */
	@Test(enabled=true)
	public void testEmailContainsMultipleAmpersands(Method method) {
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		driver = getPreparedDriver(testNameLocal);
		Waiter waiter = new Waiter();
		waiter.get(FV_ContactInfoTests.getStartingURL(), driver, 60);
		StringBuilder sBuild = new StringBuilder(driver.getPageSource());
		try { 
			Assert.assertTrue(sBuild.toString().contains("How can we contact you"));
		}catch(AssertionError aE) { 
			System.out.println(testNameLocal + ":\tfail-fast; check contact info page");
			throw aE;
		}finally {
			sBuild.delete(0, sBuild.length());
			sBuild = null;
		}
		ContactInfoPage pageUnderTest = new ContactInfoPage(driver);
		pageUnderTest.getFieldEmail_address().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		String emailToUse = "test@@test.com"; // illegal
		pageUnderTest.getFieldEmail_address().sendKeys(emailToUse);
		FieldValidationUtilities.sleepSecond(testNameLocal);
		pageUnderTest.getFieldAreaCode().click(); // click-out
		FieldValidationUtilities.sleepSecond(testNameLocal);
		WebElement temp = driver.findElement(By.xpath("//pg-email-address[@id='email']/div/pg-field/div[2]/span[6]"));
		Assert.assertNotNull(temp); // fast-fail
		String captured = temp.getText().trim();
		String expected = G_4115_Email_D.getHintEmailContainsMultipleAmpersands();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		try { 
			Assert.assertEquals(captured, expected);
			System.out.println("\n" + testNameLocal + ":\tPASS");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			System.out.println(testNameLocal + ":\tExpected '" + expected + "\'");
			System.out.println(testNameLocal + ":\tCaptured '" + captured + "\'");
			throw aE;
		}finally { 
			captured = null;
			expected = null;
			emailToUse = null;
			testNameLocal = null;
			pageUnderTest = null;
			waiter = null;
		}
	}	


	/**
	 * If subclass of TestBaseSauce, pass the test result up to the Sauce Labs Dashboard.
	 * <p>
	 * Quit driver.
	 * <p>
	 * Delete the cookie this test just created.
	 */
	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {
		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		if(this.getClass().getSuperclass().getSimpleName().equals("TestBaseSauce")) { 
			System.out.println(methodNameLocal + ":\tSauce driver type detected; Sauce-specific teardown up next...");
			((JavascriptExecutor) driver).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
		} else { 
			System.out.println(methodNameLocal + ":\tLocal driver type detected; Sauce-specific teardown skipped intentionally.\n");
		}
		driver.quit();
		System.out.println(methodNameLocal + ":\tEND tearDown.");
	}

}
