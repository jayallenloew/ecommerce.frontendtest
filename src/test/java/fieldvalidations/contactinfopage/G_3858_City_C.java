package fieldvalidations.contactinfopage;

import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import common.FieldValidationUtilities;
import common.ProgressiveLeasingConstants;
import pages.ContactInfoPage;
import utilities.saucelabs.TestBaseSauce;

/**
 * Field validation test: Contact Info page: city values.
 * <p>
 * In this test class: negative scenarios, static setup.
 * <p>
 * Full coverage for the TestLink case below:
 * <p>
 * http://slc-qaswebtli01/testlink/linkto.php?tprojectPrefix=G&item=testcase&id=G-3858
 * <p>
 * Version 1.0 2019-02-28
 * <p>
 * Version 2.0 2019-04-03:
 * <ul>
 * <li>Remove deprecated setup code.</li>
 * <li>Default to Sauce drivers.</li>
 * <li>Update changed elements.</li>
 * <li>Enhance performance.</li>
 * <li>Re-test.</li>
 * </ul>
 * Version 2.1 2019-04-19 abstract out common behavior and tune for performance
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 2.1
 * @see ContactInfoPage
 * @see G_3858_City_A
 * @see G_3858_City_B
 */
public class G_3858_City_C extends TestBaseSauce {

	private WebDriver driver;

	private ContactInfoPage pageUnderTest;
		
	private static final String NOT_LEFT_BLANK = "Your home city is required to process the application.";
	
	
	/**
	 * Ensure that city name cannot be null.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testNotNull(Method method) {
		
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		
		driver = getPreparedDriver(testNameLocal);
		
		driver.navigate().to(FV_ContactInfoTests.getStartingURL());

		FV_ContactInfoTests.commonWaitInSetup(driver);
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		jse = null;

		pageUnderTest = new ContactInfoPage(driver);

		pageUnderTest.getFieldAddressLine1().sendKeys(ProgressiveLeasingConstants.ADDRESS_DRAPER_HQ[0]);
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldCity().clear();
		
		// DRAPER_HQ_ADDRESS[1] skipped intentionally
		
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldState().sendKeys(ProgressiveLeasingConstants.ADDRESS_DRAPER_HQ[2]);
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldZIP().sendKeys(ProgressiveLeasingConstants.ADDRESS_DRAPER_HQ[3]);
		
		FieldValidationUtilities.tabOut(testNameLocal, driver);

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		
		WebElement elementErrorTemp = null;
		
		try { 
			elementErrorTemp = driver.findElement(By.cssSelector(".pg-address-error:nth-child(7)"));
		}catch(NoSuchElementException nSEE) { 
			throw new AssertionError(testNameLocal, nSEE);
		}
		
		String captured = elementErrorTemp.getText();

		try { 
			Assert.assertTrue(captured.contains(G_3858_City_C.NOT_LEFT_BLANK)); 
			System.out.println("\n" + testNameLocal + ":\t\tPASS");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			System.out.println(testNameLocal + ":\texpected \'" + G_3858_City_C.NOT_LEFT_BLANK + "\'");
			System.out.println(testNameLocal + ":\tcaptured \'" + captured + "\'");
			throw aE;
		}finally { 
			captured = null;
			elementErrorTemp = null;
			testNameLocal = null;
			pageUnderTest = null;
		}		
	}

	

	/**
	 * If subclass of TestBaseSauce, pass the test result up to the Sauce Labs Dashboard.
	 * <p>
	 * Quit driver.
	 * <p>
	 * Delete the cookie this test just created.
	 */
	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {
		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		if(this.getClass().getSuperclass().getSimpleName().equals("TestBaseSauce")) { 
			System.out.println(methodNameLocal + ":\tSauce driver type detected; Sauce-specific teardown up next...");
			((JavascriptExecutor) driver).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
		} else { 
			System.out.println(methodNameLocal + ":\tLocal driver type detected; Sauce-specific teardown skipped intentionally.\n");
		}
		driver.quit();
		System.out.println(methodNameLocal + ":\tEND tearDown.");
	}

}
