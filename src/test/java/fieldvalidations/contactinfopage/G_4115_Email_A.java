package fieldvalidations.contactinfopage;

import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import common.FieldValidationUtilities;
import pages.ContactInfoPage;
import utilities.saucelabs.TestBaseSauce;
import waiter.Waiter;

/**
 * Field validation test: Contact Info page: email values.
 * <p>
 * Full coverage for the TestLink case below:
 * <p>
 * http://slc-qaswebtli01/testlink/linkto.php?tprojectPrefix=G&item=testcase&id=G-4115
 * <p>
 * Version 1.0 2019-01-23
 * <p>
 * Version 1.1 2019-04-19 abstract out common behavior and tune for performance
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.1
 * @see ContactInfoPage
 */
public class G_4115_Email_A extends TestBaseSauce {

	private WebDriver driver;

	/**
	 * Expected text after a valid email address is entered, or 
	 * after a simple click into the email address field.
	 * <p>
	 * @return Expected text of a field label.
	 */
	public static final String getHintEmailNull() { 
		return "Your email address is required to process the application.";
	}



	/**
	 * Test the hint text for the email field as expected 
	 * after null and click-out.
	 * <p>
	 * The calls to sleep are optional (you can comment them out) 
	 * but this test runs so fast that you won't see it otherwise.
	 */
	@Test(enabled=true)
	public void testEmailHintNull(Method method) {
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		driver = getPreparedDriver(testNameLocal);
		Waiter waiter = new Waiter();
		waiter.get(FV_ContactInfoTests.getStartingURL(), driver, 60);
		StringBuilder sBuild = new StringBuilder(driver.getPageSource());
		try { 
			Assert.assertTrue(sBuild.toString().contains("How can we contact you"));
		}catch(AssertionError aE) { 
			System.out.println(testNameLocal + ":\tfail-fast; check contact info page");
			throw aE;
		}finally {
			sBuild.delete(0, sBuild.length());
			sBuild = null;
		}
		ContactInfoPage pageUnderTest = new ContactInfoPage(driver);
		pageUnderTest.getFieldEmail_address().click();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldAreaCode().click();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		String captured = driver.findElement(By.cssSelector("span.pg-field-error")).getText().trim();
		String expected = G_4115_Email_A.getHintEmailNull();
		try { 
			Assert.assertEquals(captured, expected);
			System.out.println("\n" + testNameLocal + ":\tPASS");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			System.out.println(testNameLocal + ":\tExpected '" + expected + "\'");
			System.out.println(testNameLocal + ":\tCaptured '" + captured + "\'");
			throw aE;
		}finally { 
			FieldValidationUtilities.refreshBetweenTest(testNameLocal, driver);
			captured = null;
			expected = null;
			testNameLocal = null;
			waiter = null;
			pageUnderTest = null;
		}
	}


	/**
	 * If subclass of TestBaseSauce, pass the test result up to the Sauce Labs Dashboard.
	 * <p>
	 * Quit driver.
	 * <p>
	 * Delete the cookie this test just created.
	 */
	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {
		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		if(this.getClass().getSuperclass().getSimpleName().equals("TestBaseSauce")) { 
			System.out.println(methodNameLocal + ":\tSauce driver type detected; Sauce-specific teardown up next...");
			((JavascriptExecutor) driver).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
		} else { 
			System.out.println(methodNameLocal + ":\tLocal driver type detected; Sauce-specific teardown skipped intentionally.\n");
		}
		driver.quit();
		System.out.println(methodNameLocal + ":\tEND tearDown.");
	}

}
