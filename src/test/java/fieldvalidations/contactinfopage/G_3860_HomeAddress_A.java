package fieldvalidations.contactinfopage;

import java.lang.reflect.Method;
import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import common.FieldValidationUtilities;
import common.ProgressiveLeasingConstants;
import pages.ContactInfoPage;
import utilities.saucelabs.TestBaseSauce;
import waiter.Waiter;


/**
 * Field validation test: Contact Info page: home address fields.
 * <p>
 * Positive and negative cases.
 * <p>
 * Full coverage for the TestLink case below:
 * <p>
 * http://slc-qaswebtli01/testlink/linkto.php?tprojectPrefix=G&item=testcase&id=G-4115
 * <p>
 * Version 1.0 2019-01-23
 * <p>
 * Version 1.1 2019-02-25
 * <p>
 * Version 1.2 2019-04-19 abstract out common behavior and tune for performance
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.2
 * @see ContactInfoPage
 * @see G_3860_HomeAddress_B
 */
public class G_3860_HomeAddress_A extends TestBaseSauce {

	private WebDriver driver;
	private Waiter waiter;
	private ContactInfoPage pageUnderTest;
	
	/**
	 * Street address line 1 only, all legal.
	 */
	public static final String[] legalAddressesStreet1Only = {
			"1801 Q St",
			"325 Washington St",
			"2270 South 525 West"
	};

	
	/**
	 * A few legal street addresses for testing a business rule.
	 * <p>
	 * These are in "legal" states as per the business rules that 
	 * applied in February 2019.
	 * <p>
	 * These are completed addresses on one line without punctuation.
	 */
	public static final String[] legalAddressesSingleLine = {
			"1801 Q St Lincoln NE 68508",
			"325 Washington St Providence RI 02903",
			"2270 South 525 West Beaver UT 84713"
	};

	/**
	 * Expected text after an illegal street address is entered: invalid characters.
	 * <p>
	 * @return Expected text of a field label.
	 */
	public static final String getHintAddressContainsInvalidCharacters() { 
		return "Your home address can't contain special characters other than " + 
				"period ( . ), comma ( , ), hyphen ( - ), ampersand ( & ), hash ( # ), " + 
				"forward slash ( / ), or an apostrophe ( ' ).";
	}


	/**
	 * Expected text after street address line 1 contains a post office box.
	 * <p>
	 * @return Expected text of a field label.
	 */
	public static final String getHintAddressContainsPOBox() { 
		return "Your home address can't be a P.O. box. Please enter a physical address.";
	}
	
	
	/**
	 * Expected text after street address line 1 begins with a space.
	 * <p>
	 * @return Expected text of a field label.
	 */
	public static final String getHintAddressBeginsWithSpace() { 
		return "Your home address can't begin with a space.";
	}
	
	
	
	/**
	 * Expected text after street address line 1 is left blank.
	 * <p>
	 * @return Expected text of a field label.
	 */
	public static final String getHintAddressNull() { 
		return "Your home address is required to process the application.";
	}
	
	
	/**
	 * I've seen these common permutations in real mail, so I randomized 
	 * the test to pick up any one of these three at random. As of this 
	 * JavaDoc, all of them pass.
	 */
	public static final String[] POBPermutations = { 
			"P.O. Box 1484", 
			"PO Box 1482", 
			"POB 1482" 
	};
	
	


	/**
	 * Silent truncation after position 100. Boundary testing.
	 * <p>
	 * In other words, >100 would be legal, but 101 and following are ignored.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testTruncationStreetAddress(Method method) {
		
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
				
		// begin setup
		driver = getPreparedDriver(testNameLocal);
		
		waiter = new Waiter();

		waiter.get(FV_ContactInfoTests.getStartingURL(), driver, 15);
		StringBuilder sBuild = new StringBuilder(driver.getPageSource());
		try { 
			Assert.assertTrue(sBuild.toString().contains("How can we contact you"));
		}catch(AssertionError aE) { 
			System.out.println(testNameLocal + ":\tfail-fast in setup");
			throw aE;
		}finally {
			sBuild.delete(0, sBuild.length());
			sBuild = null;
		}
		System.out.println(testNameLocal + ":\tsetup complete");
		
		pageUnderTest = new ContactInfoPage(driver);
		// end setup

		
		final HashMap<Integer,String> longStreetAddresses = new HashMap<Integer,String>();
		// phony-baloney long addresses just short of, at, and just beyond the boundary
		longStreetAddresses.put(98,  "108890 Grossteinbeck Hieberstrasse Brad Misty Todd Kellie Shawn Allen Dallin Gile CJ Scott Ryan Je");	// 98
		longStreetAddresses.put(100, "108890 Grossteinbeck Hieberstrasse Brad Misty Todd Kellie Shawn Allen Dallin Gile CJ Scott Ryan Jenn");	// 100
		longStreetAddresses.put(104, "108890 Grossteinbeck Hieberstrasse Brad Misty Todd Kellie Shawn Allen Dallin Gile CJ Scott Ryan Jennifer");	// 104

		// Optional, in case you're trying to watch it run in a headed browser.
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,700)", "");
		jse = null;

		pageUnderTest.getFieldAddressLine1().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		pageUnderTest.getFieldAddressLine1().sendKeys(longStreetAddresses.get(98)); 
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		StringBuilder captured = new StringBuilder(pageUnderTest.getFieldAddressLine1().getAttribute("value"));

		try { 
			Assert.assertEquals(captured.toString(), longStreetAddresses.get(98));
			System.out.println("\n" + testNameLocal + ": PASS at boundary - 2");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ": FAIL at boundary - 2");
			System.out.println(testNameLocal + ": expected \'" + longStreetAddresses.get(40) + "\'");
			System.out.println(testNameLocal + ": captured \'" + captured.toString() + "\'");
			throw aE;
		}finally { 
			captured.delete(0, captured.length());
			captured = null;
		}	

		pageUnderTest.getFieldAddressLine1().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldAddressLine1().sendKeys(longStreetAddresses.get(100)); 
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		captured = new StringBuilder(pageUnderTest.getFieldAddressLine1().getAttribute("value"));

		try { 
			Assert.assertEquals(captured.toString(), longStreetAddresses.get(100));
			System.out.println("\n" + testNameLocal + ": PASS at boundary");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ": FAIL at boundary");
			System.out.println(testNameLocal + ": expected \'" + longStreetAddresses.get(100) + "\'");
			System.out.println(testNameLocal + ": captured \'" + captured.toString() + "\'");
			throw aE;
		}finally { 
			captured.delete(0, captured.length());
			captured = null;
		}

		pageUnderTest.getFieldAddressLine1().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldAddressLine1().sendKeys(longStreetAddresses.get(104)); 
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		captured = new StringBuilder(pageUnderTest.getFieldAddressLine1().getAttribute("value"));

		try { 
			Assert.assertEquals(captured.toString(), longStreetAddresses.get(100));
			System.out.println("\n" + testNameLocal + ": PASS at boundary + 4");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ": FAIL at boundary + 4");
			System.out.println(testNameLocal + ": expected \'" + longStreetAddresses.get(100) + "\'");
			System.out.println(testNameLocal + ": captured \'" + captured.toString() + "\'");
			throw aE;
		}finally { 
			captured.delete(0, captured.length());
			captured = null;
			pageUnderTest = null;
			testNameLocal = null;
			waiter = null;
		}
	}	




	/**
	 * Allowed special characters: period, comma, hyphen, ampersand, hash, forward_slash, apostrophe.
	 * <ul>
	 * <li>.</li>
	 * <li>,</li>
	 * <li>-</li>
	 * <li>&</li>
	 * <li>#</li>
	 * <li>/</li>
	 * <li>'</li>
	 * </ul>
	 * A street address containing these symbols must not spawn an error.
	 * <p>
	 * This test begins with a known valid address and randomly selectrs one of these 
	 * special characters, then insert-overwrites it in a random position. Over time, 
	 * all cases will be covered.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testAllowedSpecialCharacters(Method method) {
		
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		
		
		// begin setup
		driver = getPreparedDriver(testNameLocal);
		
		waiter = new Waiter();

		waiter.get(FV_ContactInfoTests.getStartingURL(), driver, 15);
		StringBuilder sBuild = new StringBuilder(driver.getPageSource());
		try { 
			Assert.assertTrue(sBuild.toString().contains("How can we contact you"));
		}catch(AssertionError aE) { 
			System.out.println(testNameLocal + ":\tfail-fast in setup");
			throw aE;
		}finally {
			sBuild.delete(0, sBuild.length());
			sBuild = null;
		}
		System.out.println(testNameLocal + ":\tsetup complete");
		
		pageUnderTest = new ContactInfoPage(driver);
		// end setup

		

		final char[] allowedSpecialCharacters = {'.', ',', '-', '&', '#', '/', '\''};
		
		StringBuilder baseAddress = new StringBuilder(legalAddressesStreet1Only[new java.util.Random().nextInt(legalAddressesStreet1Only.length)]);
		
		java.util.Random ran = new java.util.Random();
		int replaceThis = ran.nextInt(baseAddress.toString().length()+1);
		if(replaceThis==0) { 
			replaceThis++;
		}
		int replaceWith = ran.nextInt(allowedSpecialCharacters.length);
		
		baseAddress.replace(replaceThis, (replaceThis+1), Character.toString(allowedSpecialCharacters[replaceWith]));

		pageUnderTest.getFieldAddressLine1().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		pageUnderTest.getFieldAddressLine1().sendKeys(baseAddress); 
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		
		// Optional, in case you're trying to watch it run in a headed browser.
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,700)", "");
		jse = null;

		pageUnderTest.getFieldAddressLine2().click(); 
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		WebElement expectedSuccessCheckMark = null;
		
		try { 
			expectedSuccessCheckMark = driver.findElement(By.cssSelector("i.pg-address-success-icon.pg-icon.material-icons"));
		}catch(NoSuchElementException nSEE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL early; required element missing");
			throw new AssertionError(testNameLocal,nSEE);
		}
		
		try { 
			Assert.assertTrue(expectedSuccessCheckMark.isDisplayed(), (testNameLocal + ":\tFAIL with \'" + baseAddress + "\'"));
			System.out.println("\n" + testNameLocal + ":\tPASS with \'" + baseAddress + "\'");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL with \'" + baseAddress + "\'; the expected check mark was not found");
			throw aE;
		}finally { 
			expectedSuccessCheckMark = null;
			baseAddress = null;
			pageUnderTest = null;
			testNameLocal = null;
			testNameLocal = null;
			waiter = null;
		}
	}	
	


	/**
	 * Disallowed special characters: there are more, but I'm randomly using these.
	 * <ul>
	 * <li>!</li>
	 * <li>@</li>
	 * <li>%</li>
	 * <li>&</li>
	 * <li>^</li>
	 * </ul>
	 * A street address containing these symbols must spawn an error.
	 * <p>
	 * This test begins with a known valid address (the address of DP headquarters) 
	 * and randomly selects one of these special characters, then insert-overwrites 
	 * it in a random position. 
	 * <p>
	 * @param method
	 */	
	@Test(enabled=true) 
	public void testDisallowedSpecialCharacters(Method method) { 
		
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		
		
		// begin setup
		driver = getPreparedDriver(testNameLocal);
		
		waiter = new Waiter();

		waiter.get(FV_ContactInfoTests.getStartingURL(), driver, 15);
		StringBuilder sBuild = new StringBuilder(driver.getPageSource());
		try { 
			Assert.assertTrue(sBuild.toString().contains("How can we contact you"));
		}catch(AssertionError aE) { 
			System.out.println(testNameLocal + ":\tfail-fast in setup");
			throw aE;
		}finally {
			sBuild.delete(0, sBuild.length());
			sBuild = null;
		}
		System.out.println(testNameLocal + ":\tsetup complete");
		
		pageUnderTest = new ContactInfoPage(driver);
		// end setup


		final char[] disallowedSpecialCharacters = {'!', '@', '%', '^'};
		
		StringBuilder baseAddress = new StringBuilder(legalAddressesStreet1Only[new java.util.Random().nextInt(legalAddressesStreet1Only.length)]);
		
		java.util.Random ran = new java.util.Random();
		int replaceThis = ran.nextInt(baseAddress.toString().length()+1);
		if(replaceThis==0) { 
			replaceThis++;
		}
		int replaceWith = ran.nextInt(disallowedSpecialCharacters.length);
		
		baseAddress.replace(replaceThis, (replaceThis+1), Character.toString(disallowedSpecialCharacters[replaceWith]));

		WebElement fieldTempAddress		= pageUnderTest.getFieldAddressLine1();
		WebElement fieldTempZIP			= pageUnderTest.getFieldZIP();
		
		// fast-fail
		Assert.assertNotNull(fieldTempAddress);
		Assert.assertNotNull(fieldTempZIP);
		
		fieldTempAddress.clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		
		fieldTempAddress.sendKeys(baseAddress.toString());
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		
		fieldTempZIP.clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		
		fieldTempZIP.sendKeys(ProgressiveLeasingConstants.ADDRESS_DRAPER_HQ[3]);
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		// tab out in order to trigger the expected error message
		FieldValidationUtilities.tabOut(testNameLocal, driver);
		
		FieldValidationUtilities.sleepSecond(testNameLocal);
		FieldValidationUtilities.sleepSecond(testNameLocal);

		// Optional, in case you're trying to watch it run in a headed browser.
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,700)", "");
		jse = null;

		WebElement errorExpectedTemp = null;
		
		try { 
			errorExpectedTemp = driver.findElement(By.xpath("//pg-address-wrapper/div[2]/span[2]"));
		}catch(NoSuchElementException nSEE) { 
			throw new IllegalStateException(testNameLocal + ":\terror element not created",nSEE);
		}
		
		String capturedTemp = errorExpectedTemp.getText();
		
		try { 
			Assert.assertTrue(capturedTemp.contains(getHintAddressContainsInvalidCharacters()));
			System.out.println("\n" + testNameLocal + ":\tPASS with \'" + baseAddress.toString() + "\'");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			System.out.println(testNameLocal + ":\texpected \'" + getHintAddressContainsInvalidCharacters() + "\'");
			System.out.println(testNameLocal + ":\tcaptured \'" + capturedTemp + "\'\n");
			throw aE;
		}finally { 
			baseAddress = null;
			fieldTempAddress = null;
			fieldTempZIP = null;
			capturedTemp = null;
			errorExpectedTemp = null;
			pageUnderTest = null;
			testNameLocal = null;
			waiter = null;
		}
	}
	

	

	/**
	 * Ensure that a post office box in the street address 1 field spawns a specific error.
	 * <p>
	 * Randomly select one of several commonly occurring permutations.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true) 
	public void testPostOfficeBox(Method method) { 
		
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();

		
		
		// begin setup
		driver = getPreparedDriver(testNameLocal);
		
		waiter = new Waiter();

		waiter.get(FV_ContactInfoTests.getStartingURL(), driver, 15);
		StringBuilder sBuild = new StringBuilder(driver.getPageSource());
		try { 
			Assert.assertTrue(sBuild.toString().contains("How can we contact you"));
		}catch(AssertionError aE) { 
			System.out.println(testNameLocal + ":\tfail-fast in setup");
			throw aE;
		}finally {
			sBuild.delete(0, sBuild.length());
			sBuild = null;
		}
		System.out.println(testNameLocal + ":\tsetup complete");
		
		pageUnderTest = new ContactInfoPage(driver);
		// end setup


		
		String illegalAddressTemp	= POBPermutations[new java.util.Random().nextInt(POBPermutations.length)];
		
		WebElement fieldTempAddress		= pageUnderTest.getFieldAddressLine1();
		WebElement fieldTempZIP			= pageUnderTest.getFieldZIP();
		
		// fast-fail
		Assert.assertNotNull(fieldTempAddress);
		Assert.assertNotNull(fieldTempZIP);
		
		fieldTempAddress.clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		
		fieldTempAddress.sendKeys(illegalAddressTemp);
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		
		fieldTempZIP.clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		
		fieldTempZIP.sendKeys(ProgressiveLeasingConstants.ADDRESS_DRAPER_HQ[3]);
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		// tab out in order to trigger the expected error message
		FieldValidationUtilities.tabOut(testNameLocal, driver);
		
		FieldValidationUtilities.sleepSecond(testNameLocal);
		FieldValidationUtilities.sleepSecond(testNameLocal);

		// Optional, in case you're trying to watch it run in a headed browser.
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,700)", "");
		jse = null;

		WebElement errorExpectedTemp = null;
		
		try { 
			errorExpectedTemp = driver.findElement(By.xpath("//pg-address-wrapper/div[2]/span[3]"));
		}catch(NoSuchElementException nSEE) { 
			throw new IllegalStateException(testNameLocal + ":\terror element not created",nSEE);
		}
		
		String capturedTemp = errorExpectedTemp.getText();
		
		try { 
			Assert.assertTrue(capturedTemp.contains(getHintAddressContainsPOBox()));
			System.out.println("\n" + testNameLocal + ":\tPASS with \'" + illegalAddressTemp + "\'");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			System.out.println(testNameLocal + ":\texpected \'" + getHintAddressContainsPOBox() + "\'");
			System.out.println(testNameLocal + ":\tcaptured \'" + capturedTemp + "\'\n");
			throw aE;
		}finally { 
			illegalAddressTemp = null;
			fieldTempAddress = null;
			fieldTempZIP = null;
			capturedTemp = null;
			errorExpectedTemp = null;
			testNameLocal = null;
			pageUnderTest = null;
			testNameLocal = null;
			waiter = null;
		}
	}
	
	

	

	/**
	 * Ensure that a street address beginning with a space spawns a specific error.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true) 
	public void testSpaceInPosition1(Method method) { 
		
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();

		
		// begin setup
		driver = getPreparedDriver(testNameLocal);
		
		waiter = new Waiter();

		waiter.get(FV_ContactInfoTests.getStartingURL(), driver, 15);
		StringBuilder sBuild = new StringBuilder(driver.getPageSource());
		try { 
			Assert.assertTrue(sBuild.toString().contains("How can we contact you"));
		}catch(AssertionError aE) { 
			System.out.println(testNameLocal + ":\tfail-fast in setup");
			throw aE;
		}finally {
			sBuild.delete(0, sBuild.length());
			sBuild = null;
		}
		System.out.println(testNameLocal + ":\tsetup complete");
		
		pageUnderTest = new ContactInfoPage(driver);
		// end setup

		

		/*
		 * One-off. Space in first position. To my knowledge this is 
		 * the only place we use this.
		 */
		String illegalAddressTemp = " 256 W Data Dr";
		
		WebElement fieldTempAddress		= pageUnderTest.getFieldAddressLine1();
		WebElement fieldTempZIP			= pageUnderTest.getFieldZIP();
		
		// fast-fail
		Assert.assertNotNull(fieldTempAddress);
		Assert.assertNotNull(fieldTempZIP);
		
		fieldTempAddress.clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		
		fieldTempAddress.sendKeys(illegalAddressTemp);
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		
		fieldTempZIP.clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		
		fieldTempZIP.sendKeys(ProgressiveLeasingConstants.ADDRESS_DRAPER_HQ[3]);
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		// tab out in order to trigger the expected error message
		
		Actions builder = new Actions(driver);
		builder.sendKeys(Keys.TAB).build().perform();
		
		FieldValidationUtilities.sleepSecond(testNameLocal);
		FieldValidationUtilities.sleepSecond(testNameLocal);

		// Optional, in case you're trying to watch it run in a headed browser.
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,700)", "");
		jse = null;

		WebElement errorExpectedTemp = null;
		
		try { 
			errorExpectedTemp = driver.findElement(By.xpath("//pg-address-wrapper/div[2]/span[4]"));
		}catch(NoSuchElementException nSEE) { 
			throw new IllegalStateException(testNameLocal + ":\terror element not created",nSEE);
		}
		
		String capturedTemp = errorExpectedTemp.getText();
		
		try { 
			Assert.assertTrue(capturedTemp.contains(getHintAddressBeginsWithSpace()));
			System.out.println("\n" + testNameLocal + ":\tPASS with \'" + illegalAddressTemp + "\'");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			System.out.println(testNameLocal + ":\texpected \'" + getHintAddressBeginsWithSpace() + "\'");
			System.out.println(testNameLocal + ":\tcaptured \'" + capturedTemp + "\'\n");
			throw aE;
		}finally { 
			builder = null;
			illegalAddressTemp = null;
			fieldTempAddress = null;
			fieldTempZIP = null;
			capturedTemp = null;
			errorExpectedTemp = null;
			pageUnderTest = null;
			testNameLocal = null;
			waiter = null;
		}
	}
	

	/**
	 * When the user accidentally pastes a full, one-line address into the address line 1 field, this must not spawn an error.
	 * <p>
	 * For example, if you happen to have this in your clipboard...
	 * <p>
	 * <pre>256 W Data Dr Draper UT 84020</pre>
	 * <p>
	 * ...it should not throw an error.
	 * <p>
	 * This seems wrong to us. We double-checked. Indeed, it is a requirement. Nevertheless, we 
	 * have some more questions about it. For the time being, we've disabled this test.
	 * <p>
	 * @param method
	 */
	@Test(enabled=false) // disabled intentionally; will revisit
	public void testAllowedOneLineAddress(Method method) {
				
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();

		
		// begin setup
		driver = getPreparedDriver(testNameLocal);
		
		waiter = new Waiter();

		waiter.get(FV_ContactInfoTests.getStartingURL(), driver, 15);
		StringBuilder sBuild = new StringBuilder(driver.getPageSource());
		try { 
			Assert.assertTrue(sBuild.toString().contains("How can we contact you"));
		}catch(AssertionError aE) { 
			System.out.println(testNameLocal + ":\tfail-fast in setup");
			throw aE;
		}finally {
			sBuild.delete(0, sBuild.length());
			sBuild = null;
		}
		System.out.println(testNameLocal + ":\tsetup complete");
		
		pageUnderTest = new ContactInfoPage(driver);
		// end setup

		

		// Optional, in case you're trying to watch it run in a headed browser.
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,700)", "");
		jse = null;

		pageUnderTest.getFieldAddressLine1().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		pageUnderTest.getFieldAddressLine1().sendKeys(legalAddressesStreet1Only[new java.util.Random().nextInt(legalAddressesStreet1Only.length)]);
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		
		pageUnderTest.getFieldAddressLine2().click(); 
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		WebElement expectedSuccessCheckMark = null;
		
		try { 
			expectedSuccessCheckMark = driver.findElement(By.cssSelector("i.pg-address-success-icon.pg-icon.material-icons"));
		}catch(NoSuchElementException nSEE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL early; required element missing");
			throw new AssertionError(testNameLocal,nSEE);
		}
		
		try { 
			Assert.assertTrue(expectedSuccessCheckMark.isDisplayed(), ("\n" + testNameLocal + ":\tFAIL"));
			System.out.println("\n" + testNameLocal + ":\tPASS");
		}catch(AssertionError aE) { 
			throw aE;
		}finally { 
			expectedSuccessCheckMark = null;
			testNameLocal = null;
			pageUnderTest = null;
			testNameLocal = null;
			waiter = null;
		}
	}	
	
	

	/**
	 * If subclass of TestBaseSauce, pass the test result up to the Sauce Labs Dashboard.
	 * <p>
	 * Quit driver.
	 * <p>
	 * Delete the cookie this test just created.
	 */
	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {
		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		if(this.getClass().getSuperclass().getSimpleName().equals("TestBaseSauce")) { 
			System.out.println(methodNameLocal + ":\tSauce driver type detected; Sauce-specific teardown up next...");
			((JavascriptExecutor) driver).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
		} else { 
			System.out.println(methodNameLocal + ":\tLocal driver type detected; Sauce-specific teardown skipped intentionally.\n");
		}
		driver.quit();
		System.out.println(methodNameLocal + ":\tEND tearDown.");
	}

}
