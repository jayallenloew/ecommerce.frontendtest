package fieldvalidations.contactinfopage;

import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import common.FieldValidationUtilities;
import common.ProgressiveLeasingConstants;
import pages.ContactInfoPage;
import utilities.saucelabs.DriverTypeSauce;
import utilities.saucelabs.TestBaseSauce;
import waiter.Waiter;

/**
 * Field validation test: Contact Info page: home address fields.
 * <p>
 * As of this update, this test class contains a single, negative case that 
 * wouldnt play nice with others :-)
 * <p>
 * http://slc-qaswebtli01/testlink/linkto.php?tprojectPrefix=G&item=testcase&id=G-4115
 * <p>
 * Version 1.0 2019-02-25
 * <p>
 * Version 1.1 2019-04-19 abstract out common behavior and tune for performance
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.1
 * @see ContactInfoPage
 * @see G_3860_HomeAddress_A
 */
public class G_3860_HomeAddress_B extends TestBaseSauce {

	private WebDriver driver;
	private Waiter waiter;
	private ContactInfoPage pageUnderTest;
	


	/**
	 * When the user "forgets" street address line 1, this must not spawn an error.
	 * <p>
	 * @param method
	 */	
	@Test(enabled=true) 
	public void testAddressNull(Method method) { 
		
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		
		
		// begin setup
		// you want Windows Chrome for this one...
		super.setDriverTypeForTestClass(DriverTypeSauce.WINDOWS_CHROME);
		driver = getPreparedDriver(testNameLocal);

		waiter = new Waiter();

		waiter.get(FV_ContactInfoTests.getStartingURL(), driver, 15);
		StringBuilder sBuild = new StringBuilder(driver.getPageSource());
		try { 
			Assert.assertTrue(sBuild.toString().contains("How can we contact you"));
		}catch(AssertionError aE) { 
			System.out.println(testNameLocal + ":\tfail-fast in setup");
			throw aE;
		}finally {
			sBuild.delete(0, sBuild.length());
			sBuild = null;
		}
		System.out.println(testNameLocal + ":\tsetup complete");
		
		pageUnderTest = new ContactInfoPage(driver);
		
		driver.manage().window().fullscreen();
		// end setup

		
		WebElement fieldTempZIP			= pageUnderTest.getFieldZIP();
		WebElement fieldTempAddr1		= pageUnderTest.getFieldAddressLine1();
		
		// fast-fail
		Assert.assertNotNull(fieldTempZIP);
		Assert.assertNotNull(fieldTempAddr1);
		
		// Optional, in case you're trying to watch it run in a headed browser.
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,700)", "");
		jse = null;

		fieldTempAddr1.clear();
		FieldValidationUtilities.sleepSecond(testNameLocal);
		
		FieldValidationUtilities.tabOut(testNameLocal, driver);

		fieldTempZIP.clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		fieldTempZIP.sendKeys(ProgressiveLeasingConstants.ADDRESS_DRAPER_HQ[3]);
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		
		// tab out in order to trigger the expected error message then wait 2 seconds		
		FieldValidationUtilities.tabOut(testNameLocal, driver);
		
		FieldValidationUtilities.sleepSecond(testNameLocal);
		FieldValidationUtilities.sleepSecond(testNameLocal);

		WebElement errorExpectedTemp = null;
		
		try { 
			errorExpectedTemp = driver.findElement(By.xpath("//pg-address-wrapper/div[2]/span"));
		}catch(NoSuchElementException nSEE) { 
			throw new IllegalStateException(testNameLocal + ":\terror element not created",nSEE);
		}
		
		String capturedTemp = errorExpectedTemp.getText();
		
		try { 
			Assert.assertTrue(capturedTemp.contains(G_3860_HomeAddress_A.getHintAddressNull()));
			System.out.println("\n" + testNameLocal + ":\tPASS");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			System.out.println(testNameLocal + ":\texpected \'" + G_3860_HomeAddress_A.getHintAddressNull() + "\'");
			System.out.println(testNameLocal + ":\tcaptured \'" + capturedTemp + "\'\n");
			throw aE;
		}finally { 
			fieldTempZIP = null;
			fieldTempAddr1 = null;
			capturedTemp = null;
			errorExpectedTemp = null;
			pageUnderTest = null;
			testNameLocal = null;
		}
	}
	
	


	/**
	 * If subclass of TestBaseSauce, pass the test result up to the Sauce Labs Dashboard.
	 * <p>
	 * Quit driver.
	 * <p>
	 * Delete the cookie this test just created.
	 */
	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {
		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		if(this.getClass().getSuperclass().getSimpleName().equals("TestBaseSauce")) { 
			System.out.println(methodNameLocal + ":\tSauce driver type detected; Sauce-specific teardown up next...");
			((JavascriptExecutor) driver).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
		} else { 
			System.out.println(methodNameLocal + ":\tLocal driver type detected; Sauce-specific teardown skipped intentionally.\n");
		}
		driver.quit();
		waiter = null;
		pageUnderTest = null;
		System.out.println(methodNameLocal + ":\tEND tearDown.");
	}

}
