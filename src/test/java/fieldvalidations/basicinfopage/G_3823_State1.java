package fieldvalidations.basicinfopage;

import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import common.FieldValidationUtilities;
import pages.BasicInfoPage;
import utilities.saucelabs.DriverTypeSauce;
import utilities.saucelabs.TestBaseSauce;

/**
 * TestLink path:
 * <p>
 * General | Running | ECOM_UI | ECOM_FieldValidations | G-3823:Field validation rules: Basic Info: Issuing State
 * <p>
 * Sample direct nav URL:
 * <p>
 * https://vdc-qaswebapp12.stormwind.local/eComUI/#/apply/basic-info
 * <p>
 * Run from: src/test/resources/TestNGSuiteFiles/FieldValidations/FieldValidations.xml
 * <p>
 * Version 1.0 2019-02-07
 * <p>
 * Version 1.1 2019-04-19 abstract out common behavior and tune for performance
 * <p>
 * Version 1.2 2019-04-19 abstract out common behavior and tune for performance
 * <p>
 * @author <a href="mailto:jay.loew@progleasing.com">Allen</a>
 * @author <a href="mailto:shawn.hartke@progleasing.com">Shawn</a>
 * @version 1.2
 * @see G_3823_State2.java
 * @see G_3823_State3.java
 */
public class G_3823_State1 extends TestBaseSauce {

	private WebDriver driver;


	/**
	 * Max length of field is 15. 
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testMaxLength(Method method) {

		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		
		super.setDriverTypeForTestClass(DriverTypeSauce.WINDOWS_FIREFOX);
		
		driver = getPreparedDriver(testNameLocal);
		
		driver.navigate().to(FV_BasicInfoTests.getStartingURL());
		
		FV_BasicInfoTests.commonWaitInSetup(driver);
		
		BasicInfoPage pageUnderTest = new BasicInfoPage(driver);

		String captured = pageUnderTest.getFieldIssuingState().getAttribute("maxlength");

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		try { 
			Assert.assertEquals(captured, "15");
			System.out.println("\n" + testNameLocal + ": PASS");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ": FAIL");
			System.out.println(testNameLocal + ": expected \'15\'");
			System.out.println(testNameLocal + ": captured \'" + captured + "\'");
			throw aE;
		}finally { 
			captured = null;
			pageUnderTest = null;
			testNameLocal = null;
			pageUnderTest = null;
		}	
	}	



	/**
	 * Enter characters not corresponding to any state: qwerty. 
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testNonExistentState(Method method) {

		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		
		super.setDriverTypeForTestClass(DriverTypeSauce.WINDOWS_CHROME);

		driver = getPreparedDriver(testNameLocal);
		
		driver.navigate().to(FV_BasicInfoTests.getStartingURL());
		
		FV_BasicInfoTests.commonWaitInSetup(driver);
		
		BasicInfoPage pageUnderTest = new BasicInfoPage(driver);

		driver.manage().window().maximize();

		pageUnderTest.getFieldIssuingState().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		pageUnderTest.getFieldIssuingState().sendKeys("qwerty");
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		pageUnderTest.getFieldDriverLicense().click(); // any click-out
		FieldValidationUtilities.sleepSecond(testNameLocal);

		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		jse = null;
		
		String expected = "Please pick a valid state.";
		String captured = null;
		WebElement temp = null;

		try { 
			temp = driver.findElement(By.cssSelector(".pg-field-label-float .pg-field-error:nth-child(3)"));
		}catch(NoSuchElementException nSEE) { 
			throw new AssertionError(testNameLocal, nSEE);
		}

		captured = temp.getText();

		try { 
			Assert.assertEquals(captured, expected);
			System.out.println("\n" + testNameLocal + ": PASS");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ": FAIL");
			System.out.println(testNameLocal + ": expected \'" + expected + "\'");
			System.out.println(testNameLocal + ": captured \'" + captured + "\'");
			throw aE;
		}finally { 
			expected = null;
			captured = null;
			temp = null;
			pageUnderTest = null;
			testNameLocal = null;
		}	
	}		



	/**
	 * Enter a space as the first character of a state. 
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testIllegalStateBeginsWithSpace(Method method) {

		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		
		driver = getPreparedDriver(testNameLocal);
		
		driver.navigate().to(FV_BasicInfoTests.getStartingURL());
		
		FV_BasicInfoTests.commonWaitInSetup(driver);
		
		BasicInfoPage pageUnderTest = new BasicInfoPage(driver);

		driver.manage().window().maximize();

		pageUnderTest.getFieldIssuingState().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		pageUnderTest.getFieldIssuingState().sendKeys(" Alaska");
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		FieldValidationUtilities.tabOut(testNameLocal, driver);
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		jse = null;

		String expected = "Your issuing state can't begin with a space.";
		String captured = null;
		WebElement temp = null;

		try { 
			temp = driver.findElement(By.cssSelector(".pg-field-label-float .pg-field-error:nth-child(2)"));
		}catch(NoSuchElementException nSEE) { 
			throw new AssertionError(testNameLocal, nSEE);
		}

		captured = temp.getText();

		try { 
			Assert.assertEquals(captured, expected);
			System.out.println("\n" + testNameLocal + ": PASS");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ": FAIL");
			System.out.println(testNameLocal + ": expected \'" + expected + "\'");
			System.out.println(testNameLocal + ": captured \'" + captured + "\'");
			throw aE;
		}finally { 
			expected = null;
			captured = null;
			temp = null;
			pageUnderTest = null;
			testNameLocal = null;
		}	
	}	



	/**
	 * Happy-path test of type-started, legal state value, i.e. AK for Alaska. 
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testHappySelctByState(Method method) {

		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		
		driver = getPreparedDriver(testNameLocal);
		
		driver.navigate().to(FV_BasicInfoTests.getStartingURL());
		
		FV_BasicInfoTests.commonWaitInSetup(driver);
		
		BasicInfoPage pageUnderTest = new BasicInfoPage(driver);

		pageUnderTest.getFieldIssuingState().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		pageUnderTest.getFieldIssuingState().sendKeys("AK");
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		FieldValidationUtilities.tabOut(testNameLocal, driver);

		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		jse = null;

		WebElement temp = null;

		try { 
			temp = driver.findElement(By.cssSelector("#issuing-state .material-icons:nth-child(2)"));
		}catch(NoSuchElementException nSEE) { 
			throw new AssertionError(testNameLocal, nSEE);
		}

		try { 
			Assert.assertTrue(temp.isDisplayed());
			System.out.println("\n" + testNameLocal + ": PASS");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ": FAIL");
			throw aE;
		}finally { 
			temp = null;
			pageUnderTest = null;
			testNameLocal = null;
		}	
	}		


	/**
	 * If subclass of TestBaseSauce, pass the test result up to the Sauce Labs Dashboard.
	 * <p>
	 * Quit driver.
	 * <p>
	 * Delete the cookie this test just created.
	 */
	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {
		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		if(this.getClass().getSuperclass().getSimpleName().equals("TestBaseSauce")) { 
			System.out.println(methodNameLocal + ":\tSauce driver type detected; Sauce-specific teardown up next...");
			((JavascriptExecutor) driver).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
		} else { 
			System.out.println(methodNameLocal + ":\tLocal driver type detected; Sauce-specific teardown skipped intentionally.\n");
		}
		driver.quit();
		System.out.println(methodNameLocal + ":\tEND tearDown.");
	}

}
