package fieldvalidations.basicinfopage;

import java.lang.reflect.Method;
import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import common.FieldValidationUtilities;
import pages.BasicInfoPage;
import utilities.saucelabs.TestBaseSauce;

/**
 * TestLink path:
 * <p>
 * General | Running | ECOM_UI | ECOM_FieldValidations | G-3842:Field validation rules: Basic Info: DL or Govt ID
 * <p>
 * Sample direct nav URL:
 * <p>
 * https://vdc-qaswebapp12.stormwind.local/eComUI/#/apply/basic-info
 * <p>
 * Run from: src/test/resources/TestNGSuiteFiles/FieldValidations/FieldValidations.xml
 * <p>
 * Version 1.0 2019-01-31
 * <p>
 * Version 1.1 2019-02-20 add first test (in TestLink) for label.
 * <p>
 * Version 1.2 2019-04-19 abstract out common behavior and tune for performance
 * <p>
 * @author <a href="mailto:jay.loew@progleasing.com">Allen</a>
 * @author <a href="mailto:shawn.hartke@progleasing.com">Shawn</a>
 * @version 1.2
 */
public class G_3842_DL_GI extends TestBaseSauce { 

	private WebDriver driver;

	
	/**
	 * Verify text of field label displayed: Driver license or Govt ID
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testLabel(Method method) {
		
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		
		driver = getPreparedDriver(testNameLocal);
		
		driver.navigate().to(FV_BasicInfoTests.getStartingURL());

		FV_BasicInfoTests.commonWaitInSetup(driver);

		String captured = driver.findElement(By.cssSelector("#driver-license > div > pg-field > div.pg-field-content-wrapper.autocomplete-content-wrapper > div > div.pg-field-content-infix.pg-field-content-section > label")).getText();
		String expected = "Driver license or Govt ID";
		
		try { 
			Assert.assertEquals(captured, expected);
			System.out.println("\n" + testNameLocal + ": PASS");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ": FAIL");
			System.out.println(testNameLocal + ": expected \'" + expected + "\'");
			System.out.println(testNameLocal + ": captured \'" + captured + "\'");
			throw aE;
		}finally { 
			captured = null;
			expected = null;
			testNameLocal = null;
		}
	}	
	
	
	
	/**
	 * Click in, don't do anything else, and verify message.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testPatientMessage(Method method) {
		
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		
		driver = getPreparedDriver(testNameLocal);
		
		driver.navigate().to(FV_BasicInfoTests.getStartingURL());
		
		FV_BasicInfoTests.commonWaitInSetup(driver);

		BasicInfoPage pageUnderTest = new BasicInfoPage(driver);

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		jse = null;

		pageUnderTest.getFieldDriverLicense().click(); 
		
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		
		String captured = driver.findElement(By.xpath("//pg-driver-license/div/pg-field/div[2]/span")).getText();
		String expected = "This helps us verify your identity.";
		
		try { 
			Assert.assertEquals(captured, expected);
			System.out.println("\n" + testNameLocal + ": PASS");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ": FAIL");
			System.out.println(testNameLocal + ": expected \'" + expected + "\'");
			System.out.println(testNameLocal + ": captured \'" + captured + "\'");
			throw aE;
		}finally { 
			captured = null;
			expected = null;
			pageUnderTest = null;
			testNameLocal = null;
			pageUnderTest = null;
		}
	}	
	
		

	
	/**
	 * Click in, click anywhere else, verify message indicating null value.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testNullMessage(Method method) {
		
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		
		driver = getPreparedDriver(testNameLocal);
		
		driver.navigate().to(FV_BasicInfoTests.getStartingURL());
		
		FV_BasicInfoTests.commonWaitInSetup(driver);

		BasicInfoPage pageUnderTest = new BasicInfoPage(driver);
		
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		jse = null;

		pageUnderTest.getFieldDriverLicense().click(); 

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		
		// this time we need to click out
		pageUnderTest.getFieldNameFirst().click();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		
		String captured = driver.findElement(By.xpath("//pg-driver-license/div/pg-field/div[2]/span[2]")).getText();
		String expected = "Your driver license or Govt ID is required to process the application.";
		
		try { 
			Assert.assertEquals(captured, expected);
			System.out.println("\n" + testNameLocal + ": PASS");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ": FAIL");
			System.out.println(testNameLocal + ": expected \'" + expected + "\'");
			System.out.println(testNameLocal + ": captured \'" + captured + "\'");
			throw aE;
		}finally { 
			captured = null;
			expected = null;
			pageUnderTest = null;
			testNameLocal = null;
			pageUnderTest = null;
		}
	}	
	
	
	
	/**
	 * Click in, enter a legal value, click out, wait 2 seconds, verify happy-path checkmark.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testHappyCheck(Method method) {
		
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		
		driver = getPreparedDriver(testNameLocal);
		
		driver.navigate().to(FV_BasicInfoTests.getStartingURL());
		
		FV_BasicInfoTests.commonWaitInSetup(driver);

		BasicInfoPage pageUnderTest = new BasicInfoPage(driver);
		
		pageUnderTest.getFieldDriverLicense().clear(); 
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldDriverLicense().sendKeys("552291889");
		
		// this time we need to click out
		pageUnderTest.getFieldNameFirst().click();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		
		WebElement checkmark = driver.findElement(By.cssSelector("#driver-license > div > pg-field > div.pg-field-content-wrapper.autocomplete-content-wrapper > div > div.pg-field-content-suffix.pg-field-content-section > i.material-icons.success-icon.pg-field-suffix"));
		
		try { 
			Assert.assertTrue(checkmark.isDisplayed());
			System.out.println("\n" + testNameLocal + ": PASS");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ": FAIL");
			throw aE;
		}finally { 
			checkmark = null;
			pageUnderTest = null;
			testNameLocal = null;
			pageUnderTest = null;
		}
	}	
	
	
	
	
	/**
	 * Click in, enter a legal value, wait, toggle show-hide.
	 * <p>
	 * Note to tester: yes we dislike sleep but the application under test requires it here.
	 * <p>
	 * @param method
	 * @throws InterruptedException
	 */
	@Test(enabled=true)
	public void testToggleShowHide(Method method) {
		
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		
		driver = getPreparedDriver(testNameLocal);
		
		driver.navigate().to(FV_BasicInfoTests.getStartingURL());
		
		FV_BasicInfoTests.commonWaitInSetup(driver);

		BasicInfoPage pageUnderTest = new BasicInfoPage(driver);

		String validEntry = "552291889";
		
		String fieldValueCaptured = null;
				
		driver.manage().window().maximize();
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		jse = null;
		
		WebElement fieldUnderTest = pageUnderTest.getFieldDriverLicense();
		
		fieldUnderTest.sendKeys(validEntry);
		try {
			Thread.sleep(4000);
		} catch (InterruptedException iE) {
			throw new AssertionError(getClass().getSimpleName() + "." + testNameLocal, iE);
		} // the application under test does not mask the value immediately
		
		driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='done'])[3]/following::i[1]")).click();
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException iE) {
			throw new AssertionError(testNameLocal, iE);
		} 
		
		// un-masked value
		fieldValueCaptured = pageUnderTest.getFieldDriverLicense().getAttribute("value");
		
		try { 
			Assert.assertEquals(validEntry, fieldValueCaptured);
			System.out.println("\n" + testNameLocal + ": PASS");
		}catch(AssertionError aE) {
			System.out.println("\n" + testNameLocal + ": FAIL");
			System.out.println(testNameLocal + ": expected \'" + validEntry + "\'");
			System.out.println(testNameLocal + ": captured \'" + fieldValueCaptured + "\'");
			throw aE;
		}finally { 
			pageUnderTest = null;
			fieldUnderTest = null;
			fieldValueCaptured = null;
			testNameLocal = null;
			pageUnderTest = null;
		}
	}	
	
	
	
	/**
	 * Silent truncation after position 40. Boundary testing.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testTruncation(Method method) {
		
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		
		driver = getPreparedDriver(testNameLocal);
		
		driver.navigate().to(FV_BasicInfoTests.getStartingURL());
		
		FV_BasicInfoTests.commonWaitInSetup(driver);

		BasicInfoPage pageUnderTest = new BasicInfoPage(driver);
		
		HashMap<Integer,String> iDS = new HashMap<Integer,String>();
		iDS.put(38, "bfC19052a0123456789QWeRTYabpporetz0900");		// 38
		iDS.put(40, "bfC19052a0123456789QWeRTYabpporetz0900b2");	// 40
		iDS.put(43, "bfC19052a0123456789QWeRTYabpporetz0900b2c6R");	// 43

		pageUnderTest = new BasicInfoPage(driver);
		
		pageUnderTest.getFieldDriverLicense().sendKeys(iDS.get(38)); 
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		
		StringBuilder captured = new StringBuilder(pageUnderTest.getFieldDriverLicense().getAttribute("value"));
		
		try { 
			Assert.assertEquals(captured.toString(), iDS.get(38));
			System.out.println("\n" + testNameLocal + ": PASS short of boundary");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ": FAIL short of boundary");
			System.out.println(testNameLocal + ": expected \'" + iDS.get(38) + "\'");
			System.out.println(testNameLocal + ": captured \'" + captured.toString() + "\'");
			throw aE;
		}finally { 
			captured.delete(0, captured.length());
			captured = null;
		}	
		
		pageUnderTest.getFieldDriverLicense().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldDriverLicense().sendKeys(iDS.get(40)); 
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		
		captured = new StringBuilder(pageUnderTest.getFieldDriverLicense().getAttribute("value"));
		
		try { 
			Assert.assertEquals(captured.toString(), iDS.get(40));
			System.out.println("\n" + testNameLocal + ": PASS at boundary");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ": FAIL at boundary");
			System.out.println(testNameLocal + ": expected \'" + iDS.get(40) + "\'");
			System.out.println(testNameLocal + ": captured \'" + captured.toString() + "\'");
			throw aE;
		}finally { 
			captured.delete(0, captured.length());
			captured = null;
		}
		
		pageUnderTest.getFieldDriverLicense().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldDriverLicense().sendKeys(iDS.get(43)); 
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		
		captured = new StringBuilder(pageUnderTest.getFieldDriverLicense().getAttribute("value"));
		
		try { 
			Assert.assertEquals(captured.toString(), iDS.get(40));
			System.out.println("\n" + testNameLocal + ": PASS at boundary plus three");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ": FAIL at boundary plus three");
			System.out.println(testNameLocal + ": expected \'" + iDS.get(40) + "\'");
			System.out.println(testNameLocal + ": captured \'" + captured.toString() + "\'");
			throw aE;
		}finally { 
			captured.delete(0, captured.length());
			captured = null;
			iDS = null;
			testNameLocal = null;
			pageUnderTest = null;
		}
	}	
	

	/**
	 * No government-issued ID is valid as all zero.
	 * <p>
	 * Note to tester: Details pending. New bug found. I'm stubbing this out 
	 * for now. Let's build this test out, and flip it to enabled=true, once 
	 * we get the details of the fix.
	 * <p>
	 * @param method
	 */
	@Test(enabled=false)
	public void testAllZeroDisallowed(Method method) {
		
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		
		driver = getPreparedDriver(testNameLocal);
		
		driver.navigate().to(FV_BasicInfoTests.getStartingURL());
		
		FV_BasicInfoTests.commonWaitInSetup(driver);

		BasicInfoPage pageUnderTest = new BasicInfoPage(driver);

		try { 
			System.out.println(testNameLocal + ":\tThis test isn't done yet");
		}catch(Throwable inRealWorldThisIsNonsense) { 
			throw new IllegalStateException(testNameLocal + ":\tWe need details from UX");
		}
		
		String illegalEntry = "000000000"; // nine
				
		pageUnderTest = new BasicInfoPage(driver);
		
		driver.manage().window().maximize();
		
		WebElement fieldUnderTest = pageUnderTest.getFieldDriverLicense();
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		jse = null;
		
		fieldUnderTest.sendKeys(illegalEntry);
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal); // the application under test does not mask the value immediately
		
		String errorMessageExpected = "Hey dude just chill, everybody let's just chill";
		String errorMessageCaptured = driver.findElement(By.xpath("something")).getText();
		
		try { 
			Assert.assertEquals(errorMessageExpected, errorMessageCaptured);
		}catch(AssertionError aE) {
			throw aE;
		}finally { 
			pageUnderTest = null;
			fieldUnderTest = null;
			errorMessageExpected = null;
			errorMessageCaptured = null;
			testNameLocal = null;
		}
	}	
	
	
	
	/**
	 * If subclass of TestBaseSauce, pass the test result up to the Sauce Labs Dashboard.
	 * <p>
	 * Quit driver.
	 * <p>
	 * Delete the cookie this test just created.
	 */
	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {
		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		if(this.getClass().getSuperclass().getSimpleName().equals("TestBaseSauce")) { 
			System.out.println(methodNameLocal + ":\tSauce driver type detected; Sauce-specific teardown up next...");
			((JavascriptExecutor) driver).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
		} else { 
			System.out.println(methodNameLocal + ":\tLocal driver type detected; Sauce-specific teardown skipped intentionally.\n");
		}
		driver.quit();
		System.out.println(methodNameLocal + ":\tEND tearDown.");
	}

}
