package fieldvalidations.basicinfopage;

import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import common.FieldValidationUtilities;
import pages.BasicInfoPage;
import utilities.saucelabs.DriverTypeSauce;
import utilities.saucelabs.TestBaseSauce;
import waiter.Waiter;

/**
 * TestLink path:
 * <p>
 * General | Running | ECOM_UI | ECOM_FieldValidations | G-3872:Field validation rules: Basic Info: DOB
 * <p>
 * Sample direct nav URL:
 * <p>
 * https://vdc-qaswebapp12.stormwind.local/eComUI/#/apply/basic-info
 * <p>
 * Run from: src/test/resources/TestNGSuiteFiles/FieldValidations/FieldValidations.xml
 * <p>
 * Version 1.1 2019-01-25
 * <p>
 * Version 1.2 2019-04-19 abstract out common behavior and tune for performance
 * <p>
 * @author <a href="mailto:jay.loew@progleasing.com">Allen Loew</a>
 * @author <a href="mailto:christopher.rains@progleasing.com">CJ Rains</a>
 * @version 1.2
 */
public class G_3872_DOB1 extends TestBaseSauce {

	private WebDriver driver;
	


	/**
	 * Happy-path placeholders in DOB field: MM DD YYYY.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testDOBHappyPlaceholders(Method method) {
		
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
						
		driver = getPreparedDriver(testNameLocal);
				
		driver.navigate().to(FV_BasicInfoTests.getStartingURL());
		
		FV_BasicInfoTests.commonWaitInSetup(driver);

		BasicInfoPage pageUnderTest = new BasicInfoPage(driver);

		pageUnderTest.getFieldDate_MM().click();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		StringBuilder placeholderCaptured = new StringBuilder(pageUnderTest.getFieldDate_MM().getAttribute("placeholder"));

		try { 
			Assert.assertEquals(placeholderCaptured.toString(), "MM");
			System.out.println("\n" + testNameLocal + ":\tMM placeholder expected and found");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL MM placeholder: expected \'MM\', captured \'" + placeholderCaptured.toString() + "\'");
			throw aE;
		}finally { 
			placeholderCaptured.delete(0, placeholderCaptured.length());
		}

		pageUnderTest.getFieldDate_DD().click();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		placeholderCaptured.append(pageUnderTest.getFieldDate_DD().getAttribute("placeholder"));

		try { 
			Assert.assertEquals(placeholderCaptured.toString(), "DD");
			System.out.println("\n" + testNameLocal + ":\tDD placeholder expected and found");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL DD placeholder: expected \'DD\', captured \'" + placeholderCaptured.toString() + "\'");
			throw aE;
		}finally { 
			placeholderCaptured.delete(0, placeholderCaptured.length());
		}

		pageUnderTest.getFieldDate_YYYY().click();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		placeholderCaptured.append(pageUnderTest.getFieldDate_YYYY().getAttribute("placeholder"));

		try { 
			Assert.assertEquals(placeholderCaptured.toString(), "YYYY");
			System.out.println("\n" + testNameLocal + ":\tYYYY placeholder expected and found");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL YYYY placeholder: expected \'YYYY\', captured \'" + placeholderCaptured.toString() + "\'");
			throw aE;
		}finally { 
			placeholderCaptured.delete(0, placeholderCaptured.length());
			placeholderCaptured = null;		
			testNameLocal = null;
			pageUnderTest = null;
		}
	}


	/**
	 * Happy-path entry of 8 digits that would compose a valid date.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testDOBHappy8DigitsCheckmark(Method method) {

		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		
		driver = getPreparedDriver(testNameLocal);
				
		driver.navigate().to(FV_BasicInfoTests.getStartingURL());
		
		FV_BasicInfoTests.commonWaitInSetup(driver);

		BasicInfoPage pageUnderTest = new BasicInfoPage(driver);

		pageUnderTest.enterDate("09", "10", "1994");

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		pageUnderTest.getFieldNameLast().click();

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		WebElement checkmarkTemp = pageUnderTest.getDOBHappyCheckbox();
		
		try { 
			Assert.assertTrue(checkmarkTemp.isDisplayed());
			System.out.println("\n" + testNameLocal + ":\tPASS - happy path checkmark expected and found for valid date");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + "FAIL - expected green checkmark is missing");
			throw aE;
		}finally { 
			checkmarkTemp = null;
			testNameLocal = null;
			pageUnderTest = null;
		}
	}


	/**
	 * "Accidentally" enter 10 digits (2 too many). Ensure truncation and checkmark.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testDOB10DigitsTruncateCheckmark(Method method) {

		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		
		driver = getPreparedDriver(testNameLocal);
		
		driver.navigate().to(FV_BasicInfoTests.getStartingURL());
		
		FV_BasicInfoTests.commonWaitInSetup(driver);

		BasicInfoPage pageUnderTest = new BasicInfoPage(driver);

		pageUnderTest.enterDate("09", "10", "199418"); // year is 2 digits over

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		pageUnderTest.getFieldNameLast().click();

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		WebElement checkmarkTemp = driver.findElement(By.cssSelector("#dob .material-icons:nth-child(2)"));
		if(null==checkmarkTemp) { 
			checkmarkTemp = driver.findElement(By.xpath("//i[2]"));
		}

		try { 
			Assert.assertTrue(checkmarkTemp.isDisplayed());
			System.out.println("\n" + testNameLocal + ":\tPASS - happy path checkmark expected and found");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + "FAIL - expected green checkmark is missing");
			throw aE;
		}finally { 
			testNameLocal = null;
			pageUnderTest = null;
		}
	}




	/**
	 * Enter an illegal day. Verify error message.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testBadDay(Method method) {

		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		
		driver = getPreparedDriver(testNameLocal);
		
		Waiter waiter = new Waiter();
		
		waiter.get(FV_BasicInfoTests.getStartingURL(), driver, 60);

		BasicInfoPage pageUnderTest = new BasicInfoPage(driver);

		pageUnderTest.getFieldDate_MM().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldDate_MM().sendKeys("09"); // any legal month
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		pageUnderTest.getFieldDate_DD().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldDate_DD().sendKeys("32"); // any illegal day
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		pageUnderTest.getFieldDate_YYYY().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldDate_YYYY().sendKeys("1991"); // any legal year
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		pageUnderTest.getFieldNameFirst().click();

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		jse = null;

		String captured = pageUnderTest.getDOBErrorMessageDD().getText();
		String expected = "The day you added is invalid for the entered month.";

		try { 
			Assert.assertEquals(captured, expected); 
			System.out.println("\n" + testNameLocal + ":\t\tPASS");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			System.out.println(testNameLocal + ":\texpected \'" + expected + "\'");
			System.out.println(testNameLocal + ":\tcaptured \'" + captured + "\'");
			throw aE;
		}finally { 
			captured = null;
			expected = null;
			testNameLocal = null;
			pageUnderTest = null;
			waiter = null;
		}
	}	



	/**
	 * Enter an illegal month. Verify error message.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testBadMonth(Method method) {

		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		
		DriverTypeSauce original = getDriverTypeForTestClass();
		
		// this one requires Chrome (Mac or WinTel)
		// we will un-set it after this test runs
		setDriverTypeForTestClass(DriverTypeSauce.WINDOWS_CHROME);
		
		driver = getPreparedDriver(testNameLocal);

		driver.navigate().to(FV_BasicInfoTests.getStartingURL());
		
		FV_BasicInfoTests.commonWaitInSetup(driver);

		BasicInfoPage pageUnderTest = new BasicInfoPage(driver);

		pageUnderTest.getFieldDate_MM().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldDate_MM().sendKeys("19"); // any illegal month
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		pageUnderTest.getFieldDate_DD().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldDate_DD().sendKeys("09"); // any legal day
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		pageUnderTest.getFieldDate_YYYY().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldDate_YYYY().sendKeys("1991"); // any legal year
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		Actions builder = new Actions(driver);
		builder.sendKeys(Keys.TAB).build().perform();

		FieldValidationUtilities.sleepSecond(testNameLocal);

		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		jse = null;

		String captured = pageUnderTest.getDOBErrorMessageMM().getText();
		String expected = "The month you entered is invalid.";

		try { 
			Assert.assertEquals(captured, expected); 
			System.out.println("\n" + testNameLocal + ":\tPASS");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			System.out.println(testNameLocal + ":\texpected \'" + expected + "\'");
			System.out.println(testNameLocal + ":\tcaptured \'" + captured + "\'");
			throw aE;
		}finally { 
			setDriverTypeForTestClass(original);
			captured = null;
			expected = null;
			builder = null;
			testNameLocal = null;
			pageUnderTest = null;
			original = null;
		}
	}	




	/**
	 * Enter an illegal year. Verify error message.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testBadYear(Method method) {
				
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		
		DriverTypeSauce original = getDriverTypeForTestClass();

		// this one requires Chrome (Mac or WinTel)
		// we will un-set it after this test runs
		setDriverTypeForTestClass(DriverTypeSauce.WINDOWS_CHROME);
		
		driver = getPreparedDriver(testNameLocal);
						
		driver.navigate().to(FV_BasicInfoTests.getStartingURL());
		
		FV_BasicInfoTests.commonWaitInSetup(driver);
		
		BasicInfoPage pageUnderTest = new BasicInfoPage(driver);

		pageUnderTest.getFieldDate_MM().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldDate_MM().sendKeys("07"); // any legal month
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		pageUnderTest.getFieldDate_DD().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldDate_DD().sendKeys("22"); // any legal day
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		pageUnderTest.getFieldDate_YYYY().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldDate_YYYY().sendKeys("198"); // illegal (3-digit) year
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		pageUnderTest.getFieldNameFirst().click();

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		jse = null;

		String captured = pageUnderTest.getDOBErrorMessageYYYY().getText();
		String expected = "The year you entered is invalid.";

		try { 
			Assert.assertEquals(captured, expected); 
			System.out.println("\n" + testNameLocal + ":\tPASS");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			System.out.println(testNameLocal + ":\texpected \'" + expected + "\'");
			System.out.println(testNameLocal + ":\tcaptured \'" + captured + "\'");
			throw aE;
		}finally { 
			setDriverTypeForTestClass(original);
			captured = null;
			expected = null;
			testNameLocal = null;
			pageUnderTest = null;
			original = null;
		}
	}	





	/**
	 * Enter a birth date that would make the user >= 100 years old. Verify error message.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testCenturyOld(Method method) {
		
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		
		driver = getPreparedDriver(testNameLocal);
		
		driver.navigate().to(FV_BasicInfoTests.getStartingURL());
		
		FV_BasicInfoTests.commonWaitInSetup(driver);

		BasicInfoPage pageUnderTest = new BasicInfoPage(driver);

		pageUnderTest.getFieldDate_MM().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldDate_MM().sendKeys("12"); // any legal month
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		pageUnderTest.getFieldDate_DD().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldDate_DD().sendKeys("22"); // any illegal day
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		pageUnderTest.getFieldDate_YYYY().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldDate_YYYY().sendKeys("1918"); // this user is older than company policy allows
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		
		pageUnderTest.getFieldNameFirst().click();

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		jse = null;

		String captured = pageUnderTest.getDOBErrorMessageYYYYCenturyOld().getText();
		String expected = "You must be under 100 years old to apply for a lease.";

		try { 
			Assert.assertEquals(captured, expected); 
			System.out.println("\n" + testNameLocal + ":\tPASS");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			System.out.println(testNameLocal + ":\texpected \'" + expected + "\'");
			System.out.println(testNameLocal + ":\tcaptured \'" + captured + "\'");
			throw aE;
		}finally { 
			captured = null;
			expected = null;
			testNameLocal = null;
			pageUnderTest = null;
		}
	}	
	


	/**
	 * Enter a birth date that would make the user < 18 years old. Verify error message.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testUnderage(Method method) {
		
		// prerequisite: valid month, valid day
		
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		
		driver = getPreparedDriver(testNameLocal);
		
		driver.navigate().to(FV_BasicInfoTests.getStartingURL());
		
		FV_BasicInfoTests.commonWaitInSetup(driver);

		BasicInfoPage pageUnderTest = new BasicInfoPage(driver);
		
		pageUnderTest.getFieldDate_MM().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldDate_MM().sendKeys("01"); // any legal month
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		pageUnderTest.getFieldDate_DD().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldDate_DD().sendKeys("22"); // any illegal day
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		pageUnderTest.getFieldDate_YYYY().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldDate_YYYY().sendKeys("2012"); // this user is younger than company policy allows
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		pageUnderTest.getFieldNameFirst().click();

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		jse = null;

		String captured = pageUnderTest.getDOBErrorMessageYYYYUnderage().getText();
		String expected = "You must be at least 18 years old to apply for a lease.";

		try { 
			Assert.assertEquals(captured, expected); 
			System.out.println("\n" + testNameLocal + ":\tPASS");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			System.out.println(testNameLocal + ":\texpected \'" + expected + "\'");
			System.out.println(testNameLocal + ":\tcaptured \'" + captured + "\'");
			throw aE;
		}finally { 
			captured = null;
			expected = null;
			testNameLocal = null;
			pageUnderTest = null;
		}
	}	
	
	
	/**
	 * If subclass of TestBaseSauce, pass the test result up to the Sauce Labs Dashboard.
	 * <p>
	 * Quit driver.
	 * <p>
	 * Delete the cookie this test just created.
	 */
	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {
		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		if(this.getClass().getSuperclass().getSimpleName().equals("TestBaseSauce")) { 
			System.out.println(methodNameLocal + ":\tSauce driver type detected; Sauce-specific teardown up next...");
			((JavascriptExecutor) driver).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
		} else { 
			System.out.println(methodNameLocal + ":\tLocal driver type detected; Sauce-specific teardown skipped intentionally.\n");
		}
		driver.quit();
		System.out.println(methodNameLocal + ":\tEND tearDown.");
	}

}
