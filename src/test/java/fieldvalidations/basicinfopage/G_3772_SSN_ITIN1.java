package fieldvalidations.basicinfopage;

import java.lang.reflect.Method;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import common.FieldValidationUtilities;
import pages.BasicInfoPage;
import utilities.saucelabs.TestBaseSauce;

/**
 * TestLink path:
 * <p>
 * General | Running | ECOM_UI | ECOM_FieldValidations | G-3772:Field validation rules: Basic Info: SSN / ITIN
 * <p>
 * Sample direct nav URL:
 * <p>
 * https://vdc-qaswebapp12.stormwind.local/eComUI/#/apply/basic-info
 * <p>
 * Run from: src/test/resources/TestNGSuiteFiles/FieldValidations/FieldValidations.xml
 * <p>
 * Version 1.0 2019-01-29
 * <p>
 * Version 1.1 2019-01-30
 * <p>
 * Version 1.2 2019-02-04 add three new happy path cases
 * <p>
 * Version 1.3 2019-03-01 performance enhancements and re-test
 * <p>
 * Version 1.4 2019-04-19 abstract out common behavior and tune for performance
 * <p>
 * @author <a href="mailto:jay.loew@progleasing.com">Allen</a>
 * @author <a href="mailto:christopher.rains@progleasing.com">CJ</a>
 * @version 1.4
 */
public class G_3772_SSN_ITIN1 extends TestBaseSauce {

	private WebDriver driver;
	

	/**
	 * Happy-path entry of 9 digits that would compose a valid SSN or ITIN.
	 * <p>
	 * Assert green checkmark.
	 * <p>
	 * As instructed, and for internal testing, the first digit is a 5.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testSSNHappy9DigitsCheckmark(Method method) {

		String testNameLocal = getClass().getSimpleName() + "." + method.getName();

		driver = getPreparedDriver(testNameLocal);
				
		driver.navigate().to(FV_BasicInfoTests.getStartingURL());
		
		FV_BasicInfoTests.commonWaitInSetup(driver);
		
		BasicInfoPage pageUnderTest = new BasicInfoPage(driver);

		pageUnderTest.enterSSN("551", "98", "1998");

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		pageUnderTest.getFieldNameLast().click();

		FieldValidationUtilities.sleepSecond(testNameLocal);

		WebElement checkmarkTemp = pageUnderTest.getSSNHappyCheckbox();

		try { 
			Assert.assertTrue(checkmarkTemp.isDisplayed());
			System.out.println("\n" + testNameLocal + ":\tPASS");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + "FAIL - expected green checkmark is missing");
			throw aE;
		}finally { 
			checkmarkTemp = null;
			FieldValidationUtilities.refreshBetweenTest(testNameLocal,driver);
			testNameLocal = null;
			pageUnderTest = null;
		}
	}



	/**
	 * Same as above with tab-out, as per TestLink.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testSSNHappy9DigitTabOut(Method method) {

		String testNameLocal = getClass().getSimpleName() + "." + method.getName();

		driver = getPreparedDriver(testNameLocal);
				
		driver.navigate().to(FV_BasicInfoTests.getStartingURL());
		
		FV_BasicInfoTests.commonWaitInSetup(driver);
		
		BasicInfoPage pageUnderTest = new BasicInfoPage(driver);

		pageUnderTest.enterSSN("551", "98", "1998");

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		FieldValidationUtilities.tabOut(testNameLocal, driver);
		
		WebElement checkmarkTemp = pageUnderTest.getSSNHappyCheckbox();

		try { 
			Assert.assertTrue(checkmarkTemp.isDisplayed());
			System.out.println("\n" + testNameLocal + ":\tPASS");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + "FAIL - expected green checkmark is missing");
			throw aE;
		}finally { 
			checkmarkTemp = null;
			FieldValidationUtilities.refreshBetweenTest(testNameLocal,driver);
			testNameLocal = null;
			pageUnderTest = null;
		}
	}


	/**
	 * "Accidentally" enter 10 digits (1 too many). Ensure truncation and checkmark.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testSSN10DigitsTruncateCheckmark(Method method) {

		String testNameLocal = getClass().getSimpleName() + "." + method.getName();

		driver = getPreparedDriver(testNameLocal);
				
		driver.navigate().to(FV_BasicInfoTests.getStartingURL());
		
		FV_BasicInfoTests.commonWaitInSetup(driver);
		
		BasicInfoPage pageUnderTest = new BasicInfoPage(driver);

		pageUnderTest.enterSSN("552", "21", "19920"); // 5 digits where 4 expected; verify truncation

		FieldValidationUtilities.sleepSecond(testNameLocal);

		pageUnderTest.getFieldNameLast().click();

		FieldValidationUtilities.sleepSecond(testNameLocal);

		WebElement checkmarkTemp = pageUnderTest.getSSNHappyCheckbox();

		try { 
			Assert.assertTrue(checkmarkTemp.isDisplayed());
			System.out.println("\n" + testNameLocal + ":\tPASS");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + "FAIL - expected green checkmark is missing");
			throw aE;
		}finally { 
			checkmarkTemp = null;			
			FieldValidationUtilities.refreshBetweenTest(testNameLocal,driver);
			testNameLocal = null;
			pageUnderTest = null;
		}
	}



	/**
	 * Negative values.
	 * <p>
	 * Tab out and assert expected error message.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testIllegalDigits(Method method) {

		String testNameLocal = getClass().getSimpleName() + "." + method.getName();

		String[] illegalValues = { 
				"666123456", /* triple six */
				"000867746", /* triple zero */
				"085454499"  /* leading zero */
		};

		driver = getPreparedDriver(testNameLocal);
				
		driver.navigate().to(FV_BasicInfoTests.getStartingURL());
		
		FV_BasicInfoTests.commonWaitInSetup(driver);
		
		BasicInfoPage pageUnderTest = new BasicInfoPage(driver);

		pageUnderTest.enterSSN(
				illegalValues[0].substring(0, 3),
				illegalValues[0].substring(3, 5),
				illegalValues[0].substring(5, 9)
				); // 666 in area illegal

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		FieldValidationUtilities.tabOut(testNameLocal, driver);
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		jse = null;
				
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		
		String captured = pageUnderTest.getSSNPrimaryErrorMessage().getText();
		String expected = "Please add a valid SSN / ITIN number.";

		try { 
			Assert.assertEquals(captured, expected); 
			System.out.println("\n" + testNameLocal + ":\tPASS - 666 in area illegal");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			System.out.println(testNameLocal + ":\texpected \'" + expected + "\'");
			System.out.println(testNameLocal + ":\tcaptured \'" + captured + "\'");
			throw aE;
		}finally { 
			captured = null;
		}

		pageUnderTest.clearSSN();

		pageUnderTest.enterSSN(
				illegalValues[1].substring(0, 3),
				illegalValues[1].substring(3, 5),
				illegalValues[1].substring(5, 9)
				); // 000 in area illegal

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		FieldValidationUtilities.tabOut(testNameLocal, driver);
		
		captured = pageUnderTest.getSSNPrimaryErrorMessage().getText();

		try { 
			Assert.assertEquals(captured, expected); 
			System.out.println("\n" + testNameLocal + ":\tPASS - 000 in area illegal");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			System.out.println(testNameLocal + ":\texpected \'" + expected + "\'");
			System.out.println(testNameLocal + ":\tcaptured \'" + captured + "\'");
			throw aE;
		}finally { 
			captured = null;
		}

		pageUnderTest.clearSSN();

		pageUnderTest.enterSSN(
				illegalValues[2].substring(0, 3),
				illegalValues[2].substring(3, 5),
				illegalValues[2].substring(5, 9)
				); // leading 0 illegal

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		FieldValidationUtilities.tabOut(testNameLocal, driver);

		captured = pageUnderTest.getSSNPrimaryErrorMessage().getText();

		try { 
			Assert.assertEquals(captured, expected); 
			System.out.println("\n" + testNameLocal + ":\tPASS - leading 0 illegal");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			System.out.println(testNameLocal + ":\texpected \'" + expected + "\'");
			System.out.println(testNameLocal + ":\tcaptured \'" + captured + "\'");
			throw aE;
		}finally { 
			captured = null;
			illegalValues = null;
			FieldValidationUtilities.refreshBetweenTest(testNameLocal,driver);
			testNameLocal = null;
			pageUnderTest = null;
		}
	}




	/**
	 * Positive values. Happy-path scenarios involving 666.
	 * <p>
	 * Tab out and assert expected success message + happy-path green checkmark.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testHappyTripleSix(Method method) {

		String testNameLocal = getClass().getSimpleName() + "." + method.getName();

		String[] legalValues = { 
				"553366628", /* triple six hyphen-separated */
				"556662190", /* triple six hyphen-separated different breakpoint */
				"555123666", /* triple six last three of serial field */
				"555123666"  /* triple six first three of serial field */
		};

		driver = getPreparedDriver(testNameLocal);
				
		driver.navigate().to(FV_BasicInfoTests.getStartingURL());
		
		FV_BasicInfoTests.commonWaitInSetup(driver);
		
		BasicInfoPage pageUnderTest = new BasicInfoPage(driver);

		pageUnderTest.enterSSN(
				legalValues[0].substring(0, 3),
				legalValues[0].substring(3, 5),
				legalValues[0].substring(5, 9)
				); // triple six hyphen-separated 

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		FieldValidationUtilities.tabOut(testNameLocal, driver);

		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		jse = null;
				
		FieldValidationUtilities.sleepSecond(testNameLocal);

		String captured = pageUnderTest.getSSNHappyMessage().getText();
		String expected = "This helps us verify your identity and keep your account safe.";

		try { 
			Assert.assertEquals(captured, expected); 
		}catch(AssertionError aE) { // 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			System.out.println(testNameLocal + ":\texpected \'" + expected + "\'");
			System.out.println(testNameLocal + ":\tcaptured \'" + captured + "\'");
			throw aE;
		}

		try { 
			Assert.assertTrue(pageUnderTest.getSSNHappyCheckbox().isDisplayed());
			System.out.println("\n" + testNameLocal + ":\tPASS - triple six hyphen-separated");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL - happy check mark expected but missing");
			throw aE;
		}finally { 
			captured = null;
		}

		pageUnderTest.clearSSN();


		pageUnderTest.enterSSN(
				legalValues[1].substring(0, 3),
				legalValues[1].substring(3, 5),
				legalValues[1].substring(5, 9)
				); // triple six hyphen-separated different breakpoint

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		FieldValidationUtilities.tabOut(testNameLocal, driver);

		captured = pageUnderTest.getSSNHappyMessage().getText();

		try { 
			Assert.assertEquals(captured, expected); 
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			System.out.println(testNameLocal + ":\texpected \'" + expected + "\'");
			System.out.println(testNameLocal + ":\tcaptured \'" + captured + "\'");
			throw aE;
		}

		try { 
			Assert.assertTrue(pageUnderTest.getSSNHappyCheckbox().isDisplayed());
			System.out.println("\n" + testNameLocal + ":\tPASS - triple six hyphen-separated different breakpoint");
		}catch(AssertionError aE) { 
			System.out.println(testNameLocal + ":\tFAIL - happy check mark expected but missing");
			throw aE;
		}finally { 
			captured = null;
		}

		pageUnderTest.clearSSN();

		pageUnderTest.enterSSN(
				legalValues[2].substring(0, 3),
				legalValues[2].substring(3, 5),
				legalValues[2].substring(5, 9)
				); // triple six last three of serial field

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		FieldValidationUtilities.tabOut(testNameLocal, driver);

		captured = pageUnderTest.getSSNHappyMessage().getText();

		try { 
			Assert.assertEquals(captured, expected); 
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			System.out.println(testNameLocal + ":\texpected \'" + expected + "\'");
			System.out.println(testNameLocal + ":\tcaptured \'" + captured + "\'");
			throw aE;
		}

		try { 
			Assert.assertTrue(pageUnderTest.getSSNHappyCheckbox().isDisplayed());
			System.out.println("\n" + testNameLocal + ":\tPASS - triple six last three of serial field");
		}catch(AssertionError aE) { 
			System.out.println(testNameLocal + ":\tFAIL - happy check mark expected but missing");
			throw aE;
		}finally { 
			captured = null;
		}


		pageUnderTest.clearSSN();

		pageUnderTest.enterSSN(
				legalValues[3].substring(0, 3),
				legalValues[3].substring(3, 5),
				legalValues[3].substring(5, 9)
				); // triple six first three of serial field

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		FieldValidationUtilities.tabOut(testNameLocal, driver);

		captured = pageUnderTest.getSSNHappyMessage().getText();

		try { 
			Assert.assertEquals(captured, expected); 
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			System.out.println(testNameLocal + ":\texpected \'" + expected + "\'");
			System.out.println(testNameLocal + ":\tcaptured \'" + captured + "\'");
			throw aE;
		}

		try { 
			Assert.assertTrue(pageUnderTest.getSSNHappyCheckbox().isDisplayed());
			System.out.println("\n" + testNameLocal + ":\tPASS - triple six first three of serial field");
		}catch(AssertionError aE) { 
			System.out.println(testNameLocal + ":\tFAIL - happy check mark expected but missing");
			throw aE;
		}finally { 
			captured = null;
			legalValues = null;
			FieldValidationUtilities.refreshBetweenTest(testNameLocal,driver);
			testNameLocal = null;
			pageUnderTest = null;
		}
	}


	/**
	 * Positive values. Happy-path scenarios involving range 900 - 999.
	 * <p>
	 * Tab out and assert expected success message + happy-path green checkmark.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testHappyNineHundreds(Method method) { 

		String testNameLocal = getClass().getSimpleName() + "." + method.getName();

		String[] legalValues = { 
				"900123456", /* 900 - 999 are legal */
				"999123456", 
				"935882122"  
		};

		driver = getPreparedDriver(testNameLocal);
		
		driver.navigate().to(FV_BasicInfoTests.getStartingURL());
		
		FV_BasicInfoTests.commonWaitInSetup(driver);

		BasicInfoPage pageUnderTest = new BasicInfoPage(driver);

		pageUnderTest.enterSSN(
				legalValues[0].substring(0, 3),
				legalValues[0].substring(3, 5),
				legalValues[0].substring(5, 9)
				); 

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		FieldValidationUtilities.tabOut(testNameLocal, driver);

		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		jse = null;
		
		FieldValidationUtilities.sleepSecond(testNameLocal);

		String captured = pageUnderTest.getSSNHappyMessage().getText();
		String expected = "This helps us verify your identity and keep your account safe.";

		try { 
			Assert.assertEquals(captured, expected); 
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			System.out.println(testNameLocal + ":\texpected \'" + expected + "\'");
			System.out.println(testNameLocal + ":\tcaptured \'" + captured + "\'");
			throw aE;
		}

		try { 
			Assert.assertTrue(pageUnderTest.getSSNHappyCheckbox().isDisplayed());
			System.out.println("\n" + testNameLocal + ":\tPASS - " + legalValues[0]);
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL - " + legalValues[0]);
			throw aE;
		}finally { 
			captured = null;
		}

		pageUnderTest.clearSSN();


		pageUnderTest.enterSSN(
				legalValues[1].substring(0, 3),
				legalValues[1].substring(3, 5),
				legalValues[1].substring(5, 9)
				); 

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		FieldValidationUtilities.tabOut(testNameLocal, driver);

		captured = pageUnderTest.getSSNHappyMessage().getText();

		try { 
			Assert.assertEquals(captured, expected); 
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			System.out.println(testNameLocal + ":\texpected \'" + expected + "\'");
			System.out.println(testNameLocal + ":\tcaptured \'" + captured + "\'");
			throw aE;
		}

		try { 
			Assert.assertTrue(pageUnderTest.getSSNHappyCheckbox().isDisplayed());
			System.out.println("\n" + testNameLocal + ":\tPASS - " + legalValues[1]);
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL - " + legalValues[1]);
			throw aE;
		}finally { 
			captured = null;
		}

		pageUnderTest.clearSSN();

		pageUnderTest.enterSSN(
				legalValues[2].substring(0, 3),
				legalValues[2].substring(3, 5),
				legalValues[2].substring(5, 9)
				); 

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		FieldValidationUtilities.tabOut(testNameLocal, driver);

		captured = pageUnderTest.getSSNHappyMessage().getText();

		try { 
			Assert.assertEquals(captured, expected); 
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			System.out.println(testNameLocal + ":\texpected \'" + expected + "\'");
			System.out.println(testNameLocal + ":\tcaptured \'" + captured + "\'");
			throw aE;
		}

		try { 
			Assert.assertTrue(pageUnderTest.getSSNHappyCheckbox().isDisplayed());
			System.out.println("\n" + testNameLocal + ":\tPASS - " + legalValues[2]);
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL - " + legalValues[2]);
			throw aE;
		}finally { 
			captured = null;
			legalValues = null;
			FieldValidationUtilities.refreshBetweenTest(testNameLocal,driver);
			testNameLocal = null;
			pageUnderTest = null;
		}		
	}


	/**
	 * Positive values. Happy-path scenarios involving 0000.
	 * <p>
	 * Tab out and assert expected success message + happy-path green checkmark.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testHappyQuadrupleZero(Method method) { 

		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		
		String sSNUnderTest = "553120000";

		driver = getPreparedDriver(testNameLocal);
		
		driver.navigate().to(FV_BasicInfoTests.getStartingURL());
		
		FV_BasicInfoTests.commonWaitInSetup(driver);

		BasicInfoPage pageUnderTest = new BasicInfoPage(driver);

		pageUnderTest.enterSSN(sSNUnderTest.substring(0, 3), sSNUnderTest.substring(3, 5), sSNUnderTest.substring(5, 9));

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		FieldValidationUtilities.tabOut(testNameLocal, driver);

		WebElement checkmarkTemp = pageUnderTest.getSSNHappyCheckbox();

		try { 
			Assert.assertTrue(checkmarkTemp.isDisplayed());
			System.out.println("\n" + testNameLocal + ":\tPASS with \'" + sSNUnderTest + "\'");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + "FAIL with \'" + sSNUnderTest + "\'; expected green checkmark is missing");
			throw aE;
		}finally { 
			checkmarkTemp = null;
			FieldValidationUtilities.refreshBetweenTest(testNameLocal,driver);
			testNameLocal = null;
			sSNUnderTest = null;
			pageUnderTest = null;
		}
	}



	/**
	 * Positive values. Happy-path scenarios involving 000 at two breakpoints.
	 * <p>
	 * Tab out and assert expected success message + happy-path green checkmark.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testHappyTripleZero(Method method) { 

		String testNameLocal = getClass().getSimpleName() + "." + method.getName();

		String[] legalValues = { 
				"553000199", /* Two triple zero values, different breakpoints, both legal. */
				"550002122"  
		};

		driver = getPreparedDriver(testNameLocal);
		
		driver.navigate().to(FV_BasicInfoTests.getStartingURL());
		
		FV_BasicInfoTests.commonWaitInSetup(driver);

		BasicInfoPage pageUnderTest = new BasicInfoPage(driver);

		pageUnderTest.enterSSN(
				legalValues[0].substring(0, 3),
				legalValues[0].substring(3, 5),
				legalValues[0].substring(5, 9)
				); 

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		FieldValidationUtilities.tabOut(testNameLocal, driver);

		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		jse = null;
		

		String captured = pageUnderTest.getSSNHappyMessage().getText();
		String expected = "This helps us verify your identity and keep your account safe.";

		try { 
			Assert.assertEquals(captured, expected); 
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			System.out.println(testNameLocal + ":\texpected \'" + expected + "\'");
			System.out.println(testNameLocal + ":\tcaptured \'" + captured + "\'");
			throw aE;
		}

		try { 
			Assert.assertTrue(pageUnderTest.getSSNHappyCheckbox().isDisplayed());
			System.out.println("\n" + testNameLocal + ":\tPASS - " + legalValues[0]);
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL - " + legalValues[0]);
			throw aE;
		}finally { 
			captured = null;
		}



		pageUnderTest.clearSSN();

		pageUnderTest.enterSSN(
				legalValues[1].substring(0, 3),
				legalValues[1].substring(3, 5),
				legalValues[1].substring(5, 9)
				); 

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		FieldValidationUtilities.tabOut(testNameLocal, driver);

		captured = pageUnderTest.getSSNHappyMessage().getText();

		try { 
			Assert.assertEquals(captured, expected); 
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			System.out.println(testNameLocal + ":\texpected \'" + expected + "\'");
			System.out.println(testNameLocal + ":\tcaptured \'" + captured + "\'");
			throw aE;
		}

		try { 
			Assert.assertTrue(pageUnderTest.getSSNHappyCheckbox().isDisplayed());
			System.out.println("\n" + testNameLocal + ":\tPASS - " + legalValues[1]);
		}catch(AssertionError aE) { 
			System.out.println(testNameLocal + ":\tFAIL - " + legalValues[1]);
			throw aE;
		}finally { 
			captured = null;
			legalValues = null;
			FieldValidationUtilities.refreshBetweenTest(testNameLocal,driver);
			testNameLocal = null;
			pageUnderTest = null;
		}				
	}

	

	/**
	 * If subclass of TestBaseSauce, pass the test result up to the Sauce Labs Dashboard.
	 * <p>
	 * Quit driver.
	 * <p>
	 * Delete the cookie this test just created.
	 */
	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {
		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		if(this.getClass().getSuperclass().getSimpleName().equals("TestBaseSauce")) { 
			System.out.println(methodNameLocal + ":\tSauce driver type detected; Sauce-specific teardown up next...");
			((JavascriptExecutor) driver).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
		} else { 
			System.out.println(methodNameLocal + ":\tLocal driver type detected; Sauce-specific teardown skipped intentionally.\n");
		}
		driver.quit();
		System.out.println(methodNameLocal + ":\tEND tearDown.");
	}
	

}
