package fieldvalidations.basicinfopage;

import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import common.FieldValidationUtilities;
import common.IssuingState;
import pages.BasicInfoPage;
import utilities.saucelabs.TestBaseSauce;

/**
 * TestLink path:
 * <p>
 * General | Running | ECOM_UI | ECOM_FieldValidations | G-3823:Field validation rules: Basic Info: Issuing State
 * <p>
 * Sample direct nav URL:
 * <p>
 * https://vdc-qaswebapp12.stormwind.local/eComUI/#/apply/basic-info
 * <p>
 * Run from: src/test/resources/TestNGSuiteFiles/FieldValidations/FieldValidations.xml
 * <p>
 * This is 2 of 2, and has one primary test: the states enum.
 * <p>
 * <p>
 * Note to tester: This test takes a really long time to run even when all 
 * is well. You may need to temporarily increase the MAX_DURATION value in
 * common.SauceLabsConstants.java.
 * <p>
 * <p> 
 * Version 1.0 2019-02-20
 * <p>
 * Version 1.1 2019-04-19 abstract out common behavior and tune for performance
 * <p>
 * @author <a href="mailto:jay.loew@progleasing.com">Allen</a>
 * @author <a href="mailto:shawn.hartke@progleasing.com">Shawn</a>
 * @version 1.1
 * @see G_3823_State1.java
 * @see G_3823_State3.java
 */
public class G_3823_State2 extends TestBaseSauce {

	private WebDriver driver;


	/**
	 * Happy-path test of type-started, legal state values from the enum. 
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testHappySelctByStateAll(Method method) {

		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		
		driver = getPreparedDriver(testNameLocal);
		
		driver.navigate().to(FV_BasicInfoTests.getStartingURL());
		
		FV_BasicInfoTests.commonWaitInSetup(driver);
		
		BasicInfoPage pageUnderTest = new BasicInfoPage(driver);

		for(IssuingState state : IssuingState.values()) { 

			pageUnderTest = new BasicInfoPage(driver);

			driver.manage().window().maximize();

			pageUnderTest.getFieldIssuingState().clear();
			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

			if(state.properName.equals("N/A")) { 
				pageUnderTest.getFieldIssuingState().sendKeys("N/A");
			}else { 
				pageUnderTest.getFieldIssuingState().sendKeys(state.toString());
			}
			
			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
			
			FieldValidationUtilities.tabOut(testNameLocal, driver);

			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("window.scrollBy(0,250)", "");
			jse = null;

			WebElement temp = null;

			try { 
				temp = driver.findElement(By.cssSelector("#issuing-state .material-icons:nth-child(2)"));
			}catch(NoSuchElementException nSEE) { 
				throw new AssertionError(testNameLocal, nSEE);
			}

			try { 
				Assert.assertTrue(temp.isDisplayed());
				if(state.properName.equals("Alaska")) { 
					System.out.println("\n" + testNameLocal + ": " + state.properName + " looks good");
				} else { 
					System.out.println(testNameLocal + ": " + state.properName + " looks good");
				}
			}catch(AssertionError aE) { 
				System.out.println(testNameLocal + ": FAIL at " + state.properName);
				throw aE;
			}
			
			pageUnderTest.getFieldNameFirst().click();
			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

			pageUnderTest.getFieldIssuingState().click();
			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

			Actions builder = new Actions(driver);
			if(state.properName.equals("N/A")) { 
				builder.sendKeys(Keys.BACK_SPACE).build().perform();
				builder.sendKeys(Keys.BACK_SPACE).build().perform();
				builder.sendKeys(Keys.BACK_SPACE).build().perform();
			} else { 
				builder.sendKeys(Keys.BACK_SPACE).build().perform();
				builder.sendKeys(Keys.BACK_SPACE).build().perform();
			}

			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		} // end enhanced for loop
		System.out.println("\n" + testNameLocal + ": PASS\n");
	}		



	/**
	 * If subclass of TestBaseSauce, pass the test result up to the Sauce Labs Dashboard.
	 * <p>
	 * Quit driver.
	 * <p>
	 * Delete the cookie this test just created.
	 */
	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {
		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		if(this.getClass().getSuperclass().getSimpleName().equals("TestBaseSauce")) { 
			System.out.println(methodNameLocal + ":\tSauce driver type detected; Sauce-specific teardown up next...");
			((JavascriptExecutor) driver).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
		} else { 
			System.out.println(methodNameLocal + ":\tLocal driver type detected; Sauce-specific teardown skipped intentionally.\n");
		}
		driver.quit();
		System.out.println(methodNameLocal + ":\tEND tearDown.");
	}

}
