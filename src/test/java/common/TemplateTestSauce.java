package common;

import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import pages.MarketingPage;
import utilities.saucelabs.DriverTypeSauce;
import utilities.saucelabs.TestBaseSauce;

/**
 * Optional template: TestNG test class, Sauce Labs driver.
 * <p>
 * Local driver type is either inherited from the test base, or overridden.
 * Either way, you get a ready-to-use local driver of a specified type.
 * <p>
 * Because this is a template, I've used more comments and white space 
 * than I otherwise might.
 * <p>
 * For the Sauce Labs drivers, it's typically efficient to do initial 
 * instantiation, navigation, and waiting in each test method, not in 
 * a shared setup. The page objects are likewise created and destroyed 
 * within the test methods. In short, for Sauce Labs tests, it's best 
 * to make more atomic test methods.
 * <p>
 * @author <a href="mailto:jay.loew@progleasing.com">Allen</a>
 * @version 1.0
 * @see TestBaseSauce.java
 */
public class TemplateTestSauce extends TestBaseSauce {

	private WebDriver driver;

	
	/**
	 * Test something.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true,priority=1)
	public void testSomething(Method method) {

		String testNameLocal = getClass().getSimpleName() + "." + method.getName();

		/*
		 * Optionally, you can make this call to super first, overriding 
		 * the currently declared default in the test base.
		 */
		super.setDriverTypeForTestClass(DriverTypeSauce.WINDOWS_FIREFOX);

		/*
		 * The line below will fetch a local driver of whatever type is 
		 * currently active in the superclass TestBaseSauce.java, and 
		 * if you haven't explicitly set something above.
		 */
		driver = getPreparedDriver(testNameLocal);
		
		/*
		 * Initial navigation required of test methods in this test class.
		 */
		driver.navigate().to(ProgressiveLeasingConstants.URL_WELCOME_12);
		
		/*
		 * Defensive programming for Magento and test environments.
		 * Wait for an expected field.
		 */
		try { 
			new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.id("exit-button")));
		}catch(TimeoutException tE) { 
			throw new AssertionError(testNameLocal + ":\tfast-fail", tE);
		}

		/*
		 * Now that it's safe to do so, instantiate the page object these 
		 * tests will need.
		 */
		MarketingPage pageUnderTest = new MarketingPage(driver);

		// do something with the page object
		pageUnderTest.getButtonGet_started().click();

		// do something with the utilities (optional helper methods)
		FieldValidationUtilities.sleepSecond(testNameLocal);
		
		try { 
			driver.findElement(By.id("pg-field-contact")).sendKeys("122345");
			FieldValidationUtilities.sleepSecond(testNameLocal);
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError("\n" + testNameLocal + "FAIL with " + anyExceptionWhatsoever.getClass().getSimpleName(),anyExceptionWhatsoever);
		}finally { 
			testNameLocal = null;
			pageUnderTest = null;
		}

	}
	

	/**
	 * If subclass of TestBaseSauce, pass the test result up to the Sauce Labs Dashboard.
	 * <p>
	 * Quit driver.
	 * <p>
	 * Delete the cookie this test just created.
	 */
	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {
		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		if(this.getClass().getSuperclass().getSimpleName().equals("TestBaseSauce")) { 
			System.out.println(methodNameLocal + ":\tSauce driver type detected; Sauce-specific teardown up next...");
			((JavascriptExecutor) driver).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
		} else { 
			System.out.println(methodNameLocal + ":\tLocal driver type detected; Sauce-specific teardown skipped intentionally.\n");
		}
		driver.quit();
		System.out.println(methodNameLocal + ":\tEND tearDown.");
	}
}
