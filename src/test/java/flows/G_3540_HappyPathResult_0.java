package flows;

import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import common.FieldValidationUtilities;
import common.ProgressiveLeasingConstants;
import pages.BankInfoPage;
import pages.BasicInfoPage;
import pages.ContactInfoPage;
import pages.EnterTheCodePage;
import pages.GetStartedRequestCodePage;
import pages.MarketingPage;
import utilities.CommonDriversLocal;
import utilities.DriverTypeLocal;
import utilities.ServiceTests;
import waiter.Waiter;

/**
 * ECOM_BasicBeginToResults | G-3450: Approval ResultType 0
 * <p>
 * http://slc-qaswebtli01/testlink/linkto.php?tprojectPrefix=G&item=testcase&id=G-3540
 * <p>
 * Happy path application through to approval.
 * <p>
 * Deprecate 2019-04-11: redundant, and we don't want to re-use this data.
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @author <a href="mailto:christopher.rains@progleasing.com">CJ Rains</a>
 * @version 1.0 2019-01-08
 * @deprecated
 * @see ECOMEndToEnd.java
 */
public class G_3540_HappyPathResult_0 {

	/**
	 * Must run POSTMAN calls as per 
	 * <a href="http://slc-qaswebtli01/testlink/linkto.php?tprojectPrefix=G&item=testcase&id=G-3540">test case</a> 
	 * and fetch this value first. You need a fresh session ID.
	 * <p>
	 * Path in POSTMAN:
	 * Collections
	 * ECommerceFoundation.API - Service Tests
	 * Create session, blank info
	 * 1) Create Bearer Token
	 * 2) Create Initial Session Through API
	 * Run the above two back-to-back, then copy "sessionID": value and paste below.
	 */
	private static final String SESSION_ID;

	private static final String uRL;

	static {

		SESSION_ID = ServiceTests.getSessionInitial();

		uRL = ProgressiveLeasingConstants.URL_WELCOME_13 + "?sid="
				+ G_3540_HappyPathResult_0.SESSION_ID
				+ "&returnShoppingRedirect=%2F&successRedirect=https:%2F%2Fgoogle.com";
	}

	private Waiter waiter;

	private static final int TIMEOUT_MAX_SECONDS = 8;

	@Test(enabled=false)
	public void test3450Happy(Method method) throws InterruptedException {

		String testNameLocal = new Exception().getStackTrace()[0].getMethodName();

		WebDriver driver = new CommonDriversLocal(DriverTypeLocal.CHROME).getPreparedDriver();

//		driver.manage().deleteAllCookies();

		waiter = new Waiter();

		driver.navigate().to(uRL); // The starting URL for this test lands on the marketing page.
		waiter.get(uRL,driver,TIMEOUT_MAX_SECONDS);


		// The starting URL for this test lands on the marketing page.
		driver.navigate().to(G_3540_HappyPathResult_0.uRL);

		//Marketing Page object
		MarketingPage marketingPage = new MarketingPage(driver);
		marketingPage.getButtonGet_started().click();

		//Request_Code Page object
		GetStartedRequestCodePage requestCodePage = new GetStartedRequestCodePage(driver);
		try {
			new WebDriverWait(driver, TIMEOUT_MAX_SECONDS).until(ExpectedConditions.presenceOfElementLocated(By.id("pg-field-contact")));
		} catch (TimeoutException tE) {
			throw new TimeoutException(testNameLocal, tE);
		}
		requestCodePage.happyPathRequestCode("000@test.com");
		marketingPage = null;

		//Enter_The_Code Page object
		EnterTheCodePage enterTheCode = new EnterTheCodePage(driver);
		try {
			new WebDriverWait(driver, TIMEOUT_MAX_SECONDS).until(ExpectedConditions.presenceOfElementLocated(By.id("pg-field-code")));
		} catch(TimeoutException tE2) {
			throw new TimeoutException(testNameLocal, tE2);
		}
		enterTheCode.enterThisCode("000000");
		enterTheCode = null;

		//Basic_Info_Page Object
		BasicInfoPage basicInfoPage = new BasicInfoPage(driver);
		try {
//			new WebDriverWait(driver, TIMEOUT_MAX_SECONDS).until(webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
			new WebDriverWait(driver, TIMEOUT_MAX_SECONDS).until(ExpectedConditions.elementToBeClickable(By.id("first-name-input")));
		} catch(TimeoutException tE3)  {
			throw new TimeoutException(testNameLocal, tE3);
		}
		Thread.sleep(3000);

		basicInfoPage.enterName("Apollo", "Cardoza");
		FieldValidationUtilities.sleepQuarterSecond(method.getName());
		basicInfoPage.enterDate("05", "26", "1983");
		FieldValidationUtilities.sleepQuarterSecond(method.getName());
		basicInfoPage.enterSSN("511", "02", "0146");
		FieldValidationUtilities.sleepQuarterSecond(method.getName());
		basicInfoPage.enterDriverLicense("123456");
		FieldValidationUtilities.sleepQuarterSecond(method.getName());
		basicInfoPage.selectTwoLetterState("UT");
		FieldValidationUtilities.sleepQuarterSecond(method.getName());
		basicInfoPage.getButtonContinue().click();
		basicInfoPage = null;

		//Contact_Info_Page object
		ContactInfoPage contactInfoPage = new ContactInfoPage(driver);
		try {
			new WebDriverWait(driver, TIMEOUT_MAX_SECONDS).until(ExpectedConditions.elementToBeClickable(By.id("input-home-address-street1")));
		} catch(TimeoutException tE4) {
			throw new TimeoutException(testNameLocal, tE4);
		}
		String[] validUT = {"000@test.com", "8015555555", "123 Fake St", null, "Draper", "UT", "84020"};
		contactInfoPage.happyPathPopulate(validUT);
		new WebDriverWait(driver, TIMEOUT_MAX_SECONDS).until(ExpectedConditions.elementToBeClickable(By.cssSelector(".positive")));
		contactInfoPage.getButtonContinue_to_bank_info().click();
		validUT = null;
		contactInfoPage = null;
		
		//Bank_Info_Page object
		BankInfoPage bankInfoPage = new BankInfoPage(driver);
		try {
			new WebDriverWait(driver, TIMEOUT_MAX_SECONDS).until(ExpectedConditions.elementToBeClickable(By.id("input-routing-number")));
		} catch(TimeoutException tE5) {
			throw  new TimeoutException(testNameLocal, tE5);
		}
		bankInfoPage.enterRoutingNumber("000000000");
		FieldValidationUtilities.sleepQuarterSecond(method.getName());
		bankInfoPage.enterAccountNumber("12345");
		FieldValidationUtilities.sleepQuarterSecond(method.getName());
		bankInfoPage.enterMonthlyIncome("3212");
		FieldValidationUtilities.sleepQuarterSecond(method.getName());
		new WebDriverWait(driver, TIMEOUT_MAX_SECONDS).until(ExpectedConditions.elementToBeClickable(By.cssSelector(".positive")));
		bankInfoPage.getButtonContinue_to_apply().click();
		bankInfoPage = null;
		
		//Submit_Application Page
		try {
			new WebDriverWait(driver, TIMEOUT_MAX_SECONDS).until(ExpectedConditions.textToBe(By.cssSelector("div.page-title"), "Submit your application"));
		} catch(TimeoutException tE6) {
			throw new TimeoutException(testNameLocal, tE6);
		}
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("scroll(0, 250);");
		new WebDriverWait(driver, TIMEOUT_MAX_SECONDS).until(ExpectedConditions.elementToBeClickable(By.cssSelector("button.pg-button.primary")));
		driver.findElement(By.cssSelector("button.pg-button.primary")).click();

		//Result Page
		try {
			new WebDriverWait(driver, TIMEOUT_MAX_SECONDS).until(ExpectedConditions.textToBe(By.className("page-title"), "You're approved!"));
		} catch(TimeoutException tE7) {
			throw new TimeoutException(testNameLocal, tE7);
		}
		StringBuilder sBuild = new StringBuilder(driver.getPageSource());
		try {
			Assert.assertTrue(sBuild.toString().contains("Lease ID") && !sBuild.toString().contains("Lease ID 0"));
			System.out.println(testNameLocal + ":\tPASS!");
		}catch(AssertionError aE) {
			System.out.println("WARNING could not determine Lease ID at Result page OR Lease ID == 0");
			throw new AssertionError(testNameLocal, aE);
		}
		finally {
			driver.quit();
		}
	}
}

