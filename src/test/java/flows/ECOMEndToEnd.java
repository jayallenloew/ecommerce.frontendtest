/*
 * Copyright Progressive Leasing LLC.
 */
package flows;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import common.FieldValidationUtilities;
import common.ProgressiveLeasingConstants;
import pages.BankInfoPage;
import pages.BasicInfoPage;
import pages.ContactInfoPage;
import pages.GetStartedRequestCodePage;
import pages.MarketingPage;
import pages.StoreDemoLandingPage;
import utilities.ResetThisUser;
import utilities.saucelabs.DriverTypeSauce;
import utilities.saucelabs.TestBaseSauce;

/**
 * 
 * An end-to-end test that hits the 90+ percent of ECOM's responsibility.
 * <p>
 * This test begins at store demo and ends at lease approval with lease amount.
 * <p>
 * Test doesn't (yet) persist the lease amount (expected to be $1000 in QA) or 
 * the lease ID (a system-generated integer).
 * <p>
 * Primary assertion is successRedirect in ending URL.
 * <p>
 * Version 1.0 2019-04-08
 * <p>
 * Version 2.0 data cleanup thanks to Shawn!
 * <p>
 * Version 2.1:
 * <ul>
 * <li>New IDs thanks to Shawn!</li>
 * <li>In setup, exception rather than assertion (better practice)</li>
 * <li>A 2nd chance to capture a thorny ID</li>
 * </ul>
 * <p>
 * @author <a href="mailto:Shawn.Hartke@progleasing.com">Shawn</a>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen</a>
 * @author <a href="mailto:christopher.rains@progleasing.com">CJ</a>
 * @version 2.1
 */
public class ECOMEndToEnd extends TestBaseSauce {

	private WebDriver driver;

	/**
	 * In a couple places, this test will require a test email address.
	 * <p>
	 * Four known valid possibilities are stored in an array in ProgressiveLeasingConstants.java.
	 */
	public static final String EMAIL_FOR_THIS_TEST = 
			ProgressiveLeasingConstants.TEST_DOT_COM_ADDRESSES[1];

	@BeforeMethod
	public void setupNotStatic() { 

		// reset the user to be used
		@SuppressWarnings("unused")
		ResetThisUser resetTestAccount = new ResetThisUser(EMAIL_FOR_THIS_TEST);
		resetTestAccount = null;

	}

	@Test
	public void testECOMEndToEnd() {

		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();


		//		if Sauce
		//		optional: override whatever the default driver type is currently set to in TestBaseSauce.java
		setDriverTypeForTestClass(DriverTypeSauce.WINDOWS_CHROME);

		//		if Local
		//		optional: override whatever the default driver type is currently set to in TestBaseLocal.java
		//				setDriverTypeForTestClass(DriverTypeLocal.CHROME);


		// instantiate the driver
		driver = getPreparedDriver(methodNameLocal);

		// exception and early-out if driver wasn't instantiated for any reason
		SessionId session = ((RemoteWebDriver)driver).getSessionId();
		if(null==session) {
			throw new IllegalStateException(methodNameLocal + ":\t" + "driver instantiation failure");
		}else { 
			System.out.println(methodNameLocal + ":\t" + "driver instantiated... (OK)");
		}

		//		try { 
		//			Assert.assertNotNull(driver);
		//			System.out.println(methodNameLocal + ":\t" + "driver instantiated... (OK)");
		//		}catch(AssertionError aE) { 
		//			throw aE;
		//		}

		// go to demo page
		driver.navigate().to(ProgressiveLeasingConstants.URL_STORE_DEMO);

		// wait for Find out more button (navigation success)
		try { 
			new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//img[@alt='Learn more about Progressive Leasing']")));
			System.out.println(methodNameLocal + ":\tstore demo landing page... (OK)");
		}catch(TimeoutException tE) { 
			throw new AssertionError((methodNameLocal + ":\tfast-fail in setup; check store demo page."), tE);
		}

		// instantiate store demo page
		StoreDemoLandingPage demoPageTemp = new StoreDemoLandingPage(driver);

		// click through to marketing page
		demoPageTemp.getButtonFindOutMore().click(); 
		FieldValidationUtilities.sleepSecond(methodNameLocal);

		// force redirect from 12 to 13
		// starting from storedemo (which is required here), you may land on 12
		// if you do land on 12, force redirect
		//		StringBuilder sBuild = new StringBuilder(driver.getCurrentUrl());
		//		if(sBuild.toString().contains("qaswebapp12")) { 
		//			System.out.println(methodNameLocal + ":\tforce redirect onto 13... (OK)");
		//			int indexBegin = sBuild.toString().indexOf("qaswebapp");
		//			sBuild.replace(indexBegin, (indexBegin+11), "qaswebapp13");
		//			driver.navigate().to(sBuild.toString());
		//		} else { 
		//			System.out.println(methodNameLocal + ":\talready on 13; will not redirect... (OK)");
		//		}

		// wait for Get Started button 
		try { 
			new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(By.cssSelector("button.pg-button.cta")));
			System.out.println(methodNameLocal + ":\tmarketing page... (OK)");
		}catch(TimeoutException tE) { 
			throw new AssertionError((methodNameLocal + ":\tfast-fail in setup; check marketing page."), tE);
		}finally { 
			demoPageTemp = null;
			//			sBuild = null;
		}

		MarketingPage marketingTemp = new MarketingPage(driver);

		// click through to get started request code
		marketingTemp.getButtonGet_started().click(); 
		FieldValidationUtilities.sleepSecond(methodNameLocal);

		// wait for Mobile phone or email field 
		try { 
			//			new WebDriverWait(driver, 20).until(ExpectedConditions.presenceOfElementLocated(By.id("otp-contact-input")));
			new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.id("pg-field-contact")));
			System.out.println(methodNameLocal + ":\tget started request code page... (OK)");
		}catch(TimeoutException tE) { 
			throw new AssertionError((methodNameLocal + ":\tfast-fail in setup; check get started request code page."), tE);
		}finally { 
			marketingTemp = null;
		}

		GetStartedRequestCodePage requestCodeTemp = new GetStartedRequestCodePage(driver);

		// enter a test email address or phone
		driver.findElement(By.id("pg-field-contact")).sendKeys(EMAIL_FOR_THIS_TEST);
		FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);

		// accept the terms in order to proceed to enter the code
		requestCodeTemp.getCheckboxTerms().click(); // accept terms
		FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);

		// The continue button here if pretty sketchy so we'll give it two ways
		try { 
			requestCodeTemp.getButtonSendCode_Get_started().click();
			FieldValidationUtilities.sleepSecond(methodNameLocal);
		}catch(Exception anyException) { 
			// deliberate swallow; messaging only
			System.out.println(methodNameLocal + ":\t" + "failed at get started button; 2nd try up next...");
			System.out.println(methodNameLocal + ":\t" + "exception caught:" + anyException.getClass().getSimpleName());
			// after the checkbox, 5 tabs and a space to advance to the next page
			Actions builder = new Actions(driver);
			builder.sendKeys(Keys.TAB).build().perform();
			FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);
			builder.sendKeys(Keys.TAB).build().perform();
			FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);
			builder.sendKeys(Keys.TAB).build().perform();
			FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);
			builder.sendKeys(Keys.TAB).build().perform();
			FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);
			builder.sendKeys(Keys.TAB).build().perform();
			FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);
			builder.sendKeys(Keys.SPACE).build().perform();
			FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);			
			builder = null;
		}finally { 
			requestCodeTemp = null;
		}		

		FieldValidationUtilities.sleepSecond(methodNameLocal);


		// wait for enter the code field 
		try { 
			new WebDriverWait(driver, 20).until(ExpectedConditions.presenceOfElementLocated(By.id("pg-field-code")));
			System.out.println(methodNameLocal + ":\tenter the code page... (OK)");
		}catch(TimeoutException tE) { 
			throw new AssertionError((methodNameLocal + ":\tfast-fail in setup; check enter the code page."), tE);
		}

		// enter the (test) code and take a nap, unfortunately
		driver.findElement(By.id("pg-field-code")).sendKeys("000000");
		FieldValidationUtilities.sleepSecond(methodNameLocal);
		// yeah the sleeps suck, thank Magento, move on....
		System.out.println(methodNameLocal + ":\tcode sent... (OK)");
		try { 
			Thread.sleep(8000); // Don't remove or reduce. It's not our issue.
		}catch(InterruptedException iE) { 
			throw new AssertionError((methodNameLocal + ":\tfast-fail in setup"), iE);
		}



		// wait for basic info page
		try { 
			new WebDriverWait(driver, 90).until(ExpectedConditions.presenceOfElementLocated(By.id("first-name-input")));
			System.out.println(methodNameLocal + ":\tcode received; at basic info page... (OK)");
		}catch(TimeoutException tE) { 
			throw new AssertionError((methodNameLocal + ":\tfast-fail in setup; check basic info page."), tE);
		}
		BasicInfoPage basicTemp = new BasicInfoPage(driver);
		//		basicTemp.enterName("Tester", "Lester");
		//		basicTemp.enterDate("09", "10", "1985");
		//		basicTemp.enterSSN("555", "12", "1212");
		//		basicTemp.enterDriverLicense("12345");
		//		basicTemp.getFieldIssuingState().sendKeys("UT");

		// name first & last
		basicTemp.enterName(ProgressiveLeasingConstants.BASIC_INFO_555121212[0],
				ProgressiveLeasingConstants.BASIC_INFO_555121212[1]);

		// date MM DD YYYY
		basicTemp.enterDate(ProgressiveLeasingConstants.BASIC_INFO_555121212[2],
				ProgressiveLeasingConstants.BASIC_INFO_555121212[3],
				ProgressiveLeasingConstants.BASIC_INFO_555121212[4]);

		// SSN ### ### ####
		basicTemp.enterSSN(ProgressiveLeasingConstants.BASIC_INFO_555121212[5],
				ProgressiveLeasingConstants.BASIC_INFO_555121212[6], 
				ProgressiveLeasingConstants.BASIC_INFO_555121212[7]);

		// driver license or government ID; in QA it's typically a 5-digit integer
		basicTemp.enterDriverLicense(ProgressiveLeasingConstants.BASIC_INFO_555121212[8]);

		// two-letter state or N/A; in QA it's typically UT
		basicTemp.getFieldIssuingState().sendKeys(ProgressiveLeasingConstants.BASIC_INFO_555121212[9]);	

		basicTemp.getButtonContinue().click();
		FieldValidationUtilities.sleepSecond(methodNameLocal);
		FieldValidationUtilities.sleepSecond(methodNameLocal);
		basicTemp = null;

		// wait for contact info page
		try { 
			new WebDriverWait(driver, 60).until(ExpectedConditions.presenceOfElementLocated(By.id("mobile-phone-area-code-input")));
			System.out.println(methodNameLocal + ":\tcontact info page... (OK)");
		}catch(TimeoutException tE) { 
			throw new AssertionError((methodNameLocal + ":\tfast-fail in setup; check contact info page."), tE);
		}
		ContactInfoPage contactTemp = new ContactInfoPage(driver);

		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,300)", "");
		jse = null;

		String[] validUT = {
				EMAIL_FOR_THIS_TEST, 
				ProgressiveLeasingConstants.CONTACT_INFO_84095[0],	// mobile phone
				ProgressiveLeasingConstants.CONTACT_INFO_84095[1],	// address line 1
				ProgressiveLeasingConstants.CONTACT_INFO_84095[2],	// address line 2 (default null) 												// address line 2
				ProgressiveLeasingConstants.CONTACT_INFO_84095[3],	// city
				ProgressiveLeasingConstants.CONTACT_INFO_84095[4],	// state
				ProgressiveLeasingConstants.CONTACT_INFO_84095[5]	// zip
		};

		try { 
			contactTemp.happyPathPopulate(validUT);
			System.out.println(methodNameLocal + ": happy path populate contact info... (OK)");
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError((methodNameLocal + ":\tfast-fail in setup entering contact info"),anyExceptionWhatsoever);
		}
		FieldValidationUtilities.sleepSecond(methodNameLocal);
		contactTemp.getButtonContinue_to_bank_info().click();
		FieldValidationUtilities.sleepSecond(methodNameLocal);
		contactTemp = null;
		validUT = null;

		// wait for banking info page
		try { 
			new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.id("input-routing-number")));
			System.out.println(methodNameLocal + ": banking info page... (OK)");
		}catch(TimeoutException tE) { 
			throw new AssertionError((methodNameLocal + ":\tfast-fail in setup; check banking info page."), tE);
		}

		BankInfoPage bankTemp = new BankInfoPage(driver);
		try { 
			bankTemp.happyPathPopulate();
			System.out.println(methodNameLocal + ": banking info happy path populate complete... (OK)");
		}catch(Exception anyExceptionWhatsoever) { 
			/*
			 * Multiple actions, if any fail, throw assertion error
			 */
			throw new AssertionError(methodNameLocal, anyExceptionWhatsoever);
		}

		try { 
			bankTemp.getButtonContinue_to_apply().click();
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError(methodNameLocal, anyExceptionWhatsoever);
		}

		FieldValidationUtilities.sleepSecond(methodNameLocal);

		jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		jse = null;

		FieldValidationUtilities.sleepSecond(methodNameLocal);

		// wait for Get Started button 
		// we'll give it two chances
		boolean isBankingInfoSuccess = false;
		try { 
			new WebDriverWait(driver, 15).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//pg-app-layout-footer/div/div/button")));
			isBankingInfoSuccess = true;
		}catch(TimeoutException tE) { 
			// deliberate swallow; assertion will occur below
		}

		if(isBankingInfoSuccess) { 
			System.out.println(methodNameLocal + "  second try skipped on purpose.... (OK)");
		} else {
			try { 
				new WebDriverWait(driver, 15).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("button.pg-button.primary")));
				isBankingInfoSuccess = true;
			}catch(TimeoutException tE) { 
				// deliberate swallow; assertion will occur below
			}
		}


		// Apply Now - 
		WebElement buttonApplyNow = null;
		try { 
			buttonApplyNow = driver.findElement(By.cssSelector("button.pg-button.primary"));
		}catch(Exception any) { 
			buttonApplyNow = driver.findElement(By.xpath("//pg-app-layout-footer/div/div/button"));
		}
		buttonApplyNow.click();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		WebElement leaseIDTemp = null;

		try { 
			new WebDriverWait(driver, 60).until(ExpectedConditions.presenceOfElementLocated(By.id("applicant-lease-id")));
			System.out.println(methodNameLocal + "  A Lease ID element was detected on 1st try.... (OK)");
		}catch(TimeoutException tE) { 
			// deliberate swallow
			System.out.println(methodNameLocal + "  one more try for Lease ID element.... (OK)");
		}

		try { 
			leaseIDTemp = driver.findElement(By.id("applicant-lease-id"));
		}catch(Exception anyExceptionWhatsoever) { 
			// deliberate swallow there will be another try
		}

		if(null==leaseIDTemp) { 
			try { 
				new WebDriverWait(driver, 60).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//app-results/div/div/div[3]")));
				System.out.println(methodNameLocal + "  A Lease ID element was detected on 1st try.... (OK)");
			}catch(TimeoutException tE) { 
				throw new AssertionError(methodNameLocal + " second try FAILED; test will fail", tE);
			}

			try { 
				leaseIDTemp = driver.findElement(By.xpath("//app-results/div/div/div[3]"));
			}catch(Exception anyExceptionWhatsoever) { 
				throw new AssertionError(methodNameLocal + " second try FAILED; test will fail", anyExceptionWhatsoever);
			}
		}
		
		// assert Lease ID displayed
		String iDCaptured = null;
		try { 
			iDCaptured = leaseIDTemp.getText();
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError(methodNameLocal, anyExceptionWhatsoever);
		}

		try { 
			Assert.assertTrue(iDCaptured.contains("Lease ID"));
			System.out.println(methodNameLocal + " PASS; captured \'" + iDCaptured + "\'");
		}catch (AssertionError aE) {
			System.out.println(methodNameLocal + " FAIL; expected a Lease ID value here.");
			throw aE;
		}finally { 
			iDCaptured = null;
			leaseIDTemp = null;
		}

		// assert not failure URL
		String captured = driver.getCurrentUrl();
		try { 
			Assert.assertFalse(captured.contains("unable-to-approve"));
		}catch(AssertionError aE) { 
			System.out.println(methodNameLocal + " FAIL; URL captured '" + captured + "\'");
			throw aE;
		}finally { 
			captured = null;
		}



	}


	/**
	 * If subclass of TestBaseSauce, pass the test result up to the Sauce Labs Dashboard.
	 * <p>
	 * Quit driver.
	 * <p>
	 * Delete the cookie this test just created.
	 */
	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {
		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		if(this.getClass().getSuperclass().getSimpleName().equals("TestBaseSauce")) { 
			System.out.println(methodNameLocal + ":\tSauce driver type detected; Sauce-specific teardown up next...");
			((JavascriptExecutor) driver).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
		} else { 
			System.out.println(methodNameLocal + ":\tLocal driver type detected; Sauce-specific teardown skipped intentionally.\n");
		}
		driver.quit();
		System.out.println("\n" + methodNameLocal + ":\tEND tearDown.");
	}



}
