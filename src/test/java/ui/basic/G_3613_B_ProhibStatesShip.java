package ui.basic;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;

import fieldvalidations.contactinfopage.FV_ContactInfoTests;
import pages.ContactInfoPage;
import utilities.saucelabs.TestBaseSauce;

/**
 * TestLink path:
 * <p>
 * General | Running | ECOM_UI | ECOM_BasicBeginToResults | G-3613:Ship To Address State
 * <p>
 * *** THIS IS PART 2 of 2 ***
 * <p>
 * Customers in CO, HI, IA, NE, SC, and WY should see an error message.
 * <p>
 * We can't ship leased products to these states.
 * <p>
 * PART 1 concerns leasing; we can't create a lease for customers in those states.
 * <p>
 * Version 1.0 2019-05-06
 * <p>
 * *** TBD ***
 * <p>
 * @author <a href="mailto:jay.loew@progleasing.com">Allen</a>
 * @author <a href="mailto:shawn.hartke@progleasing.com">Shawn</a>
 * @author <a href="mailto:christopher.rains@progleasing.com">CJ Rains</a>
 * @version 1.0
 * @see G_3613_A_ProhibStatesLease
 */
public class G_3613_B_ProhibStatesShip extends TestBaseSauce {

	
	private WebDriver driver;
	private ContactInfoPage pageUnderTest;

	
	private void setUpNotStatic(String testNameIn) {

		driver = getPreparedDriver(testNameIn);
				
		driver.navigate().to(FV_ContactInfoTests.getStartingURL());
		
		FV_ContactInfoTests.commonWaitInSetup(driver);
		
		pageUnderTest = new ContactInfoPage(driver);		
	}

	
//	@Test(enabled=true)
//	public void testProhibitedStateWI() {
//		
//		String testNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
//
//		setUpNotStatic(testNameLocal);
//		
//		String[] WI = {"000@test.com", "6082621408", "1220 Linden Dr", null, "Madison", "WI", "53706"};	
//
//		try { 
//			Assert.assertTrue(testProhibitedState(WI, IssuingState.WI));
//		}catch(AssertionError aE) { 
//			throw aE;
//		}
//	}
//	
//	
//	@Test(enabled=true)
//	public void testProhibitedStateMN() {
//
//		String testNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
//
//		setUpNotStatic(testNameLocal);
//
//		String[] MN = {"000@test.com", "2187304394", "411 West First St", null, "Duluth", "MN", "55802"};	
//		
//		try { 
//			Assert.assertTrue(testProhibitedState(MN, IssuingState.MN));
//		}catch(AssertionError aE) { 
//			throw aE;
//		}
//	}
//
//	
//	@Test(enabled=true)
//	public void testProhibitedStateNJ() {
//
//		String testNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
//
//		setUpNotStatic(testNameLocal);
//
//		String[] NJ = {"000@test.com", "6099893532", "319 East State St", null, "Trenton", "NJ", "08608"};	
//		
//		try { 
//			Assert.assertTrue(testProhibitedState(NJ, IssuingState.NJ));
//		}catch(AssertionError aE) { 
//			throw aE;
//		}
//	}
//
//	
//	@Test(enabled=true)
//	public void testProhibitedStateVT() {
//
//		String testNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
//
//		setUpNotStatic(testNameLocal);
//
//		String[] MN = {"000@test.com", "8028657000", "149 Church St", null, "Burlington", "VT", "05401"};	
//		
//		try { 
//			Assert.assertTrue(testProhibitedState(MN, IssuingState.VT));
//		}catch(AssertionError aE) { 
//			throw aE;
//		}
//	}
//	
//	
//	private boolean testProhibitedState(String[] addressUnderTest, IssuingState enumState) {
//
//		String testNameLocal = new Exception().getStackTrace()[0].getMethodName();
//
//		boolean isPass = false;
//
//		WebElement fieldEmailTemp = driver.findElement(By.id("email-input"));
//		Assert.assertNotNull(fieldEmailTemp);
//		fieldEmailTemp.sendKeys(addressUnderTest[0]);
//		FieldValidationUtilities.sleepSecond(testNameLocal);
//		
//		pageUnderTest.getFieldAreaCode().sendKeys(addressUnderTest[1].substring(0, 3));
//
//		pageUnderTest.getFieldPrefix().sendKeys(addressUnderTest[1].substring(3, 6));
//		
//		Actions actions = new Actions(driver);
//		actions.sendKeys(Keys.PAGE_DOWN).build().perform();
//		FieldValidationUtilities.sleepSecond(testNameLocal);
//
//		pageUnderTest.getFieldLineNumber().sendKeys(addressUnderTest[1].substring(6, 10));
//
//		pageUnderTest.getFieldAddressLine1().sendKeys(addressUnderTest[2]);
//
//		if(!(null==addressUnderTest[3])) { 
//			pageUnderTest.getFieldAddressLine2().sendKeys(addressUnderTest[3]);
//		} else { 
//			System.out.println(testNameLocal + ":\taddress line two skipped intentionally");
//		}
//
//		actions.sendKeys(Keys.PAGE_DOWN).build().perform();
//		FieldValidationUtilities.sleepSecond(testNameLocal);
//		actions = null;
//		
//		pageUnderTest.getFieldCity().sendKeys(addressUnderTest[4]);
//
//		pageUnderTest.getFieldState().sendKeys(addressUnderTest[5]);
//
//		pageUnderTest.getFieldZIP().sendKeys(addressUnderTest[6]);
//
//		// scroll down after ZIP field else false-fail at the checkbox
//		JavascriptExecutor jse = (JavascriptExecutor)driver;
//		jse.executeScript("window.scrollBy(0,250)", "");
//		jse = null;
//
//		pageUnderTest.acceptSpam();
//		
//		String stateNameLong = enumState.properName;
//		String expected = "Unfortunately, we are unable to lease products to customers residing in " + stateNameLong + ".";
//		String captured = driver.findElement(By.cssSelector(".pg-address-error:nth-child(14)")).getText();
//		// //div[2]/span[14]
//
//		if(expected.equals(captured)) { 
//			isPass = true;
//			System.out.println("\n" + testNameLocal + ":\tPASS with " + addressUnderTest[5]);
//		} else { 
//			System.out.println("\n" + testNameLocal + ":\tFAIL with " + addressUnderTest[5]);
//			System.out.println(testNameLocal + ":\texpected \'" + expected + "\'");
//			System.out.println(testNameLocal + ":\tcaptured \'" + captured + "\'");
//		}
//
//		addressUnderTest = null;
//		stateNameLong = null;
//		expected = null;
//		captured = null;
//		testNameLocal = null;
//
//		return isPass;
//	}

	
	/**
	 * If subclass of TestBaseSauce, pass the test result up to the Sauce Labs Dashboard.
	 * <p>
	 * Quit driver.
	 * <p>
	 * Delete the cookie this test just created.
	 */
	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {
		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		if(this.getClass().getSuperclass().getSimpleName().equals("TestBaseSauce")) { 
			System.out.println(methodNameLocal + ":\tSauce driver type detected; Sauce-specific teardown up next...");
			((JavascriptExecutor) driver).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
		} else { 
			System.out.println(methodNameLocal + ":\tLocal driver type detected; Sauce-specific teardown skipped intentionally.\n");
		}
		driver.quit();
		System.out.println(methodNameLocal + ":\tEND tearDown.");
	}

}
