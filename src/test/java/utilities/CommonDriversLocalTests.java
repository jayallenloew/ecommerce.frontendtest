package utilities;

import java.lang.reflect.Method;
import java.util.logging.Logger;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import common.ProgressiveLeasingConstants;
import utilities.DriverTypeLocal;
import waiter.Waiter;

/**
 * Fast, minimalist tests of the common (and local) driver utility.
 * <p>
 * Each driver type in the common drivers class is exercised. When you 
 * add or remove a driver there, update this test class accordingly.
 * <p>
 * Experimental use of TheWaiter classes.
 * <p>
 * Version 1.0 - 2019-01-04
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a> 
 * @version 1.0
 * @see CommonDriversLocal.java
 */
public class CommonDriversLocalTests {
	
	private static final Logger LOGGER = Logger.getLogger(CommonDriversLocalTests.class.getName());

	@Test
	public void testChrome(Method method) {
		Assert.assertTrue(testEngine(method.getName(),DriverTypeLocal.CHROME));
	}

	@Test
	public void testChromeHeadless(Method method) {
		Assert.assertTrue(testEngine(method.getName(),DriverTypeLocal.CHROME_HEADLESS));
	}

	@Test
	public void testFirefox(Method method) {
		Assert.assertTrue(testEngine(method.getName(),DriverTypeLocal.FIREFOX));
	}

	@Test
	public void testFirefoxHeadless(Method method) {
		Assert.assertTrue(testEngine(method.getName(),DriverTypeLocal.FIREFOX_HEADLESS));
	}

	private boolean testEngine(String testNameIn, DriverTypeLocal driverTypeIn) { 
		WebDriver driverLocal = new CommonDriversLocal(driverTypeIn).getPreparedDriver();
		Waiter waiter = new Waiter();
		waiter.get(ProgressiveLeasingConstants.URL_LANDING_PRODUCTION, driverLocal, 5);
		try { 
			Assert.assertTrue(driverLocal.getTitle().contains("Home | Progressive Leasing"));
			LOGGER.info(testNameIn + ":\tPASS");
			return true;
		}catch(AssertionError aE) { 
			LOGGER.warning(testNameIn + ":\tFAIL");
			return false;
		}finally { 
			driverLocal.quit();
			waiter = null;
		}
	}
}
