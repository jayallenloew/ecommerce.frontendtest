package utilities.saucelabs;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import common.FieldValidationUtilities;
import common.ProgressiveLeasingConstants;

/**
 * This test class receives instances from a test factory.
 * <p>
 * Version 1.0 2019-05-10
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.0
 * @see TestBaseSauceMobileFactory.java
 * @see TestBaseSauceMobile.xml
 */
public class TestBaseSauceMobile extends TestBaseSauce { 
	
	
	@SuppressWarnings("unused")
	private DriverTypeSauce driverType;
	private CommonDriverSauceMobile cdsm;
	private WebDriver driverUnderTest;
	private String testNameIn;
	
	
	public TestBaseSauceMobile(DriverTypeSauce driverType) { 
		this.driverType = driverType;
		cdsm = new CommonDriverSauceMobile(driverType, DataCenter.USA);	
		testNameIn = driverType.name();
	}
	

	@Test
	public void testSauceDriver() {
		cdsm.setNameOfCallingTest(testNameIn);
		driverUnderTest = cdsm.getPreparedDriver();
		driverUnderTest.navigate().to(ProgressiveLeasingConstants.URL_BANK_INFO_13);
		try { 
			new WebDriverWait(driverUnderTest, 15).until(ExpectedConditions.presenceOfElementLocated(By.id("input-account-number")));
		}catch(TimeoutException tE) { 
			driverUnderTest.quit();
			throw new AssertionError(testNameIn,tE);
		}
		
		WebElement fieldUnderTest = driverUnderTest.findElement(By.id("input-account-number"));
		
		try { // type
			fieldUnderTest.sendKeys("123456"); // phoney account number
			FieldValidationUtilities.sleepSecond(testNameIn);
		}catch(Exception anyExceptionFailsHere) { 
			driverUnderTest.quit();
			throw new AssertionError(testNameIn,anyExceptionFailsHere);
		}

		try { // clear -- and we're good, last one
			fieldUnderTest.clear(); 
			FieldValidationUtilities.sleepSecond(testNameIn);
			System.out.println("\n" + testNameIn + ":\tPASS\n");
		}catch(Exception anyExceptionFailsHere) { 
			System.out.println("\n" + testNameIn + ":\tFAIL catching " + anyExceptionFailsHere.getClass().getSimpleName() + "\n");
			throw new AssertionError(testNameIn,anyExceptionFailsHere);
		}finally { 
			fieldUnderTest = null;
		}
	}
	
	/**
	 * Pass the result up to the Sauce Labs Dashboard.
	 * <p>
	 * Quit the driver.
	 * <p>
	 * There is no local option; these are all mobile tests. All of our 
	 * mobile tests currently run in the Sauce Labs cloud.
	 */
	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {
		((JavascriptExecutor) driverUnderTest).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
		driverUnderTest.quit();
	}

	
	
}