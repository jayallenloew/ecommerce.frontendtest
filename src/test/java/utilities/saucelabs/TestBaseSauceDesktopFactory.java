package utilities.saucelabs;

import org.testng.annotations.Factory;

/**
 * This factory instantiates a test with each currently supported driver type.
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.0
 * @see TestBaseSauce.java
 * @see TesetBaseSauceTests.java
 */
public class TestBaseSauceDesktopFactory {
	
	@Factory
	public Object[] testSauceDrivers() { 
		return new Object[] { 
			new TestBaseSauceDesktop(DriverTypeSauce.MAC_CHROME),
			new TestBaseSauceDesktop(DriverTypeSauce.MAC_FIREFOX),
			new TestBaseSauceDesktop(DriverTypeSauce.MAC_SAFARI),
			new TestBaseSauceDesktop(DriverTypeSauce.WINDOWS_CHROME),
			new TestBaseSauceDesktop(DriverTypeSauce.WINDOWS_FIREFOX),
			new TestBaseSauceDesktop(DriverTypeSauce.WINDOWS_IE)
		};
	}
}
