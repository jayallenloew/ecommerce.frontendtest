package utilities.saucelabs;

import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import common.ProgressiveLeasingConstants;

/**
 * Health check runs one instance of one OS-browser combination.
 * <p>
 * A quick health check for basic Sauce driver and tunnel readiness, and 
 * of course for project fitness. Fails at command line if compiler errors.
 * <p>
 * Version 1.0 2019-04-10
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.0
 */
public class HealthCheckSauce extends TestBaseSauce {
	
	private WebDriver driverToTest;
	
	@Test
	public void healthCheckSauce(Method method) {
		setDriverTypeForTestClass(DriverTypeSauce.WINDOWS_CHROME);
		driverToTest = getPreparedDriver(method.getName());
		driverToTest.navigate().to(ProgressiveLeasingConstants.URL_CONTACT_INFO_13);
		try { 
			new WebDriverWait(driverToTest, 30).until(ExpectedConditions.presenceOfElementLocated(By.id("email-input")));
		}catch(TimeoutException tE) { 
			throw new AssertionError(method.getName(), tE);
		}
	}

    /**
     * Pass the result up to the Sauce Labs Dashboard.
     * <p>
     * Quit the driver.
     */
    @AfterMethod
    public void tearDown(ITestResult result) throws Exception {
        ((JavascriptExecutor) driverToTest).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
        driverToTest.quit();
    }
	
 
}
