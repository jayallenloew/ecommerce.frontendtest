package utilities.saucelabs;

import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import common.FieldValidationUtilities;
import common.ProgressiveLeasingConstants;
import utilities.TestBaseLocal;

/**
 * Minimalist unit test for new base class for Sauce Labs drivers.
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.0
 * @see TestBaseLocal.java
 * @see TestBaseSauceDesktopFactory.java
 */
public class TestBaseSauceDesktop extends TestBaseSauce { 
	
	@SuppressWarnings("unused")
	private DriverTypeSauce driverType;
	private CommonDriverSauceDesktop cds;
	private WebDriver driverUnderTest;
	private String testNameIn;
	
	public TestBaseSauceDesktop(DriverTypeSauce driverType) { 
		this.driverType = driverType;
		cds = new CommonDriverSauceDesktop(driverType, DataCenter.USA);	
		testNameIn = driverType.name();
	}
	
	

	@Test
	public void testSauceDriver(Method method) {
		cds.setNameOfCallingTest(testNameIn);
		driverUnderTest = cds.getPreparedDriver();
		driverUnderTest.navigate().to(ProgressiveLeasingConstants.URL_BANK_INFO_13);
		try { 
			new WebDriverWait(driverUnderTest, 15).until(ExpectedConditions.presenceOfElementLocated(By.id("input-account-number")));
		}catch(TimeoutException tE) { 
			driverUnderTest.quit();
			throw new AssertionError(method.getName(),tE);
		}
		
		WebElement fieldUnderTest = driverUnderTest.findElement(By.id("input-account-number"));
		
		try { // type
			fieldUnderTest.sendKeys("123456"); // phoney account number
			FieldValidationUtilities.sleepSecond(method.getName());
		}catch(Exception anyExceptionFailsHere) { 
			driverUnderTest.quit();
			throw new AssertionError(method.getName(),anyExceptionFailsHere);
		}

		try { // clear -- and we're good, last one
			fieldUnderTest.clear(); 
			FieldValidationUtilities.sleepSecond(method.getName());
		}catch(Exception anyExceptionFailsHere) { 
			throw new AssertionError(method.getName(),anyExceptionFailsHere);
		}finally { 
			fieldUnderTest = null;
		}
	}
	
	/**
	 * Pass the result up to the Sauce Labs Dashboard.
	 * <p>
	 * Quit the driver.
	 */
	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {
		((JavascriptExecutor) driverUnderTest).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
		driverUnderTest.quit();
	}

	
	
}