package utilities;

import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import common.FieldValidationUtilities;
import common.ProgressiveLeasingConstants;
import pages.MarketingPage;
import utilities.saucelabs.TestBaseSauceDesktopFactory;

/**
 * Minimalist unit test for new base class for local drivers.
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.0
 * @see TestBaseLocal.java
 * @see TestBaseSauceDesktopFactory.java
 */
public class TestBaseLocalTests extends TestBaseLocal { 
	
	public TestBaseLocalTests(DriverTypeLocal driverType) { 
		setDriverTypeForTestClass(driverType);
	}

	@Test
	public void testLocalDriver(Method method) {
		WebDriver driverUnderTest = getPreparedDriver();
		System.out.println(method.getName() + ":\tbegin with " + getDriverTypeForTestClass());
		driverUnderTest.navigate().to(ProgressiveLeasingConstants.URL_WELCOME_12);
		try { 
			new WebDriverWait(driverUnderTest, 15).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@type='cta']")));
		}catch(TimeoutException tE) { 
			driverUnderTest.quit();
			throw new AssertionError(method.getName(),tE);
		}
		
		MarketingPage pageTemp = new MarketingPage(driverUnderTest);
				
		try { // type
			pageTemp.getButtonGet_started().click();
			FieldValidationUtilities.sleepSecond(method.getName());
		}catch(Exception anyExceptionFailsHere) { 
			driverUnderTest.quit();
			throw new AssertionError(method.getName(),anyExceptionFailsHere);
		}finally { 
			pageTemp = null;
			driverUnderTest.quit();
		}
	}
	

	
	
}