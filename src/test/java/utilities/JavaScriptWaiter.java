package utilities;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * This won't work on the Magento pages. We'll need a proper test subject.
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @deprecated
 */
public class JavaScriptWaiter {

	private WebDriver jsWaitDriver;
	private static WebDriverWait jsWait;
	private static JavascriptExecutor jsExecutor;

	public void setDriver(WebDriver driver) { 
		jsWaitDriver = driver;
		jsWait = new WebDriverWait(driver, 60);
		jsExecutor = (JavascriptExecutor) jsWaitDriver; 
	}

	public void waitUntilJavaScriptReady() { 
		try {
			ExpectedCondition<Boolean> jsLoad = driver -> ((JavascriptExecutor) this.jsWaitDriver)
					.executeScript("return document.readyState").toString().equals("complete");

			boolean jsReady = jsExecutor.executeScript("return document.readyState").toString().equals("complete");

			if (!jsReady) {
				jsWait.until(jsLoad);
			}
		} catch (WebDriverException ignored) {
			// deliberate swallow
		}
	}
	
	   public void ajaxComplete() {
		   jsExecutor.executeScript("var callback = arguments[arguments.length - 1];"
	            + "var xhr = new XMLHttpRequest();" + "xhr.open('GET', '/Ajax_call', true);"
	            + "xhr.onreadystatechange = function() {" + "  if (xhr.readyState == 4) {"
	            + "    callback(xhr.responseText);" + "  }" + "};" + "xhr.send();");
	    }	

}
