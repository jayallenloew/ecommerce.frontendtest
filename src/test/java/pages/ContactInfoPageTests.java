/*
 * Copyright Progressive Leasing LLC.
 */
package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchSessionException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import common.FieldValidationUtilities;
import common.ProgressiveLeasingConstants;
import utilities.DeleteSessionCookie;
import utilities.ResetThisUser;
import utilities.saucelabs.TestBaseSauce;

/**
 * Test class for page object for the "contact info" page ("How can we contact you?")
 * <p>
 * Version 1.0 2019-01-22
 * <p>
 * Version 1.1 2019-02-21 add test for new happy path populate method
 * <p>
 * Version 1.2 2019-03-26 hardening and automatic cleanup
 * <p> 
 * Version 2.0 2019-04-16 Sauce default plus performance enhancements
 * <p>
 * Version 2.1 2019-05-03 accept spam
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 2.1
 * @see ContactInfoPage.java
 */
public class ContactInfoPageTests extends TestBaseSauce {

	private WebDriver driver;

	private ContactInfoPage pageUnderTest;

	private static boolean isPrimaryTestPass = false;

	/**
	 * This value will be set during test execution. Then passed to 
	 * a cleanup utility during teardown.
	 */
	private String cookieToDelete;

	/**
	 * In a couple places, this test will require a test email address.
	 * <p>
	 * Current possible: 000@test.com through 010@test.com
	 */
	public static final String EMAIL_FOR_THIS_TEST = "001@test.com";


	private void setUpNotStatic(String testNameIn) {

		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();

		System.out.println(methodNameLocal + ":\tbegin setup for " + testNameIn);

		@SuppressWarnings("unused")
		ResetThisUser resetTemp = new ResetThisUser(EMAIL_FOR_THIS_TEST);
		FieldValidationUtilities.sleepSecond(methodNameLocal);
		resetTemp = null;

		driver = getPreparedDriver(methodNameLocal);

		// go to demo page
		driver.navigate().to(ProgressiveLeasingConstants.URL_STORE_DEMO);

		// wait for Find out more button 
		try { 
			new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//img[@alt='Learn more about Progressive Leasing']")));
			System.out.println(methodNameLocal + ":\tstore demo landing page... (OK)");
		}catch(TimeoutException tE) { 
			throw new AssertionError((methodNameLocal + ":\tfast-fail in setup; check store demo page."), tE);
		}

		StoreDemoLandingPage demoPageTemp = new StoreDemoLandingPage(driver);

		// click through to marketing page
		demoPageTemp.getButtonFindOutMore().click(); 
		demoPageTemp = null;

		// starting from storedemo (which is required in this test) you'll always land on 12
		// force redirect from 12 to 13
		StringBuilder sBuild = new StringBuilder(driver.getCurrentUrl());
		int indexBegin = sBuild.toString().indexOf("qaswebapp");
		sBuild.replace(indexBegin, (indexBegin+11), "qaswebapp13");
		driver.navigate().to(sBuild.toString());

		// wait for Get Started button 
		try { 
			new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@type='cta']")));
			System.out.println(methodNameLocal + ":\tmarketing page... (OK)");
		}catch(TimeoutException tE) { 
			throw new AssertionError((methodNameLocal + ":\tfast-fail in setup; check marketing page."), tE);
		}

		MarketingPage marketingPage = new MarketingPage(driver);

		// click through to get started request code
		marketingPage.getButtonGet_started().click(); 
		FieldValidationUtilities.sleepSecond(methodNameLocal);
		FieldValidationUtilities.sleepSecond(methodNameLocal);
		marketingPage = null;

		// wait for Mobile phone or email field 
		try { 
			//			new WebDriverWait(driver, 20).until(ExpectedConditions.presenceOfElementLocated(By.id("otp-contact-input")));
			new WebDriverWait(driver, 20).until(ExpectedConditions.presenceOfElementLocated(By.id("pg-field-contact")));
			System.out.println(methodNameLocal + ":\tget started request code page... (OK)");
		}catch(TimeoutException tE) { 
			throw new AssertionError((methodNameLocal + ":\tfast-fail in setup; check get started request code page."), tE);
		}

		GetStartedRequestCodePage requestCodeTemp = new GetStartedRequestCodePage(driver);


		// enter a test email address or phone
		driver.findElement(By.id("pg-field-contact")).sendKeys(EMAIL_FOR_THIS_TEST);
		FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);

		// accept the terms in order to proceed to enter the code
		requestCodeTemp.getCheckboxTerms().click(); // accept terms
		FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);

		// after the checkbox, 5 tabs and a space to advance to the next page
		Actions builder = new Actions(driver);
		builder.sendKeys(Keys.TAB).build().perform();
		FieldValidationUtilities.sleepSecond(methodNameLocal);
		builder.sendKeys(Keys.TAB).build().perform();
		FieldValidationUtilities.sleepSecond(methodNameLocal);
		builder.sendKeys(Keys.TAB).build().perform();
		FieldValidationUtilities.sleepSecond(methodNameLocal);
		builder.sendKeys(Keys.TAB).build().perform();
		FieldValidationUtilities.sleepSecond(methodNameLocal);
		builder.sendKeys(Keys.TAB).build().perform();
		FieldValidationUtilities.sleepSecond(methodNameLocal);
		builder.sendKeys(Keys.SPACE).build().perform();
		FieldValidationUtilities.sleepSecond(methodNameLocal);

		FieldValidationUtilities.sleepSecond(methodNameLocal);

		requestCodeTemp = null;
		//		builder = null;

		// wait for enter the code field 
		try { 
			new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.id("pg-field-code")));
			System.out.println(methodNameLocal + ":\tenter the code page... (OK)");
		}catch(TimeoutException tE) { 
			throw new AssertionError((methodNameLocal + ":\tfast-fail in setup; check enter the code page."), tE);
		}

		// enter the (test) code and take a nap, unfortunately
		driver.findElement(By.id("pg-field-code")).sendKeys("000000");
		// yeah the sleeps suck, thank Magento, move on....
		try { 
			Thread.sleep(10000); // Don't remove or reduce. It's not our issue.
		}catch(InterruptedException iE) { 
			throw new AssertionError((methodNameLocal + ":\tfast-fail in setup"), iE);
		}
		System.out.println("\n====cookie to console====");
		java.util.Set<Cookie> cookies = driver.manage().getCookies();
		for(Cookie C : cookies) { 
			if(!(C.getName().equals("sessionId"))) { 
				continue;
			}
			cookieToDelete = C.getValue();
			System.out.print("To delete:\t" + cookieToDelete);
		}
		System.out.println("\n====cookie to console====\n");

		// wait for basic info page
		try { 
			new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.id("first-name-input")));
			System.out.println(methodNameLocal + ":\tbasic info page... (OK)");
		}catch(TimeoutException tE) { 
			throw new AssertionError((methodNameLocal + ":\tfast-fail in setup; check basic info page."), tE);
		}

		BasicInfoPage basicInfo = new BasicInfoPage(driver);

		basicInfo.enterName(ProgressiveLeasingConstants.BASIC_INFO_555121212[0], ProgressiveLeasingConstants.BASIC_INFO_555121212[1]);
		basicInfo.enterDate(ProgressiveLeasingConstants.BASIC_INFO_555121212[2], ProgressiveLeasingConstants.BASIC_INFO_555121212[3], ProgressiveLeasingConstants.BASIC_INFO_555121212[4]);
		basicInfo.enterSSN(ProgressiveLeasingConstants.BASIC_INFO_555121212[5], ProgressiveLeasingConstants.BASIC_INFO_555121212[6], ProgressiveLeasingConstants.BASIC_INFO_555121212[7]);
		basicInfo.enterDriverLicense(ProgressiveLeasingConstants.BASIC_INFO_555121212[8]);
		basicInfo.selectTwoLetterState(ProgressiveLeasingConstants.BASIC_INFO_555121212[9]);

		//		basicInfo.enterName("Tester", "Lester");
		//		basicInfo.enterDate("09", "09", "1994");
		//		basicInfo.enterSSN("553", "12", "1212");
		//		basicInfo.enterDriverLicense("123456");
		//		basicInfo.selectTwoLetterState("UT");

		basicInfo.getButtonContinue().click();
		FieldValidationUtilities.sleepSecond(methodNameLocal);

		// wait for contact info page (the page under test)
		try { 
			new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.id("mobile-phone-area-code-input")));
			System.out.println(methodNameLocal + ":\tcontact info page... (OK)");
		}catch(TimeoutException tE) { 
			throw new AssertionError((methodNameLocal + ":\tfast-fail in setup; check contact info page."), tE);
		}

		pageUnderTest = new ContactInfoPage(driver);

		System.out.println("\n" + methodNameLocal + " end setup for " + testNameIn);
	}


	/**
	 * This happy path test is going to populate the form with a valid Utah address.
	 * <p>
	 * If all goes well, a boolean is set to true, and downstream tests are skipped.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true,priority=1)
	public void testHappyPathPopulate() { 

		String testNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();

		setUpNotStatic(testNameLocal);

		// 		For testing purposes I'm leaving this Wisconsin address in here too.
		// 		Note that a Wisconsin address should cause an error message.
		//		String[] validWI = {"000@test.com", "6082621408", "1220 Linden Dr", null, "Madison", "WI", "53706"};

		String[] validUT = {EMAIL_FOR_THIS_TEST, "8014464357", "1600 Towne Center Dr", null, "South Jordan", "UT", "84095"};

		driver.manage().window().maximize();

		try { 
			pageUnderTest.happyPathPopulate(validUT);
			System.out.println("\n" + testNameLocal + " has passed; some downstream tests will be skipped....\n");
			isPrimaryTestPass = true;			
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError(testNameLocal, anyExceptionWhatsoever);
		}

	}	


	@Test(enabled=true,priority=2)
	public void testFormFull() { 

		String testNameLocal = new Exception().getStackTrace()[0].getMethodName();

		if(isPrimaryTestPass) { 
			throw new SkipException("\n" + testNameLocal + " skipped intentionally (OK)");
		} 

		setUpNotStatic(testNameLocal);

		pageUnderTest.getFieldEmail_address().sendKeys("Foo@Boo.com");
		pageUnderTest.getFieldAreaCode().sendKeys("719");
		pageUnderTest.getFieldPrefix().sendKeys("481");
		pageUnderTest.getFieldLineNumber().sendKeys("9992");
		pageUnderTest.getFieldAddressLine1().sendKeys("5055 E. Broadway");
		pageUnderTest.getFieldAddressLine2().sendKeys("Ste F");
		pageUnderTest.getFieldCity().sendKeys("Lehi");
		pageUnderTest.getFieldState().sendKeys("UT");
		pageUnderTest.getFieldZIP().sendKeys("84121");
		// scroll down after ZIP field else false-fail at the checkbox
		((JavascriptExecutor) driver).executeScript("window.scrollBy(0,250);");
		
		pageUnderTest.acceptSpam();

		new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(pageUnderTest.getButtonContinue_to_bank_info()));
		try { 
			pageUnderTest.getButtonContinue_to_bank_info().click();
			FieldValidationUtilities.sleepSecond(testNameLocal);	
			isPrimaryTestPass = true;
			System.out.println("\n" + testNameLocal + " has passed; some downstream tests will be skipped....\n");
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError(testNameLocal,anyExceptionWhatsoever);
		}
	}	


	@Test(enabled=true,priority=3)
	public void testEmail() { 

		String testNameLocal = new Exception().getStackTrace()[0].getMethodName();

		if(isPrimaryTestPass) { 
			throw new SkipException("\n" + testNameLocal + " skipped intentionally (OK)");
		} 

		setUpNotStatic(testNameLocal);

		WebElement fieldTemp = pageUnderTest.getFieldEmail_address();

		fieldTemp.clear();

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		try { 
			fieldTemp.sendKeys("Foo@Boo.com"); // any valid name
			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
			fieldTemp.clear();
			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
			System.out.println("\n" + testNameLocal + ":\tPASS");
		}catch(Exception anyExceptionWhatsoever) { 
			System.out.println("\n" + testNameLocal + ":\tPASS");
			throw new AssertionError(testNameLocal,anyExceptionWhatsoever);
		}finally { 
			fieldTemp = null;
		}
	}



	@Test(enabled=true,priority=3)
	public void testFullPhoneNumber() { 

		String testNameLocal = new Exception().getStackTrace()[0].getMethodName();

		if(isPrimaryTestPass) { 
			throw new SkipException("\n" + testNameLocal + " skipped intentionally (OK)");
		} 

		setUpNotStatic(testNameLocal);

		WebElement fieldTemp = pageUnderTest.getFieldAreaCode();

		fieldTemp.clear();

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		try { 
			fieldTemp.sendKeys("481"); // any valid area code
			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
			fieldTemp.clear();
			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError(testNameLocal + ":\tFAIL on area code",anyExceptionWhatsoever);
		}

		fieldTemp = null;

		fieldTemp = pageUnderTest.getFieldPrefix();

		fieldTemp.clear();

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		try { 
			fieldTemp.sendKeys("481"); // any valid prefix
			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
			fieldTemp.clear();
			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError(testNameLocal + ":\tFAIL on prefix",anyExceptionWhatsoever);
		}	

		fieldTemp = null;

		fieldTemp = pageUnderTest.getFieldLineNumber();

		fieldTemp.clear();

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		try { 
			fieldTemp.sendKeys("9996"); // any valid number (last 4)
			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
			fieldTemp.clear();
			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
			System.out.println("\n" + testNameLocal + ":\tPASS -- full phone (3 parts)");
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError(testNameLocal + ":\tFAIL on line number",anyExceptionWhatsoever);
		}finally { 
			fieldTemp = null;
		}
	}



	@Test(enabled=true,priority=3)
	public void testFullStreetAddress() { 

		String testNameLocal = new Exception().getStackTrace()[0].getMethodName();

		if(isPrimaryTestPass) { 
			throw new SkipException("\n" + testNameLocal + " skipped intentionally (OK)");
		} 

		setUpNotStatic(testNameLocal);

		WebElement fieldTemp = pageUnderTest.getFieldAddressLine1();

		fieldTemp.clear();

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		try { 
			fieldTemp.sendKeys("5055 E. Broadway"); // ordinary primary street address
			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
			fieldTemp.clear();
			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError(testNameLocal + ":\tFAIL on street 1",anyExceptionWhatsoever);
		}

		fieldTemp = null;

		fieldTemp = pageUnderTest.getFieldAddressLine2();

		fieldTemp.clear();

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		try { 
			fieldTemp.sendKeys("Ste F"); // alpha suite
			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
			fieldTemp.clear();
			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError(testNameLocal + ":\tFAIL on street 2 with alpha suite",anyExceptionWhatsoever);
		}	

		fieldTemp.clear();

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		try { 
			fieldTemp.sendKeys("Ste 701"); // numeric suite
			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
			fieldTemp.clear();
			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError(testNameLocal + ":\tFAIL on street 2 with numeric suite",anyExceptionWhatsoever);
		}	

		fieldTemp.clear();

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		try { 
			fieldTemp.sendKeys("Apt 1308"); // apartment
			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
			fieldTemp.clear();
			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
			System.out.println("\n" + testNameLocal + ":\tPASS -- full street address (2 parts)");
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError(testNameLocal + ":\tFAIL on street 2 with apartment",anyExceptionWhatsoever);
		}finally { 
			fieldTemp = null;
		}	
	}


	@Test(enabled=true,priority=3)
	public void testCity() { 

		String testNameLocal = new Exception().getStackTrace()[0].getMethodName();

		if(isPrimaryTestPass) { 
			throw new SkipException("\n" + testNameLocal + " skipped intentionally (OK)");
		} 

		setUpNotStatic(testNameLocal);

		WebElement fieldTemp = pageUnderTest.getFieldCity();

		fieldTemp.clear();

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		try { 
			fieldTemp.sendKeys("Lehi"); 
			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
			fieldTemp.clear();
			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
			System.out.println("\n" + testNameLocal + ":\tPASS");
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError(testNameLocal + ":\tFAIL",anyExceptionWhatsoever);
		}finally { 
			fieldTemp = null;
		}
	}	


	@Test(enabled=true,priority=3)
	public void testState() { 

		String testNameLocal = new Exception().getStackTrace()[0].getMethodName();

		if(isPrimaryTestPass) { 
			throw new SkipException("\n" + testNameLocal + " skipped intentionally (OK)");
		} 

		setUpNotStatic(testNameLocal);

		WebElement fieldTemp = pageUnderTest.getFieldState();

		fieldTemp.clear();

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		try { 
			fieldTemp.sendKeys("UT"); 
			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
			fieldTemp.clear();
			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
			System.out.println("\n" + testNameLocal + ":\tPASS");
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError(testNameLocal + ":\tFAIL",anyExceptionWhatsoever);
		}finally { 
			fieldTemp = null;
		}
	}		


	@Test(enabled=true,priority=3)
	public void testZIP() { 

		String testNameLocal = new Exception().getStackTrace()[0].getMethodName();

		if(isPrimaryTestPass) { 
			throw new SkipException("\n" + testNameLocal + " skipped intentionally (OK)");
		} 

		setUpNotStatic(testNameLocal);

		WebElement fieldTemp = pageUnderTest.getFieldZIP();

		fieldTemp.clear();

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		try { 
			fieldTemp.sendKeys("84121"); 
			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
			fieldTemp.clear();
			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
			System.out.println("\n" + testNameLocal + ":\tPASS");
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError(testNameLocal + ":\tFAIL",anyExceptionWhatsoever);
		}finally { 
			fieldTemp = null;
		}
	}		



	@Test(enabled=true,priority=3)
	public void testAcceptSpam() { 

		String testNameLocal = new Exception().getStackTrace()[0].getMethodName();

		if(isPrimaryTestPass) { 
			throw new SkipException("\n" + testNameLocal + " skipped intentionally (OK)");
		} 

		setUpNotStatic(testNameLocal);
		
		pageUnderTest.getFieldZIP().sendKeys(Keys.ESCAPE);
		((JavascriptExecutor) driver).executeScript("window.scrollBy(0,250);");

		try { 
			pageUnderTest.acceptSpam();
			System.out.println("\n" + testNameLocal + ":\tPASS");
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError(testNameLocal + ":\tFAIL on 1st click to select",anyExceptionWhatsoever);
		}
	}



	@Test(enabled=true,priority=3)
	public void testGetButtonPrimary() { 

		String testNameLocal = new Exception().getStackTrace()[0].getMethodName();

		if(isPrimaryTestPass) { 
			throw new SkipException("\n" + testNameLocal + " skipped intentionally (OK)");
		} 

		setUpNotStatic(testNameLocal);

		WebElement buttonTemp = pageUnderTest.getButtonContinue_to_bank_info();

		try { 
			Assert.assertNotNull(buttonTemp);
			System.out.println("\n" + testNameLocal + ":\tPASS");
		}catch(AssertionError aE) {
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			throw aE;
		}

		buttonTemp = null;
	}



	/**
	 * If subclass of TestBaseSauce, pass the test result up to the Sauce Labs Dashboard.
	 * <p>
	 * Quit driver.
	 * <p>
	 * Delete the cookie this test just created.
	 */
	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {
		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		if(this.getClass().getSuperclass().getSimpleName().equals("TestBaseSauce")) { 
			System.out.println(methodNameLocal + ":\tSauce driver type detected; Sauce-specific teardown up next...");
			try { 
				((JavascriptExecutor) driver).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
			}catch(NoSuchSessionException nSSE) { 
				// deliberate swallow
			}
		} else { 
			System.out.println(methodNameLocal + ":\tLocal driver type detected; Sauce-specific teardown skipped intentionally.\n");
		}
		driver.quit();
		pageUnderTest = null;
		@SuppressWarnings("unused")
		DeleteSessionCookie deleteCookie = new DeleteSessionCookie(cookieToDelete,EMAIL_FOR_THIS_TEST);
		System.out.println("\n" + methodNameLocal + ":\tEND tearDown.");
	}


}
