package pages;

import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchSessionException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import common.FieldValidationUtilities;
import common.ProgressiveLeasingConstants;
import utilities.ResetThisUser;
import utilities.saucelabs.DriverTypeSauce;
import utilities.saucelabs.TestBaseSauce;
import waiter.Waiter;

/**
 * Test class for the request code page object.
 * <p>
 * Version 1.1 2019-01-14 add assertion for happy=path click-through.
 * <p>
 * Version 1.2 2019-03-26 hardening and automatic cleanup
 * <p>
 * Version 2.0 2019-04-16 Sauce default plus performance enhancements
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 2.0
 * @see GetStartedRequestCodePage
 */
public class GetStartedRequestCodePageTests extends TestBaseSauce {

	private WebDriver driver;
	
	private GetStartedRequestCodePage requestCodePage;

	private Waiter waiter;
	
	private static boolean isPrimaryTestPass = false;

	/**
	 * In a couple places, this test will require a test email address.
	 * <p>
	 * Current possible: 000@test.com through 010@test.com
	 */
	public static final String EMAIL_FOR_THIS_TEST = "006@test.com";


	private void setUp(String testNameIn) {
		
		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();

		if(isPrimaryTestPass) { 
			throw new SkipException(methodNameLocal + ":\tskip intentionally for " + testNameIn);
		}
		
		@SuppressWarnings("unused")
		ResetThisUser reset = new ResetThisUser(EMAIL_FOR_THIS_TEST);
		reset = null;
		
		setDriverTypeForTestClass(DriverTypeSauce.WINDOWS_CHROME);
		driver = getPreparedDriver(testNameIn);
		
		// go to demo page
		driver.navigate().to(ProgressiveLeasingConstants.URL_STORE_DEMO);

		// wait for Find out more button 
		try { 
			new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//img[@alt='Learn more about Progressive Leasing']")));
			System.out.println(methodNameLocal + ":\tstore demo landing page... (OK)");
		}catch(TimeoutException tE) { 
			throw new AssertionError((methodNameLocal + ":\tfast-fail in setup; check store demo page."), tE);
		}

		StoreDemoLandingPage demoPageTemp = new StoreDemoLandingPage(driver);

		// click through to marketing page
		demoPageTemp.getButtonFindOutMore().click(); 
		demoPageTemp = null;

		// starting from storedemo (which is required in this test) you'll always land on 12
		// force redirect from 12 to 13
		StringBuilder sBuild = new StringBuilder(driver.getCurrentUrl());
		int indexBegin = sBuild.toString().indexOf("qaswebapp");
		sBuild.replace(indexBegin, (indexBegin+11), "qaswebapp13");
		driver.navigate().to(sBuild.toString());
		sBuild = null;

		// wait for Get Started button on marketing page
		try { 
			new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@type='cta']")));
			System.out.println(methodNameLocal + ":\tmarketing page... (OK)");
		}catch(TimeoutException tE) { 
			throw new AssertionError((methodNameLocal + ":\tfast-fail in setup; check marketing page."), tE);
		}

		MarketingPage marketingPage = new MarketingPage(driver);

		// click through to get started request code - the page under test
		marketingPage.getButtonGet_started().click(); 
		FieldValidationUtilities.sleepSecond(methodNameLocal);
		FieldValidationUtilities.sleepSecond(methodNameLocal);
		marketingPage = null;

		// wait for Mobile phone or email field 
		try { 
			//			new WebDriverWait(driver, 20).until(ExpectedConditions.presenceOfElementLocated(By.id("otp-contact-input")));
			new WebDriverWait(driver, 60).until(ExpectedConditions.presenceOfElementLocated(By.id("pg-field-contact")));
			System.out.println(methodNameLocal + ":\tget started request code page... (OK)");
		}catch(TimeoutException tE) { 
			throw new AssertionError((methodNameLocal + ":\tfast-fail in setup; check get started request code page."), tE);
		}
		
		System.out.println(methodNameLocal + " setup complete");
	}

	
	@Test(enabled=true,priority=1)
	public void testHappyPathRequestCode(Method method) {
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		setUp(testNameLocal);
		String temp = driver.getCurrentUrl();
		driver.navigate().to(temp);
		FieldValidationUtilities.sleepSecond(testNameLocal);
		temp = null;
		requestCodePage = new GetStartedRequestCodePage(driver);
		requestCodePage.happyPathRequestCode(EMAIL_FOR_THIS_TEST);
		EnterTheCodePage dependentPageTemp = new EnterTheCodePage(driver);
		FieldValidationUtilities.sleepSecond(testNameLocal); // do not reduce or remove
		try { 
			dependentPageTemp.getFieldEnterTheCode().sendKeys("Foo");
			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
			dependentPageTemp.getFieldEnterTheCode().clear();
			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
			GetStartedRequestCodePageTests.isPrimaryTestPass = true;
			System.out.println("\n" + testNameLocal + ":\tPASS");
		}catch(Exception anyExceptionWhatsoever) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			// any exception caught here should and will fail the test
			throw new AssertionError(testNameLocal,anyExceptionWhatsoever);
		}finally { 
			dependentPageTemp = null;
			testNameLocal = null;
			requestCodePage = null;
		}
	}

	
	@Test(enabled=true,priority=2)
	public void testGetFieldMobileOrEmail(Method method) { 
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		setUp(testNameLocal);
		try { 
			requestCodePage.getFieldMobileOrEmail().clear();
			requestCodePage.getFieldMobileOrEmail().sendKeys(EMAIL_FOR_THIS_TEST);
			requestCodePage.getFieldMobileOrEmail().clear();
			System.out.println("\n" + testNameLocal + ":\tPASS");
		}catch(Exception anyExceptionWhatsoever) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			throw new AssertionError(testNameLocal, anyExceptionWhatsoever);
		}finally { 
			testNameLocal = null;
		}
	}

	
	@Test(enabled=true,priority=2)
	public void testGetCheckbox(Method method) throws InterruptedException { 
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		setUp(testNameLocal);
		try { 
			requestCodePage.getCheckboxTerms().click();
			FieldValidationUtilities.sleepQuarterSecond(method.getName());
			requestCodePage.getCheckboxTerms().click();
			System.out.println("\n" + testNameLocal + ":\tPASS");
		}catch(Exception anyExceptionWhatsoever) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			throw new AssertionError(testNameLocal, anyExceptionWhatsoever);
		}finally { 
			testNameLocal = null;
		}
	}

	
	@Test(enabled=true,priority=2)
	public void testGetButtonSendCodeGetStarted(Method method) { 
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		setUp(testNameLocal);
		requestCodePage.getFieldMobileOrEmail().clear();
		FieldValidationUtilities.sleepSecond(testNameLocal);
		requestCodePage.getFieldMobileOrEmail().sendKeys(EMAIL_FOR_THIS_TEST);
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		requestCodePage.getCheckboxTerms().click();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		try { 
			Assert.assertTrue(requestCodePage.getButtonSendCode_Get_started().isEnabled());
			System.out.println("\n" + testNameLocal + ":\tPASS; optional Exit next...");
			try { 
				waiter.click(requestCodePage.getButtonExit(), driver, 10);
			}catch(Exception anyException) { 
				// deliberate swallow
			}
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			throw aE;
		}finally { 
			testNameLocal = null;
		}
	}
	
	
	/**
	 * If subclass of TestBaseSauce, pass the test result up to the Sauce Labs Dashboard.
	 * <p>
	 * Quit driver.
	 * <p>
	 * Delete the cookie this test just created.
	 */
	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {

		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		if(this.getClass().getSuperclass().getSimpleName().equals("TestBaseSauce")) { 
			System.out.println(methodNameLocal + ":\tSauce driver type detected; Sauce-specific teardown up next...");
			try { 
				((JavascriptExecutor) driver).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
			}catch(NoSuchSessionException nSSE) { 
				 // deliberate swallow
			}
		} else { 
			System.out.println(methodNameLocal + ":\tLocal driver type detected; Sauce-specific teardown skipped intentionally.\n");
		}
		driver.quit();
		waiter = null;
		System.out.println("\n" + methodNameLocal + ":\tEND tearDown.");
	}
}
