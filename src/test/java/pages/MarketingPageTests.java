/*
 * Copyright Progressive Leasing LLC.
 */
package pages;

import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchSessionException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import common.FieldValidationUtilities;
import common.ProgressiveLeasingConstants;
import utilities.saucelabs.DriverTypeSauce;
import utilities.saucelabs.TestBaseSauce;

/**
 * Test class for page object for the "marketing page" page. 
 * <p>
 * Path: Store demo landing page | LEARN MORE
 * <p>
 * There are just a few things we're responsible for on this page, so 
 * this page object is small.
 * <p>
 * Version 1.0 2019-01-07
 * <p>
 * Version 1.1 2019-01-08 pass driver type as parameter from xml suite file
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @author <a href="mailto:christopher.rains@progleasing.com">CJ Rains</a>
 * @version 1.1 2019-01-08
 * @see MarketingPageTests.java
 */
public class MarketingPageTests extends TestBaseSauce {

	private WebDriver driver;

	private MarketingPage pageUnderTest;

	public void setUp(String testNameIn) {

		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();

		setDriverTypeForTestClass(DriverTypeSauce.WINDOWS_CHROME);
		
		driver = getPreparedDriver(testNameIn);

		// go to demo page
		driver.navigate().to(ProgressiveLeasingConstants.URL_STORE_DEMO);

		// wait for Find out more button 
		try { 
			new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//img[@alt='Learn more about Progressive Leasing']")));
			System.out.println(methodNameLocal + ":\tstore demo landing page... (OK)");
		}catch(TimeoutException tE) { 
			throw new AssertionError((methodNameLocal + ":\tfast-fail in setup; check store demo page."), tE);
		}

		StoreDemoLandingPage demoPageTemp = new StoreDemoLandingPage(driver);

		// click through to marketing page
		demoPageTemp.getButtonFindOutMore().click(); 
		demoPageTemp = null;

		// starting from storedemo (which is required in this test) you'll always land on 12
		// force redirect from 12 to 13
		StringBuilder sBuild = new StringBuilder(driver.getCurrentUrl());
		int indexBegin = sBuild.toString().indexOf("qaswebapp");
		sBuild.replace(indexBegin, (indexBegin+11), "qaswebapp13");
		driver.navigate().to(sBuild.toString());

		// wait for Get Started button on marketing page
		try { 
			new WebDriverWait(driver, 60).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@type='cta']")));
			System.out.println(methodNameLocal + ":\tmarketing page... (OK)");
		}catch(TimeoutException tE) { 
			throw new AssertionError((methodNameLocal + ":\tfast-fail in setup; check marketing page."), tE);
		}

		System.out.println(methodNameLocal + " setup complete for " + testNameIn);
	}


	@Test(enabled=true)
	public void testButtonGet_started() { 
		
		String testNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();

		setUp(testNameLocal);
		
		pageUnderTest = new MarketingPage(driver);
		
		pageUnderTest.getButtonGet_started().click();
		
		FieldValidationUtilities.sleepSecond(testNameLocal);
		
		GetStartedRequestCodePage temp = new GetStartedRequestCodePage(driver);
		
		try { // if success the accept terms will be clickable
			temp.getCheckboxTerms().click();
			System.out.println("\n" + testNameLocal + ":\tPASS");
		}catch(Exception anyExceptionWhatsoever) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			throw new AssertionError(testNameLocal,anyExceptionWhatsoever);
		}finally { 
			testNameLocal = null;
			pageUnderTest = null;
			temp = null;
		}
	}



	@Test(enabled=true)
	public void testButtonExit() { 
		
		String testNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();

		setUp(testNameLocal);

		pageUnderTest = new MarketingPage(driver);
		
		pageUnderTest.getButtonExit().click();
		
		try { 
			new WebDriverWait(driver, 30).until(ExpectedConditions.titleContains("Home Page"));
			System.out.println("\n" + testNameLocal + ":\tPASS");
		}catch(TimeoutException tE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			throw new AssertionError(testNameLocal,tE);
		}finally { 
			testNameLocal = null;
			pageUnderTest = null;
		}
	}



	@Test(enabled=true)
	public void testButtonResume(Method method) { 
	
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();

		setUp(testNameLocal);

		pageUnderTest = new MarketingPage(driver);

		pageUnderTest.getButtonResume().click();

		FieldValidationUtilities.sleepSecond(method.getName()); // sorry but it's needed here :-(  don't remove or reduce

		try { // if success expect resume page
			new WebDriverWait(driver, 30).until(ExpectedConditions.urlContains("login-resume"));
			System.out.println("\n" + testNameLocal + ":\tPASS");
		}catch(TimeoutException tE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			throw new AssertionError(testNameLocal,tE);
		}finally { 
			testNameLocal = null;
			pageUnderTest = null;
		}
	}


	/**
	 * If subclass of TestBaseSauce, pass the test result up to the Sauce Labs Dashboard.
	 * <p>
	 * Quit driver.
	 * <p>
	 * Delete the cookie this test just created.
	 */
	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {

		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		if(this.getClass().getSuperclass().getSimpleName().equals("TestBaseSauce")) { 
			System.out.println(methodNameLocal + ":\tSauce driver type detected; Sauce-specific teardown up next...");
			try { 
				((JavascriptExecutor) driver).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
			}catch(NoSuchSessionException nSSE) { 
				 // deliberate swallow
			}
		} else { 
			System.out.println(methodNameLocal + ":\tLocal driver type detected; Sauce-specific teardown skipped intentionally.\n");
		}
		driver.quit();
		System.out.println("\n" + methodNameLocal + ":\tEND tearDown.");
	}
}
