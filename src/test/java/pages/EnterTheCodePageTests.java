/*
 * Copyright Progressive Leasing LLC.
 */
package pages;

import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import common.FieldValidationUtilities;
import common.ProgressiveLeasingConstants;
import utilities.DeleteSessionCookie;
import utilities.saucelabs.TestBaseSauce;

/**
 * Test class for page object for the "Enter the code" page. 
 * <p>
 * Path: 
 * <p>
 * Store demo page > No Credit Needed<BR>Find out more | Marketing page > Get started | Request code page > Enter number, accept terms, Get started | Enter the code
 * <p>
 * Version 1.0 2019-01-16
 * <p>
 * Version 1.1 2019-03-26 hardening and automatic cleanup
 * <p>
 * Version 2.0 2019-04-16 Sauce default plus performance enhancements
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 2.0
 * @see EnterTheCodePage.java
 */
public class EnterTheCodePageTests extends TestBaseSauce {

	private WebDriver driver;

	private EnterTheCodePage pageUnderTest;

	/**
	 * This value will be set during test execution. Then passed to 
	 * a cleanup utility during teardown.
	 */
	private String cookieToDelete;

	/**
	 * In a couple places, this test will require a test email address.
	 * <p>
	 * Current possible: 000@test.com through 010@test.com
	 */
	public static final String EMAIL_FOR_THIS_TEST = "005@test.com";
	

	public void setUp(String testNameIn) {

		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();

		driver = getPreparedDriver(testNameIn);
		
		// go to demo page
		driver.navigate().to(ProgressiveLeasingConstants.URL_STORE_DEMO);

		// wait for Find out more button 
		try { 
			new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//img[@alt='Learn more about Progressive Leasing']")));
			System.out.println(methodNameLocal + ":\tstore demo landing page... (OK)");
		}catch(TimeoutException tE) { 
			throw new AssertionError((methodNameLocal + ":\tfast-fail in setup; check store demo page."), tE);
		}

		StoreDemoLandingPage demoPageTemp = new StoreDemoLandingPage(driver);

		// click through to marketing page
		demoPageTemp.getButtonFindOutMore().click(); 
		demoPageTemp = null;

		// starting from storedemo (which is required in this test) you'll always land on 12
		// force redirect from 12 to 13
		StringBuilder sBuild = new StringBuilder(driver.getCurrentUrl());
		int indexBegin = sBuild.toString().indexOf("qaswebapp");
		sBuild.replace(indexBegin, (indexBegin+11), "qaswebapp13");
		driver.navigate().to(sBuild.toString());

		// wait for Get Started button 
		try { 
			new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@type='cta']")));
			System.out.println(methodNameLocal + ":\tmarketing page... (OK)");
		}catch(TimeoutException tE) { 
			throw new AssertionError((methodNameLocal + ":\tfast-fail in setup; check marketing page."), tE);
		}

		MarketingPage marketingPage = new MarketingPage(driver);

		// click through to get started request code
		marketingPage.getButtonGet_started().click(); 
		FieldValidationUtilities.sleepSecond(methodNameLocal);
		FieldValidationUtilities.sleepSecond(methodNameLocal);
		marketingPage = null;

		// wait for Mobile phone or email field 
		try { 
			//			new WebDriverWait(driver, 20).until(ExpectedConditions.presenceOfElementLocated(By.id("otp-contact-input")));
			new WebDriverWait(driver, 20).until(ExpectedConditions.presenceOfElementLocated(By.id("pg-field-contact")));
			System.out.println(methodNameLocal + ":\tget started request code page... (OK)");
		}catch(TimeoutException tE) { 
			throw new AssertionError((methodNameLocal + ":\tfast-fail in setup; check get started request code page."), tE);
		}

		GetStartedRequestCodePage requestCodeTemp = new GetStartedRequestCodePage(driver);

		// enter a test email address or phone
		driver.findElement(By.id("pg-field-contact")).sendKeys(EMAIL_FOR_THIS_TEST);
		FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);

		// accept the terms in order to proceed to enter the code
		requestCodeTemp.getCheckboxTerms().click(); // accept terms
		FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);

		// after the checkbox, 5 tabs and a space to advance to the next page
		Actions builder = new Actions(driver);
		builder.sendKeys(Keys.TAB).build().perform();
		FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);
		builder.sendKeys(Keys.TAB).build().perform();
		FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);
		builder.sendKeys(Keys.TAB).build().perform();
		FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);
		builder.sendKeys(Keys.TAB).build().perform();
		FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);
		builder.sendKeys(Keys.TAB).build().perform();
		FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);
		builder.sendKeys(Keys.SPACE).build().perform();
		FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);

		FieldValidationUtilities.sleepSecond(methodNameLocal);

		requestCodeTemp = null;
		builder = null;

		// wait for enter the code field 
		try { 
			new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.id("pg-field-code")));
			System.out.println(methodNameLocal + ":\tenter the code page... (OK)");
		}catch(TimeoutException tE) { 
			throw new AssertionError((methodNameLocal + ":\tfast-fail in setup; check enter the code page."), tE);
		}

		System.out.println(methodNameLocal + " setup complete for " + testNameIn);
	}


	@Test(enabled=true,priority=1)
	public void testPrimary(Method method) throws InterruptedException { 
		
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		
		setUp(testNameLocal);
		
		String temp = driver.getCurrentUrl();
		driver.navigate().to(temp);
		FieldValidationUtilities.sleepSecond(testNameLocal);
		temp = null;

		pageUnderTest = new EnterTheCodePage(driver);
		
		WebElement fieldTemp = pageUnderTest.getFieldEnterTheCode();
		try { 
			Assert.assertNotNull(fieldTemp);
		}catch(AssertionError aE) {
			System.out.println(testNameLocal + ":\tFAIL; page object not returning a field");
			System.out.println(testNameLocal + ":\tfailed call: getFieldEnterTheCode()");
			throw aE;
		}
		
		// enter the (test) code and take a nap, unfortunately
		fieldTemp.sendKeys("000000");
		// yeah the sleeps suck, thank Magento, move on....
		try { 
			Thread.sleep(5000); // Don't remove or reduce. It's not our issue.
		}catch(InterruptedException iE) { 
			throw new AssertionError((testNameLocal + ":\tfast-fail in setup"), iE);
		}
		
		// wait for basic info page -- if present this test class is PASS
		try { // first and last name fields mean this test passes
			new WebDriverWait(driver, 60).until(ExpectedConditions.presenceOfElementLocated(By.id("first-name-input")));
			captureSessionCookieToDelete(); // automatically delete the session cookie which by this time exists
			System.out.println(testNameLocal + ":\tPASS; advanced to basic info page");
		}catch(TimeoutException tE) { 
			throw new AssertionError((testNameLocal + ":\tFAIL; didn't reach basic info page as expected"), tE);
		}finally {
			fieldTemp = null;
			testNameLocal = null;
		}
	}
	
	private void captureSessionCookieToDelete() { 
		System.out.println("\n====cookie to console====");
		java.util.Set<Cookie> cookies = driver.manage().getCookies();
		for(Cookie C : cookies) { 
			if(!(C.getName().equals("sessionId"))) { 
				continue;
			}
			cookieToDelete = C.getValue();
			System.out.print("To delete:\t" + cookieToDelete);
		}
		System.out.println("\n====cookie to console====\n");
		cookies = null;
	}



	/**
	 * If subclass of TestBaseSauce, pass the test result up to the Sauce Labs Dashboard.
	 * <p>
	 * Quit driver.
	 * <p>
	 * Delete the cookie this test just created.
	 */
	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {

		@SuppressWarnings("unused")
		DeleteSessionCookie deleteCookie = new DeleteSessionCookie(cookieToDelete,EMAIL_FOR_THIS_TEST);

		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		if(this.getClass().getSuperclass().getSimpleName().equals("TestBaseSauce")) { 
			System.out.println(methodNameLocal + ":\tSauce driver type detected; Sauce-specific teardown up next...");
			((JavascriptExecutor) driver).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
		} else { 
			System.out.println(methodNameLocal + ":\tLocal driver type detected; Sauce-specific teardown skipped intentionally.\n");
		}
		driver.quit();
		pageUnderTest = null;
		System.out.println("\n" + methodNameLocal + ":\tEND tearDown.");
	}
}
