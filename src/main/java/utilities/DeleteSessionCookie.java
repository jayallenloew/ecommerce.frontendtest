package utilities;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * A successful ecom front end test generates a cookie that needs to be deleted, so 
 * that it doesn't block the flow the next time through.
 * <p>
 * Capture the cookie, then pass it here for destruction.
 * <p>
 * This utility assumes the email address used in the calling test is in the format 000@test.com.
 * <p>
 * If you're using another random test email address, this won't work.
 * <p>
 * This utility also assumes QA13 test environment for now (currently the 
 * only environment on which these tests are run). We'll parameterize the 
 * environments later.
 * <p>
 * Version 1.1 2019-03-26 code review suggestions CJ
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @author <a href="mailto:christopher.rains@progleasing.com">CJ Rains</a>
 * @version 1.1
 */
public class DeleteSessionCookie {
	
	private String cookieToDelete;
	private String emailAddressUsed;
	
	private SSLContext ctx;
	
	private URL uRL = null;
	
	private HttpsURLConnection con = null;
	
	/**
	 * The only constructor is public and requires one String argument, 
	 * which must be the valid cookie. As long as your String argument 
	 * is a real cookie, the constructor will do the rest.
	 * <p>
	 * @param cookieToDelete
	 */
	public DeleteSessionCookie(String cookieToDelete, String emailAddressUsed) { 
		
		this.cookieToDelete = cookieToDelete;
		this.emailAddressUsed = emailAddressUsed;

		doSSLContextSetup();
		doURLSetup();
		doHttpsURLConnectionSetup();
		doAllConSetup();

	}
	
	private void doSSLContextSetup() { 
		try {
			ctx = SSLContext.getInstance("TLS");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		try {
			ctx.init(new KeyManager[0], new TrustManager[] { new DefaultTrustManager()},  new SecureRandom());
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
		SSLContext.setDefault(ctx);
	}
	
	private void doURLSetup() { 
		try {
			uRL = new URL("https://vdc-qaswebapp13.stormwind.local/ecommsession/api/v1/checkoutsession/resume/login?AuthenticationId=" + this.emailAddressUsed.substring(0, 3) + "%40test.com&SessionId=" + cookieToDelete);
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		}
	}
	
	private void doHttpsURLConnectionSetup() { 
		try {
			con = (HttpsURLConnection) uRL.openConnection();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void doAllConSetup() { 
		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		con.setHostnameVerifier(new HostnameVerifier() {
			@Override
			public boolean verify(String arg0, SSLSession arg1) { 
				return true;
			}
		});
		
		con.setRequestProperty("Authorization", "Basic cHJvY2Vzc01hbmFnZXI6KWs8TmY9MlwpeXpweWJM");
		try {
			con.setRequestMethod("DELETE");
			try {
				con.connect();
			} catch (IOException e) {
				e.printStackTrace();
			}
			int responseCodeTemp = 0;
			try {
				responseCodeTemp = con.getResponseCode();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			if(responseCodeTemp==204) { 
				System.out.println("\n" + methodNameLocal + ":\t  deleted " + this.cookieToDelete);
			}
		} catch (ProtocolException e) {
			e.printStackTrace();
		}finally { 
			con.disconnect();
			System.out.println(methodNameLocal + ":\t  disconnected");
		}
	}
	
	
	private static class DefaultTrustManager implements X509TrustManager { 

		@Override
		public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException { }

		@Override
		public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException { }

		@Override
		public X509Certificate[] getAcceptedIssuers() {
			return null;
		}
	}	
	
}
