/*
 * Copyright Progressive Leasing LLC.
 */
package utilities;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

/**
 * Use this one time to generate a unique password that you will use 
 * for encryption and decryption.
 * <p>
 * Then come back and do it again periodically as you change this 
 * password. For example, if you make this password the same as 
 * your LDAP (and you can), return to this class and re-run it  
 * whenever you change your LDAP.
 * <P>
 * This class generates a hashed value to be stored in your 
 * encryption properties file, typically in src/test/resources. 
 * Then downstream tests will consume it to decrypt an encrypted 
 * value on the fly.
 * <p>
 * The generated value is printed to console to be copied and pasted 
 * one time. It is not returned. It is not persisted.
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.0
 */
public class Encryption_A_CreateKey {
	
	/**
	 * An instance of this Jasypt class is required for encryption or decryption.
	 */
	private StandardPBEStringEncryptor spbesEncryptor;
	
	/**
	 * Change this to some password this class will need in order 
	 * to generate an encrypted value.
	 * <p>
	 * DO NOT PERSIST your change!
	 * <p>
	 * Run the main method.
	 * <p>
	 * Then capture the encrypted value that gets printed out to the console.
	 * <p>
	 * Then abort your temporary change, possibly like this:
	 * <p>
	 * <pre>git checkout -- /path/to/CreateEncryptionPassword.java</pre>
	 * <p>
	 * or like this, if you understand the impact,
	 * <p>
	 * <pre>git reset HEAD --hard</pre> 
	 */
	private String myEncryptionPassword = 
			"Temporarily overwrite but DO NOT PERSIST";
	
	public Encryption_A_CreateKey() { 
		// a single contrustor takes no arguments
	} 
	
	public static void main(String[] args) {
		
		Encryption_A_CreateKey createMyKey = new Encryption_A_CreateKey();
				
		createMyKey.spbesEncryptor = new StandardPBEStringEncryptor();
		
		createMyKey.spbesEncryptor.setPassword(createMyKey.myEncryptionPassword);
				
		createMyKey.spbesEncryptor.setAlgorithm("PBEWithMD5AndTripleDES");
		
		String encryptedText = createMyKey.spbesEncryptor.encrypt(createMyKey.myEncryptionPassword);
		
		System.out.println("Your key:\t" + encryptedText);
		
		createMyKey.spbesEncryptor = null;
		
		encryptedText = null;
		
		createMyKey = null;
				
	}

}
