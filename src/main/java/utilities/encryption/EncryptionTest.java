package utilities.encryption;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

public class EncryptionTest {

	public static void main(String[] args) {
		
		StandardPBEStringEncryptor encrypt = new StandardPBEStringEncryptor();
		encrypt.setPassword("Password01"); // not secure - experiment
		encrypt.setAlgorithm("PBEWithMD5AndTripleDES");
		String toEncrypt = "BaileyPup";
		String encrypted = encrypt.encrypt(toEncrypt);
		System.out.println("encrypted " + encrypted);
		String decrypted = encrypt.decrypt(encrypted);
		System.out.println("decrypted " + decrypted);
		
	}

}
