/*
 * Copyright Progressive Leasing LLC.
 */
package utilities.encryption;

import java.io.UnsupportedEncodingException;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import java.util.Arrays;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class EncryptionDemo {

	private static SecretKeySpec secretKey;
	private static byte[] keyByteArray;

	public static void setMyKey(String myKey) {

		if(myKey.trim().length() < 7) { 
			throw new IllegalArgumentException("invalid length");
		}
		MessageDigest sha = null;
		try { 
			keyByteArray = myKey.getBytes("UTF-8");
			sha = MessageDigest.getInstance("SHA-1");
			keyByteArray = sha.digest(keyByteArray);
			keyByteArray = Arrays.copyOf(keyByteArray,16);
			secretKey = new SecretKeySpec(keyByteArray,"AES");
		}catch(NoSuchAlgorithmException nSAE) { 
			nSAE.printStackTrace();
		}catch(UnsupportedEncodingException uEE) { 
			uEE.printStackTrace();
		}
	}

	public static String getThisValueEncrypted(String toEncrypt, String secret) { 
		try { 
			setMyKey(secret);
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, secretKey);
			return Base64.getEncoder().encodeToString(cipher.doFinal(toEncrypt.getBytes("UTF-8")));
		} catch (Exception eX) {
			System.out.println("Error while encrypting");
			eX.printStackTrace();
		}
		return null;
	}

	public static String getThisValueDecrypted(String toDecrypt, String secret) { 
		try { 
			setMyKey(secret);
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			cipher.init(Cipher.DECRYPT_MODE, secretKey);
			return new String(cipher.doFinal(Base64.getDecoder().decode(toDecrypt)));
		} catch (Exception eX) {
			System.out.println("Error while decrypting");
			eX.printStackTrace();
		}
		return null;
	}


	public static void main(String[] args) {

		final String toActUpon = "Progressive Leasing Draper";
		final String myNotSoSecretKey = "Testing99";

		String encryptedString = EncryptionDemo.getThisValueEncrypted(toActUpon, myNotSoSecretKey);
		String decryptedString = EncryptionDemo.getThisValueDecrypted(encryptedString, myNotSoSecretKey);

		System.out.println("A String to act upon:\t" + toActUpon);
		System.out.println("The value encrypted:\t" + encryptedString);
		System.out.println("The value decrypted:\t" + decryptedString);

	}

}
