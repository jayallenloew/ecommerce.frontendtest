/*
 * Copyright Progressive Leasing LLC.
 */
package utilities.encryption;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import java.util.Properties;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 * Fetch existing encryption password from properties file, then encrypt a given value.
 * <p>
 * First, create your key (see A_CreateMyKey.java), and put it in your encryption properties file.
 * <p>
 * Assumption: values to encrypt are > 8 chars long.
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.0 2019-01-04
 * @see A_CreateMyKey.java
 */
public class B_Encrypt {

	/**
	 * Change this to the full path to your encryption properties file in src/test/resources.
	 */
	public static final String pathToMyEncryptionProperties = 
			"./src/test/resources/encryption_aloew.properties";

	/*
	 * Don't mess with these...
	 */
	private static SecretKeySpec secretKey;
	private static byte[] keyByteArray;
	private Properties props;
	private FileInputStream streamIn;
	private File fileIn;

	/**
	 * Internal, private method fetches raw key from properties file.
	 * <p>
	 * Don't refactor. 
	 * @return - Raw key from file.
	 */
	private String getMyKey() { 
		props = new Properties();
		fileIn = new File(pathToMyEncryptionProperties);
		try {
			streamIn = new FileInputStream(fileIn);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		try {
			props.load(streamIn);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return props.getProperty("key");
	}

	/**
	 * Internal, private method sets secret key.
	 * <p>
	 * Don't refactor. 
	 * @return - Raw key from file.
	 * @throws IllegalArgumentException If length < 7.
	 */
	private void setMyKey(String myKey) {

		if(myKey.trim().length() < 7) { 
			throw new IllegalArgumentException("invalid length");
		}
		MessageDigest sha = null;
		try { 
			keyByteArray = myKey.getBytes("UTF-8");
			sha = MessageDigest.getInstance("SHA-1");
			keyByteArray = sha.digest(keyByteArray);
			keyByteArray = Arrays.copyOf(keyByteArray,16);
			secretKey = new SecretKeySpec(keyByteArray,"AES");
		}catch(NoSuchAlgorithmException nSAE) { 
			nSAE.printStackTrace();
		}catch(UnsupportedEncodingException uEE) { 
			uEE.printStackTrace();
		}
	}

	/**
	 * Return an encrypted equivalent of whatever String you pass in.
	 * <p>
	 * @param toEncrypt A readable String to be encrypted.
	 * @return Encrypted result.
	 * @throws IllegalArgumentException When value to encrypt is null or < 8 chars long.
	 */
	public String getThisValueEncrypted(String toEncrypt) { 
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();
		try { 
			if(null==toEncrypt) { 
				throw new IllegalArgumentException(methodNameLocal + ":\t" + "not null please");
			}
		}catch(NullPointerException nPE) { 
			// deliberate swallow
		}
		if(toEncrypt.trim().length()<8) { 
			throw new IllegalArgumentException(methodNameLocal + ":\t" + "at least 8 characters please");
		}
		try { 
			setMyKey(this.getMyKey());
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, secretKey);
			return Base64.getEncoder().encodeToString(cipher.doFinal(toEncrypt.getBytes("UTF-8")));
		} catch (Exception eX) {
			System.out.println("Error while encrypting");
		}
		return null;
	}
	
}
