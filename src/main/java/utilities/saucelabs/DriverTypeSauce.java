package utilities.saucelabs;

/**
 * Currently supported OS-browser combinations for front end 
 * testing in Sauce Labs. This collection is expected to change 
 * over time.
 * <p>
 * Version 1.0 2019-03-14
 * <p>
 * Version 1.1 2019-03-27
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a> 
 * @version 1.1
 */
public enum DriverTypeSauce {
	WINDOWS_CHROME,
	WINDOWS_FIREFOX,
	WINDOWS_IE,
	MAC_CHROME,
	MAC_FIREFOX,
	MAC_SAFARI,
	IPHONE_8_SIM,
	IPHONE_X_SIM,
	ANDROID_GALAXY_S4_SIM;
}
