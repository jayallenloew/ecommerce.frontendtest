package utilities.saucelabs;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import common.SauceLabsConstants;

/**
 * Simplify driver instantiation for tests that will run in the 
 * Sauce Labs cloud.
 * <p>
 * Version 1.0 - 2019-03-14
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a> 
 * @version 1.0
 * @see utilities.saucelabs.CommonDriversSauceTests.java
 * @see common.SauceLabsConstants.java
 */
public class CommonDriverSauceDesktop {

	private WebDriver driverToReturn;	// Selenium
	private DesiredCapabilities dCaps;	// Selenium and Sauce Labs
	private DriverTypeSauce driverTypeSauce;	// Sauce Labs and Progressive
	private DataCenter dataCenterSauce;	// Sauce Labs
	private String nameOfCallingTest = "not yet set"; // all you :-)

	/**
	 * Select a driver type from the enum: which OS-browser combination 
	 * do you want for the test you are about to run? (These options can 
	 * and will change over time.)
	 * <p>
	 * Select a data center: USA or EU?
	 * <p>
	 * Pass in a free-form String value (heads up, it's unchecked) 
	 * representing the name of your tunnel. The prepared driver you 
	 * get from this class will push your test through this tunnel, 
	 * so make sure the tunnel is open first.
	 * <p>
	 * @param driverType - An element from the enum for driver type.
	 * @param dataCenter - An element from the enum for data center.
	 * @param nameOfYourTunnel - An unchecked String: the name of your tunnel.
	 */
	public CommonDriverSauceDesktop(DriverTypeSauce driverType, DataCenter dataCenter) { 

		this.driverTypeSauce = driverType;
		this.dataCenterSauce = dataCenter;

		instantiateDesiredCapabilities();
		authenticateSauceLabs();
		setSauceLabsGlobalTimeouts();
		setTunnelSauceLabs();

	}

	/**
	 * Sauce Labs uses the DesiredCapabilities class to set numerous 
	 * required and optional values.
	 */
	@SuppressWarnings("static-access")
	private void instantiateDesiredCapabilities() { 
		if(this.driverTypeSauce.equals(DriverTypeSauce.WINDOWS_CHROME)) { 
			dCaps = new DesiredCapabilities();
		}
		if(this.driverTypeSauce.equals(DriverTypeSauce.WINDOWS_FIREFOX) || this.driverTypeSauce.equals(DriverTypeSauce.MAC_FIREFOX)) { 
			dCaps = new DesiredCapabilities().firefox();
		}
		if(this.driverTypeSauce.equals(DriverTypeSauce.WINDOWS_IE)) { 
			dCaps = new DesiredCapabilities().internetExplorer();
		}
		if(this.driverTypeSauce.equals(DriverTypeSauce.MAC_CHROME)) { 
			dCaps = new DesiredCapabilities().chrome();
		}
		if(this.driverTypeSauce.equals(DriverTypeSauce.MAC_SAFARI)) { 
			dCaps = new DesiredCapabilities().safari();
		}
	}

	/**
	 * Your Sauce Labs username and access key in src/test/resources/.env
	 */
	private final void authenticateSauceLabs() { 
		dCaps.setCapability("username", System.getenv("SAUCE_USERNAME"));
		dCaps.setCapability("accessKey", System.getenv("SAUCE_ACCESS_KEY"));		
	}


	/**
	 * This Sauce Labs test will try to run in this tunnel, so make 
	 * sure it's open first.
	 * <p>
	 * If this tunnel isn't open when your test begins, your test 
	 * will fail from the setup method in a recognizable way. Start 
	 * up your tunnel, and try again.
	 */
	private final void setTunnelSauceLabs() { 
		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		System.out.println("\n" + methodNameLocal + ":\tyour tunnel is " + System.getenv("SAUCE_TUNNEL"));
		dCaps.setCapability("tunnelIdentifier", System.getenv("SAUCE_TUNNEL"));
	}
	
	/**
	 * 
	 * @param nameOfCallingTest - The test name will appear in your Dashboard.
	 */
	public void setNameOfCallingTest(String nameOfCallingTest) { 
		this.nameOfCallingTest = nameOfCallingTest;
	}
	public String getNameOfCallingTest() { 
		return this.nameOfCallingTest;
	}

	
	/**
	 * The next three values are defensive, and appropriate for most of our testing. 
	 * <p>
	 * These values can be overridden, but please be careful. And do it on your 
	 * own branch. 
	 * <p>
	 * Whatever these values are set to, they will apply to all tests running 
	 * in Sauce Labs.
	 * <p>
	 * See the JavaDoc for these values in SauceLabsConstants.java.
	 * <p>
	 * @see src/main/java/common/SauceLabsConstants.java
	 */
	private final void setSauceLabsGlobalTimeouts() { 
		dCaps.setCapability("commandTimeout", SauceLabsConstants.COMMAND_TIMEOUT);
		dCaps.setCapability("idleTimeout", SauceLabsConstants.IDLE_TIMEOUT);
		dCaps.setCapability("maxDuration", SauceLabsConstants.MAX_DURATION);		
	}

	/**
	 * Call this method to return a ready-to-use driver instance.
	 * <p>
	 * From within your test, do something like this:
	 * <p>
	 * <pre>
	 * driver = new CommonDriversSauce(DriverType.WINDOWS_CHROME, DataCenter.USA, "AllenTunnel").getPreparedDriver(method.getName());
	 * </pre>
	 * @param getNameOfCallingTest() - The test name will appear in your Dashboard.
	 * @return - An instance of RemoteWebDriver configured for immediate use in a test.
	 */
	public WebDriver getPreparedDriver() { 

		switch(this.driverTypeSauce) { 


		case WINDOWS_CHROME : { 

			/*
			 * OS-browser combination.
			 * <p>
			 * The 'latest-1' argument commands 1 version earlier than 
			 * whatever the latest supported version is.
			 */
			dCaps.setCapability("browserName", "Chrome");
			dCaps.setCapability("platform", "Windows 10");
			dCaps.setCapability("version", "latest-1");

			/*
			 * Name of currently running test will appear in Sauce Labs Dashboard.
			 */
			dCaps.setCapability("name", getNameOfCallingTest());


			URL toDeviceCloud = null;
			try {
				if(this.dataCenterSauce.equals(DataCenter.USA)) { 
					toDeviceCloud = new URL(SauceLabsConstants.URL_DEVICE_CLOUD_USA);
				}else if(this.dataCenterSauce.equals(DataCenter.EU)) { 
					toDeviceCloud = new URL(SauceLabsConstants.URL_DEVICE_CLOUD_EU);
				}
			} catch (MalformedURLException mUE) {
				System.out.println(getNameOfCallingTest() + ":\tError creating new URL from input");
				throw new IllegalStateException(getNameOfCallingTest(), mUE);
			}

			driverToReturn = new RemoteWebDriver(toDeviceCloud,dCaps);

			break;
		} // end case


		case WINDOWS_FIREFOX : { 

			/*
			 * OS-browser combination.
			 * <p>
			 * The 'latest-1' argument commands 1 version earlier than 
			 * whatever the latest supported version is.
			 */
			dCaps.setCapability("platform", "Windows 10");
			dCaps.setCapability("version", "latest-1");

			/*
			 * Name of currently running test will appear in Sauce Labs Dashboard.
			 */
			dCaps.setCapability("name", getNameOfCallingTest());


			URL toDeviceCloud = null;
			try {
				if(this.dataCenterSauce.equals(DataCenter.USA)) { 
					toDeviceCloud = new URL(SauceLabsConstants.URL_DEVICE_CLOUD_USA);
				}else if(this.dataCenterSauce.equals(DataCenter.EU)) { 
					toDeviceCloud = new URL(SauceLabsConstants.URL_DEVICE_CLOUD_EU);
				}
			} catch (MalformedURLException mUE) {
				System.out.println(getNameOfCallingTest() + ":\tError creating new URL from input");
				throw new IllegalStateException(getNameOfCallingTest(), mUE);
			}

			driverToReturn = new RemoteWebDriver(toDeviceCloud,dCaps);

			break;
		} // end case


		case WINDOWS_IE : { 

			/*
			 * OS-browser combination.
			 * <p>
			 * The 'latest-1' argument commands 1 version earlier than 
			 * whatever the latest supported version is.
			 */
			dCaps.setCapability("platform", "Windows 10");
			dCaps.setCapability("version", "11.285"); // the only supported version
			
			/*
			 * Name of currently running test will appear in Sauce Labs Dashboard.
			 */
			dCaps.setCapability("name", getNameOfCallingTest());


			URL toDeviceCloud = null;
			try {
				if(this.dataCenterSauce.equals(DataCenter.USA)) { 
					toDeviceCloud = new URL(SauceLabsConstants.URL_DEVICE_CLOUD_USA);
				}else if(this.dataCenterSauce.equals(DataCenter.EU)) { 
					toDeviceCloud = new URL(SauceLabsConstants.URL_DEVICE_CLOUD_EU);
				}
			} catch (MalformedURLException mUE) {
				System.out.println(getNameOfCallingTest() + ":\tError creating new URL from input");
				throw new IllegalStateException(getNameOfCallingTest(), mUE);
			}

			driverToReturn = new RemoteWebDriver(toDeviceCloud,dCaps);

			break;
		} // end case
		


		case MAC_CHROME : { 

			/*
			 * OS-browser combination.
			 * <p>
			 * The 'latest-1' argument commands 1 version earlier than 
			 * whatever the latest supported version is.
			 */
			dCaps.setCapability("platform", "macOS 10.14");
			dCaps.setCapability("version", "latest-1");			
			
			/*
			 * Name of currently running test will appear in Sauce Labs Dashboard.
			 */
			dCaps.setCapability("name", getNameOfCallingTest());


			URL toDeviceCloud = null;
			try {
				if(this.dataCenterSauce.equals(DataCenter.USA)) { 
					toDeviceCloud = new URL(SauceLabsConstants.URL_DEVICE_CLOUD_USA);
				}else if(this.dataCenterSauce.equals(DataCenter.EU)) { 
					toDeviceCloud = new URL(SauceLabsConstants.URL_DEVICE_CLOUD_EU);
				}
			} catch (MalformedURLException mUE) {
				System.out.println(getNameOfCallingTest() + ":\tError creating new URL from input");
				throw new IllegalStateException(getNameOfCallingTest(), mUE);
			}

			driverToReturn = new RemoteWebDriver(toDeviceCloud,dCaps);

			break;
		} // end case
		


		case MAC_FIREFOX : { 

			/*
			 * OS-browser combination.
			 * <p>
			 * The 'latest-1' argument commands 1 version earlier than 
			 * whatever the latest supported version is.
			 */
			dCaps.setCapability("platform", "macOS 10.14");
			dCaps.setCapability("version", "latest-1");			
			
			/*
			 * Name of currently running test will appear in Sauce Labs Dashboard.
			 */
			dCaps.setCapability("name", getNameOfCallingTest());


			URL toDeviceCloud = null;
			try {
				if(this.dataCenterSauce.equals(DataCenter.USA)) { 
					toDeviceCloud = new URL(SauceLabsConstants.URL_DEVICE_CLOUD_USA);
				}else if(this.dataCenterSauce.equals(DataCenter.EU)) { 
					toDeviceCloud = new URL(SauceLabsConstants.URL_DEVICE_CLOUD_EU);
				}
			} catch (MalformedURLException mUE) {
				System.out.println(getNameOfCallingTest() + ":\tError creating new URL from input");
				throw new IllegalStateException(getNameOfCallingTest(), mUE);
			}

			driverToReturn = new RemoteWebDriver(toDeviceCloud,dCaps);

			break;
		} // end case


		case MAC_SAFARI : { 

			/*
			 * OS-browser combination.
			 * <p>
			 * The 'latest-1' argument commands 1 version earlier than 
			 * whatever the latest supported version is.
			 */
			dCaps.setCapability("platform", "macOS 10.14");
			dCaps.setCapability("version", "12.0");	// the only supported version		
			
			/*
			 * Name of currently running test will appear in Sauce Labs Dashboard.
			 */
			dCaps.setCapability("name", getNameOfCallingTest());


			URL toDeviceCloud = null;
			try {
				if(this.dataCenterSauce.equals(DataCenter.USA)) { 
					toDeviceCloud = new URL(SauceLabsConstants.URL_DEVICE_CLOUD_USA);
				}else if(this.dataCenterSauce.equals(DataCenter.EU)) { 
					toDeviceCloud = new URL(SauceLabsConstants.URL_DEVICE_CLOUD_EU);
				}
			} catch (MalformedURLException mUE) {
				System.out.println(getNameOfCallingTest() + ":\tError creating new URL from input");
				throw new IllegalStateException(getNameOfCallingTest(), mUE);
			}

			driverToReturn = new RemoteWebDriver(toDeviceCloud,dCaps);

			break;
		} // end case		
		
		}		

		return driverToReturn;		
	}


}
