package common;

/**
 * Issuing State drop-down menu contains 51 entries as follows:
 * <ul>
 * <li>'N/A" for federal (a military ID, a passport...).</li>
 * <li>50 entries for the American states</li>
 * </ul>
 * <p>
 * User has option of typing in the USPS-approved two-letter abbreviation, 
 * or the proper name, i.e. 'AK' or 'Alaska'.
 * <p>
 * Version 1.0 2019-02-06
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.0
 */
public enum IssuingState {
	
	AK("Alaska"),
	AL("Alabama"),
	AR("Arkansas"),
	AZ("Arizona"),
	CA("California"),
	CO("Colorado"),
	CT("Connecticut"),
	DE("Deleware"),
	FL("Florida"),
	GA("Georgia"),
	HI("Hawaii"),
	IA("Iowa"),
	ID("Idaho"),
	IL("Illinois"),
	IN("Indiana"),
	KS("Kansas"),
	KY("Kentucky"),
	LA("Louisiana"),
	MA("Massachusetts"),
	MD("Maryland"),
	ME("Maine"),
	MI("Michigan"),
	MN("Minnesota"),
	MO("Missouri"),
	MS("Mississippi"),
	MT("Montana"),
	NA("N/A"), // this is a legal value (it's for federal)
	NC("North Carolina"),
	ND("North Dakota"),
	NE("Nebraska"),
	NH("New Hampshire"),
	NJ("New Jersey"),
	NM("New Mexico"),
	NV("Nevada"),
	NY("New York"),
	OH("Ohio"),
	OK("Oklahoma"),
	OR("Oregon"),
	PA("Pennsylvania"),
	RI("Rhode Island"),
	SC("South Carolina"),
	SD("South Dakota"),
	TN("Tennessee"),
	TX("Texas"),
	UT("Utah"),
	VT("Vermont"),
	VA("Virginia"),
	WA("Washington"),
	WI("Wisconsin"),
	WV("West Virginia"),
	WY("Wyoming");
	
	public String properName;
	
	IssuingState(String properName) { 
		this.properName = properName;
	}
	
	
}
