package common;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

/**
 * Abstract out shared functions commonly called from field validation tests.
 * <p>
 * Version 1.1 2019-03-05 add vertical scroll, fix, and re-test
 * <p>
 * @author <a href="mailto:jay.loew@progleasing.com">Allen</a>
 * @author <a href="mailto:shawn.hartke@progleasing.com">Shawn</a>
 * @author <a href="mailto:christopher.rains@progleasing.com">CJ Rains</a>
 * @version 1.1
 */
public class FieldValidationUtilities {
	
	public static void sleepQuarterSecond(String testNameIn) { 
		try { 
			Thread.sleep(250);
		}catch(InterruptedException iE) { 
			System.out.println(testNameIn + " sleep issues:\t" + iE.getMessage());
		}		
	}
	
	public static void sleepSecond(String testNameIn) { 
		try { 
			Thread.sleep(1000);
		}catch(InterruptedException iE) { 
			System.out.println(testNameIn + " sleep issues:\t" + iE.getMessage());
		}		
	}	

	/**
	 * Refresh to clear all messages between tests. Particularly useful 
	 * for test classes with static setup.
	 * <p>
	 * @param testNameIn
	 */
	public static void refreshBetweenTest(String testNameIn, WebDriver driverIn) { 
		driverIn.navigate().refresh();
		FieldValidationUtilities.sleepQuarterSecond(testNameIn);
	}
	
	
	public static void tabOut(String testNameIn, WebDriver driverIn) { 
		Actions builder = new Actions(driverIn);
		builder.sendKeys(Keys.TAB).build().perform();
		FieldValidationUtilities.sleepSecond(testNameIn);
	}
	

}
