/*
 * Copyright Progressive Leasing LLC.
 */
package common;

/**
 * A common class for constants, following Oracle's example. 
 * <p>
 * For any constants specific to Progressive Leasing: URLs, titles, and so on.
 * <p>
 * Add anything you like here, so long as you make it a constant.
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.0
 */
public class ProgressiveLeasingConstants {
	
	/**
	 * Do not instantiate this class.
	 */
	private ProgressiveLeasingConstants() { 
		// A single private no-args.
		// Do not instantiate.
	}
	
	/**
	 * Demo store often used in testing.
	 */
	public static final String URL_STORE_DEMO = "http://storedemo.progressivelp.com/";
	
	/**
	 * Our customer-facing landing page.
	 */
	public static final String URL_LANDING_PRODUCTION = "https://progleasing.com/";
	
	
	/**
	 * URL for direct navigation to welcome page, also called marketing page.
	 * <p>
	 * "No credit needed, pay over time!"
	 * <p>
	 * "Lease with Progressive and pay for your items...."
	 * <p>
	 * For field validation only. For functional testing, you can't use 
	 * the direct nav URL.
	 * <p>
	 * @return a String representing a limited, direct nav URL.
	 */
	public static final String URL_WELCOME_12 = 
			"https://vdc-qaswebapp12.stormwind.local/eComUI/#/welcome";

	public static final String URL_WELCOME_13 = 
			"https://vdc-qaswebapp13.stormwind.local/eComUI/#/welcome";

	
	public static final String URL_GET_STARTED_REQUEST_CODE_13 = 
			"https://vdc-qaswebapp13.stormwind.local/eComUI/#/apply/get-started";

	public static final String URL_GET_STARTED_REQUEST_CODE_12 = 
			"https://vdc-qaswebapp12.stormwind.local/eComUI/#/apply/get-started";
	
	
	public static final String URL_BASIC_INFO_PAGE_13 = 
			"https://vdc-qaswebapp13.stormwind.local/eComUI/#/apply/basic-info";

	public static final String URL_BASIC_INFO_PAGE_12 = 
			"https://vdc-qaswebapp12.stormwind.local/eComUI/#/apply/basic-info";

	
	public static final String URL_API_12 =
			"http://vdc-qaswebapp12.stormwind.local/eComApi";

	public static final String URL_API_13 =
			"http://vdc-qaswebapp13.stormwind.local/eComApi";
	
	/**
	 * URL for direct navigation to contact info page ("How can we contact you?")
	 * <p>
	 * For field validation only. For functional testing, you can't use 
	 * the direct nav URL.
	 * <p>
	 * @return a String representing a limited, direct nav URL.
	 */
	public static final String URL_CONTACT_INFO_12 = 
			"https://vdc-qaswebapp12.stormwind.local/eComUI/#/apply/contact-info";
	
	public static final String URL_CONTACT_INFO_13 = 
			"https://vdc-qaswebapp13.stormwind.local/eComUI/#/apply/contact-info";
	
	
	/**
	 * URL for direct navigation to bank info page ("Your bank account")
	 * <p>
	 * Three fields:
	 * <ul>
	 * <li>Bank routing number</li>
	 * <li>Checking account number</li>
	 * <li>Monthly Income</li>
	 * </ul>
	 * <p>
	 * 'income-info' is the URL parameter and 'bank info' the spoken identifier
	 * <p>
	 * For field validation only. For functional testing, you can't use 
	 * the direct nav URL.
	 * <p>
	 * @return a String representing a limited, direct nav URL.
	 */
	public static final String URL_BANK_INFO_12 = 
			"https://vdc-qaswebapp13.stormwind.local/eComUI/#/apply/income-info";
	
	public static final String URL_BANK_INFO_13 = 
			"https://vdc-qaswebapp13.stormwind.local/eComUI/#/apply/income-info";

	
	public static final String[] ADDRESS_DRAPER_HQ = { 
			"256 W Data Dr",
			"Draper",
			"UT",
			"84020"
	};
	
	/**
	 * Basic Info page requires name, birth date, SSN, DL/GID, state.
	 */
	public static final String[] BASIC_INFO_555121212 = {
			"Tester", // nameFirst 0
			"Lester", // nameLast 1
			"09",     // month 2
			"10",     // day 3
			"1985",   // year 4
			"555",    // first three SSN 5
			"12",     // middle two SSN 6
			"1212",   // last four SSN 7
			"12345",  // driver license number 8
			"UT"      // two letter state 9
	};

	
	/**
	 * Contact Info page requires an email address (not here) plus this.
	 * <p>
	 * In most of our tests, address line 2 is not used, hence null here.
	 */
	public static final String[] CONTACT_INFO_84095 = { 
			"8014464357",
			"1600 Towne Center Dr",
			null, // address line 2; override it if you need it
			"South Jordan",
			"UT",
			"84095"
	};

	
	/**
	 * Required on the Bank Info page, this is the only known valid value for testing.
	 */
	public static final String ROUTING_NUMBER_FOR_TESTING = "000000000";

	
	/**
	 * A few email addresses valid for use in the "mobile or email" field 
	 * during ECOM tests.
	 */
	public static final String[] TEST_DOT_COM_ADDRESSES = {
			"000@test.com",
			"001@test.com",
			"002@test.com",
			"003@test.com",
			"004@test.com",
			"005@test.com",
			"006@test.com",
			"007@test.com",
			"008@test.com",
			"009@test.com",
			"010@test.com"
	};
	
}
