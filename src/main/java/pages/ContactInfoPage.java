/*
 * Copyright Progressive Leasing LLC.
 */
package pages;

import java.util.HashSet;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import common.FieldValidationUtilities;
import utilities.WebElementGetter;

/**
 * Test class for page object for the contact info page ("How can we contact you?").
 * <p>
 * Version 1.0 2019-01-22
 * <p>
 * Version 1.1 2019-02-21 add happy path populate method
 * <p>
 * Version 1.1 2019-05-03 fix troublesome checkbox
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @see ContactInfoPageTests.java
 */
public class ContactInfoPage {

	private WebDriver driverIn;

	public ContactInfoPage(WebDriver driverIn) { 
		this.driverIn = driverIn;
		FieldValidationUtilities.sleepSecond(getClass().getSimpleName());
	}


	/**
	 * Fetch an instance of org.openqa.selenium.WebElement representing the Email address field, 
	 * known in TestLink as the Add Email field.
	 * <p>
	 * It is the first field on this page.
	 * <p>
	 * It is a required field.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getFieldEmail_address() { 

		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.id("email-input"));
		locators.add(By.cssSelector("#email-input"));
		locators.add(By.xpath("//input[@id='email-input']"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}			



	/**
	 * Fetch an instance of org.openqa.selenium.WebElement representing the area code 
	 * portion of the mobile phone field.
	 * <p>
	 * It is a required field.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getFieldAreaCode() { 

		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.id("mobile-phone-area-code-input"));
		locators.add(By.cssSelector("#mobile-phone-area-code-input"));
		locators.add(By.xpath("//input[@id='mobile-phone-area-code-input']"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}	



	/**
	 * Fetch an instance of org.openqa.selenium.WebElement representing the prefix  
	 * portion of the mobile phone field.
	 * <p>
	 * It is a required field.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getFieldPrefix() { 

		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.id("mobile-phone-prefix-number-input"));
		locators.add(By.cssSelector("#mobile-phone-prefix-number-input"));
		locators.add(By.xpath("//input[@id='mobile-phone-prefix-number-input']"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}		



	/**
	 * Fetch an instance of org.openqa.selenium.WebElement representing the line number   
	 * portion of the mobile phone field (last 4 digits).
	 * <p>
	 * It is a required field.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getFieldLineNumber() { 

		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.id("mobile-phone-line-number-input"));
		locators.add(By.cssSelector("#mobile-phone-line-number-input"));
		locators.add(By.xpath("//input[@id='mobile-phone-line-number-input']"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}		



	/**
	 * Fetch an instance of org.openqa.selenium.WebElement representing  
	 * street address line 1 of 2.   
	 * <p>
	 * It is a required field.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getFieldAddressLine1() { 

		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.id("input-home-address-street1"));
		locators.add(By.cssSelector("#input-home-address-street1"));
		locators.add(By.xpath("//input[@id='input-home-address-street1']"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}		




	/**
	 * Fetch an instance of org.openqa.selenium.WebElement representing  
	 * street address line 2 of 2.   
	 * <p>
	 * It is a required field.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getFieldAddressLine2() { 

		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.id("input-home-address-street2"));
		locators.add(By.cssSelector("#input-home-address-street2"));
		locators.add(By.xpath("//input[@id='input-home-address-street2']"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}	



	/**
	 * Fetch an instance of org.openqa.selenium.WebElement representing City. 
	 * <p>
	 * It is a required field.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getFieldCity() { 

		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.id("input-home-address-city"));
		locators.add(By.cssSelector("#input-home-address-city"));
		locators.add(By.xpath("//input[@id='input-home-address-city']"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}	



	/**
	 * Fetch an instance of org.openqa.selenium.WebElement representing State. 
	 * <p>
	 * It is a required field.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getFieldState() { 

		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.id("input-home-address-state"));
		locators.add(By.cssSelector("#input-home-address-state"));
		locators.add(By.xpath("//input[@id='input-home-address-state']"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}		



	/**
	 * Fetch an instance of org.openqa.selenium.WebElement representing ZIP. 
	 * <p>
	 * It is a required field.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getFieldZIP() { 

		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.id("input-home-address-zip"));
		locators.add(By.cssSelector("#input-home-address-zip"));
		locators.add(By.xpath("//input[@id='input-home-address-zip']"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}		



	/**
	 * Fetch an instance of org.openqa.selenium.WebElement representing the checkbox 
	 * for acceptance of marketing communications. 
	 * <p>
	 * It is a required field.
	 * <p>
	 * This has been problematic so we're wrapping it.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	private WebElement getCheckboxMarComm() { 

		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.cssSelector("span.pg-checkbox-mark.primary"));
		locators.add(By.cssSelector(".pg-checkbox-mark"));
		locators.add(By.xpath("//label/span"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}			



	/**
	 * Fetch an instance of org.openqa.selenium.WebElement representing the 
	 * primary button at bottom: Continue to bank info. Not activated by 
	 * default.  
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getButtonContinue_to_bank_info() { 

		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.cssSelector("button.pg-button.positive"));
		locators.add(By.cssSelector(".positive"));
		locators.add(By.xpath("//pg-app-layout-footer/div/div/button"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}		



	/**
	 * Fetch an instance of org.openqa.selenium.WebElement by which 
	 * the email field's hint text is displayed.  
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getElementForHintTextEmail() { 

		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.cssSelector(".pg-field-label-float .pg-field-hint"));
		locators.add(By.cssSelector(".is-hovered .pg-field-error:nth-child(2)"));
		locators.add(By.cssSelector("span.pg-field-hint"));
		locators.add(By.cssSelector("span.pg-field-error"));
		locators.add(By.xpath("//pg-email-address[@id='email']/div/pg-field/div[2]/span"));
		locators.add(By.xpath(".pg-field-error:nth-child(8)"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}			


	public WebElement getButtonExit() { 

		String MethodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.cssSelector(".ng-star-inserted > #exit-button"));
		locators.add(By.cssSelector("css=div.exit-button-wrapper.ng-star-inserted > #exit-button"));
		locators.add(By.xpath("(//button[@id='exit-button'])[2]"));
		locators.add(By.xpath("//div[2]/div[3]/button"));

		return WebElementGetter.fetchThisElement(MethodNameLocal, driverIn, locators);
	}


	/**
	 * Pass in valid contact info as an array of Strings: email, phone, address.
	 * <p>
	 * Happy path assumptions:
	 * <ul>
	 * <li>A 10-digit pure numeric phone.</li>
	 * <li>A 5-digit pure numeric postal code.</li>
	 * <li>Yes to marketing emails; the checkbox will get checked.</li>
	 * </ul>
	 * <p>
	 * The page under test supports a 2-line address. If you don't need the 2nd line, pass in a null like this:
	 * <p>
	 * String[] validUT = {"000@test.com", "8014464357", "1600 Towne Center Dr", null, "South Jordan", "UT", "84095"};
	 * <p>
	 * @param validContactInfo - An array of Strings representing a valid, legal address.
	 */
	public void happyPathPopulate(String[] validContactInfo) { 

		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();

		if(driverIn.findElement(By.cssSelector("#email-input")).isEnabled()){
			getFieldEmail_address().sendKeys(validContactInfo[0]);
		} else {
			getFieldAreaCode().sendKeys(validContactInfo[1].substring(0, 3));

			getFieldPrefix().sendKeys(validContactInfo[1].substring(3, 6));

			getFieldLineNumber().sendKeys(validContactInfo[1].substring(6, 10));
		}
		try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		getFieldAddressLine1().sendKeys(validContactInfo[2]);

		try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		if(!(null==validContactInfo[3])) {
			getFieldAddressLine2().sendKeys(validContactInfo[3]);
		} else { 
			System.out.println(methodNameLocal + ":\t  address line two skipped intentionally");
		}

		getFieldCity().sendKeys(validContactInfo[4]);
		try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		getFieldState().sendKeys(validContactInfo[5]);
		try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		getFieldZIP().sendKeys(validContactInfo[6]);
		try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// scroll down after ZIP field else false-fail at the checkbox
		// in case of Firefox
		JavascriptExecutor jsEx = (JavascriptExecutor) driverIn;
		jsEx.executeScript("window.scrollBy(0,800)");
		jsEx = null;

		acceptSpam();

		System.out.println(methodNameLocal + ":\t  end happy path populate");

	}
	
	
	public void acceptSpam() { 
		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		FieldValidationUtilities.sleepSecond(methodNameLocal);
		FieldValidationUtilities.sleepSecond(methodNameLocal);
		getCheckboxMarComm().click();
		FieldValidationUtilities.sleepSecond(methodNameLocal);
		FieldValidationUtilities.sleepSecond(methodNameLocal);
	}

}
