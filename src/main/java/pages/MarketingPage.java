/*
 * Copyright Progressive Leasing LLC.
 */
package pages;

import java.util.HashSet;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utilities.WebElementGetter;

/**
 * Page object for "marketing screen" / Get started. 
 * <p>
 * "No credit needed, pay over time!"
 * <p>
 * There are just a few things on this page that we're responsible for, 
 * so this page object is small.
 * <p>
 * Nevertheless, this page is a stepping stone in flows we care about.
 * <p>
 * Version 1.0 2019-01-07
 * <p>
 * Version 1.1 2019-03-27 maintenance per changes to page under test
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @author <a href="mailto:christopher.rains@progleasing.com">CJ Rains</a>
 * @version 1.1 
 * @see StoreDemoLandingPageTests.java
 */
public class MarketingPage {
	
	private WebDriver driverIn;
	
	public MarketingPage(WebDriver driverIn) { 
		this.driverIn = driverIn;
	}
	
	/**
	 * Fetch an instance of org.openqa.selenium.WebElement representing the Get started button.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getButtonGet_started() { 
		
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();
		
		Set<By> locators = new HashSet<By>();
		locators.add(By.cssSelector(".get-started-button > .pg-button"));
		locators.add(By.xpath("//button[@type='cta']"));	
		locators.add(By.xpath("//div[2]/div/div[3]/button"));
		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}

	/**
	 * Fetch an instance of org.openqa.selenium.WebElement representing the Exit button.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getButtonExit() { 
		
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();
		
		Set<By> locators = new HashSet<By>();
		locators.add(By.cssSelector("#desktop-header #exit-button"));
		locators.add(By.xpath("(//button[@id='exit-button'])[2]"));
		locators.add(By.xpath("//app-welcome/div/div[4]/button"));
		
		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}	
	

	/**
	 * Fetch an instance of org.openqa.selenium.WebElement representing the Resume button.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getButtonResume() { 
		
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();
		
		Set<By> locators = new HashSet<By>();
		locators.add(By.cssSelector(".resume-button > .neutral"));
		locators.add(By.xpath("//button[@type='neutral']"));
		locators.add(By.xpath("//app-welcome/div/div[3]/button"));
		
		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}	
	
}
