/*
 * Copyright Progressive Leasing LLC.
 */
package pages;

import java.util.HashSet;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import common.FieldValidationUtilities;
import utilities.WebElementGetter;

/**
 * Test class for page object for the basic info page. Enter name and date and SSN...
 * <p>
 * Path to this page: 
 * <p>
 * Marketing page --> Get started | Request code (complete & submit) | Enter code
 * <p>
 * Version 1.0 2019-01-08
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @author <a href="mailto:christopher.rains@progleasing.com">CJ Rains</a>
 * @see RequestCodePageTests.java
 */
public class BasicInfoPage {

	private WebDriver driverIn;

	public BasicInfoPage(WebDriver driverIn) { 
		this.driverIn = driverIn;
	}


	/**
	 * Fetch a WebElement representing the Exit button, upper right.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getButtonExit() { 

		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.xpath("//button[@id='exit-button'])[2]"));
		locators.add(By.cssSelector(".ng-star-inserted > #exit-button"));
		locators.add(By.cssSelector("div.exit-button-wrapper.ng-star-inserted > #exit-button"));
		locators.add(By.xpath("//div[2]/div[3]/button"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}		


	/**
	 * Fetch a WebElement representing the First name field.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getFieldNameFirst() { 

		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<>();
		locators.add(By.id("first-name-input"));
		locators.add(By.cssSelector("#first-name-input"));
		locators.add(By.xpath("//input[@id='first-name-input']"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}		


	/**
	 * Fetch a WebElement representing the First name field.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getFieldNameLast() { 

		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.id("last-name-input"));
		locators.add(By.cssSelector("#last-name-input"));
		locators.add(By.xpath("//input[@id='last-name-input']"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}		


	/**
	 * Fetch a WebElement representing the two-character month portion of the Date of birth.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getFieldDate_MM() { 

		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.id("dob-month-input"));
		locators.add(By.cssSelector("#dob-month-input"));
		locators.add(By.xpath("//input[@id='dob-month-input']"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}


	/**
	 * Fetch a WebElement representing the two-character day portion of the Date of birth.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getFieldDate_DD() { 

		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.id("dob-day-input"));
		locators.add(By.cssSelector("#dob-day-input"));
		locators.add(By.xpath("//input[@id='dob-day-input']"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}


	/**
	 * Fetch a WebElement representing the four-character year portion of the Date of birth.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */	
	public WebElement getFieldDate_YYYY() { 

		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.id("dob-year-input"));
		locators.add(By.cssSelector("#dob-year-input"));
		locators.add(By.xpath("//input[@id='dob-year-input']"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}


	/**
	 * Fetch a WebElement representing the Driver license field.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getFieldDriverLicense() { 

		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.id("driver-license-input"));
		locators.add(By.cssSelector("#driver-license-input"));
		locators.add(By.xpath("//input[@id='driver-license-input']"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}	


	/**
	 * Fetch a WebElement representing the Continue to contact info button.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getButtonContinue() { 

		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.cssSelector("button.pg-button.positive"));
		locators.add(By.xpath("//pg-app-layout-footer/div/div/button"));
		locators.add(By.xpath(".//*[normalize-space(text()) and normalize-space(.)='Wyoming'])[1]/following::button[1]"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}


	/**
	 * Fetch a WebElement representing the three-character (1st position) portion of the SSN or ITIN field.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getFieldSSN1_area() { 

		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.id("ssn-area-input"));
		locators.add(By.cssSelector("#ssn-area-input"));
		locators.add(By.xpath("//input[@id='ssn-area-input']"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}


	/**
	 * Fetch a WebElement representing the two-character (2nd position) portion of the SSN or ITIN field.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getFieldSSN2_group() { 

		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.id("ssn-group-input"));
		locators.add(By.cssSelector("#ssn-group-input"));
		locators.add(By.xpath("//input[@id='ssn-group-input']"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}


	/**
	 * Fetch a WebElement representing the four-character (3rd position) portion of the SSN or ITIN field.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getFieldSSN3_serial() { 

		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.id("ssn-serial-input"));
		locators.add(By.cssSelector("#ssn-serial-input"));
		locators.add(By.xpath("//input[@id='ssn-serial-input']"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}


	/**
	 * Fetch WebElement instance representing the green check mark that appears 
	 * in the SSN / ITIN field during happy-path scenario.
	 * <p>
	 * The check mark appears after a legal value is entered and after click-out 
	 * or tab-out.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getSSNHappyCheckbox() { 

		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.cssSelector("#ssn .material-icons:nth-child(2)"));
		locators.add(By.xpath("//pg-field/div/div/div[3]/i[2]"));
		locators.add(By.xpath("//pg-ssn[@id='ssn']/div/pg-field/div/div/div[3]/i[2]"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}


	/**
	 * Fetch WebElement instance representing the green check mark that appears 
	 * in the Date of birth field during happy-path scenario.
	 * <p>
	 * The check mark appears after a legal value is entered and after click-out 
	 * or tab-out.
	 * <p>
	 * Example: enter 8 digits such as 08121991 for August 12, 1991.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getDOBHappyCheckbox() { 

		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.cssSelector("#dob .material-icons:nth-child(2)"));
		locators.add(By.xpath("//i[2]"));
		locators.add(By.xpath("//pg-date-of-birth[@id='dob']/div/pg-field-wrapper/div/div/div[3]/i[2]"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}


	/**
	 * Fetch WebElement instance representing the message displayed below the 
	 * SSN / ITIN field during happy-path scenario.
	 * <p>
	 * "This helps us verify your ..."
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getSSNHappyMessage() { 
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.cssSelector(".valid-editable .pg-field-hint"));
		locators.add(By.cssSelector("span.pg-field-hint"));
		locators.add(By.xpath("//pg-ssn/div/pg-field/div[2]/span"));
		locators.add(By.xpath("//pg-ssn[@id='ssn']/div/pg-field/div[2]/span"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}


	/**
	 * Fetch WebElement instance representing the message displayed below the 
	 * SSN / ITIN field during negative scenario.
	 * <p>
	 * "Please add a valid ..."
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getSSNPrimaryErrorMessage() { 
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.cssSelector(".pg-field-label-float .pg-field-error:nth-child(4)"));
		locators.add(By.xpath("//pg-ssn[@id='ssn']/div/pg-field/div[2]/span[4]"));
		locators.add(By.xpath("//pg-ssn/div/pg-field/div[2]/span[4]"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}


	/**
	 * Fetch WebElement instance representing the message displayed below the 
	 * date of birth field during negative scenario.
	 * <p>
	 * "Your date of birth is required ..."
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getDOBPrimaryErrorMessage() { 
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.cssSelector(".is-hovered .pg-field-error:nth-child(1)"));
		locators.add(By.xpath("//pg-date-of-birth[@id='dob']/div/pg-field-wrapper/div[2]/span"));
		locators.add(By.xpath("//pg-field-wrapper/div[2]/span"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}


	/**
	 * Fetch WebElement instance representing the message displayed below the 
	 * date of birth field during negative scenario involving invalid day.
	 * <p>
	 * "The day you entered is invald."
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getDOBErrorMessageDD() { 
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.cssSelector(".is-hovered .pg-field-error:nth-child(3)"));
		locators.add(By.xpath("//pg-date-of-birth[@id='dob']/div/pg-field-wrapper/div[2]/span[3]"));
		locators.add(By.xpath("//pg-field-wrapper/div[2]/span[3]"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}


	/**
	 * Fetch WebElement instance representing the message displayed below the 
	 * date of birth field during negative scenario involving invalid month.
	 * <p>
	 * "The month you entered is invald."
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getDOBErrorMessageMM() { 
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.cssSelector(".is-hovered .pg-field-error:nth-child(2)"));
		locators.add(By.xpath("//pg-date-of-birth[@id='dob']/div/pg-field-wrapper/div[2]/span[2]"));
		locators.add(By.xpath("//pg-field-wrapper/div[2]/span[2]"));
		locators.add(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='You must be at least 18 years old to apply for a lease.'])[1]/preceding::span[2]"));
		

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}


	/**
	 * Fetch WebElement instance representing the message displayed below the 
	 * date of birth field during negative scenario involving invalid year.
	 * <p>
	 * "The year you entered is invalid."
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getDOBErrorMessageYYYY() { 
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.cssSelector(".pg-field-error:nth-child(6)"));
		locators.add(By.xpath("//pg-date-of-birth[@id='dob']/div/pg-field-wrapper/div[2]/span[6]"));
		locators.add(By.xpath("//span[6]"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}


	/**
	 * Fetch WebElement instance representing the message displayed below the 
	 * date of birth field during negative scenario involving an applicant who 
	 * is more than 100 years old (disallowed).
	 * <p>
	 * "You must be under 100 ..."
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getDOBErrorMessageYYYYCenturyOld() { 
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.cssSelector(".pg-field-error:nth-child(5)"));
		locators.add(By.xpath("//pg-date-of-birth[@id='dob']/div/pg-field-wrapper/div[2]/span[5]"));
		locators.add(By.xpath("//span[5]"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}


	/**
	 * Fetch WebElement instance representing the message displayed below the 
	 * date of birth field during negative scenario involving an applicant who 
	 * is <= 18 years old (disallowed).
	 * <p>
	 * "You must be at least 100 ..."
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getDOBErrorMessageYYYYUnderage() { 
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.cssSelector(".pg-field-error:nth-child(4)"));
		locators.add(By.xpath("//pg-date-of-birth[@id='dob']/div/pg-field-wrapper/div[2]/span[4]"));
		locators.add(By.xpath("//span[4]"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}


	/**
	 * Fetch WebElement instance representing the Issuing State field.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getFieldIssuingState() { 
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		Set<By> locators = new HashSet<By>();
		locators.add(By.id("issuing-state-input"));
		locators.add(By.cssSelector("#issuing-state-input"));
		locators.add(By.xpath("//input[@id='issuing-state-input']"));

		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}


	/**
	 * Optional helper method clears The SSN or ITIN field.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public void clearSSN() { 
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();
		getFieldSSN1_area().clear();;
		FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);
		getFieldSSN2_group().clear();
		FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);
		getFieldSSN3_serial().clear();
		FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);
	}


	/**
	 * Optional helper method: enter a full name.
	 * <p>
	 * @param first
	 * @param last
	 */
	public void enterName(String first, String last) {

		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		getFieldNameFirst().clear();
		getFieldNameFirst().sendKeys(first);
		FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);

		getFieldNameLast().clear();
		getFieldNameLast().sendKeys(last);
		FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);
	}


	/**
	 * Optional helper method safely enters an SSN or ITIN in its three parts.
	 * <p>
	 * area - first of three parts; three characters in length<BR>
	 * group - second of three parts; two characters in length<BR>
	 * serial - third of three parts; four characters in length
	 * <p>
	 * @param area 
	 * @param group
	 * @param serial
	 */
	public void enterSSN(String area, String group, String serial) { 

		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		WebElement firstThree = getFieldSSN1_area();
		firstThree.clear();
		firstThree.sendKeys(area);
		FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);

		WebElement middleTwo = getFieldSSN2_group();
		middleTwo.clear();
		middleTwo.sendKeys(group);
		FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);

		WebElement lastFour = getFieldSSN3_serial();
		lastFour.clear();
		lastFour.sendKeys(serial);
		FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);
	}


	/**
	 * Optional helper method: enter a legal date of birth.
	 * <p>
	 * @param twoDigitMonth
	 * @param twoDigitDay
	 * @param fourDigitYear
	 */
	public void enterDate(String twoDigitMonth, String twoDigitDay, String fourDigitYear) {

		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		WebElement month = getFieldDate_MM();
		month.clear();
		month.sendKeys(twoDigitMonth);
		FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);

		WebElement day = getFieldDate_DD();
		day.clear();
		day.sendKeys(twoDigitDay);
		FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);

		WebElement year = getFieldDate_YYYY();
		year.clear();
		year.sendKeys(fourDigitYear);
		FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);
	}


	/**
	 * Optional helper method safely enters a value into the DL or Govt ID field.
	 * <p>
	 * There is no validation. The value you pass in is assumed valid.
	 * <p>
	 * @param license - a driver license number or a passport number, etc.
	 */
	public void enterDriverLicense(String license) { 

		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		getFieldDriverLicense().clear();
		FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);

		getFieldDriverLicense().sendKeys(license);
		FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);
	}

	
	/**
	 * Optional helper method enters a valid two-letter American state, such as AK or FL.
	 * <p>
	 * @param ST - A real USPS-approved two-letter state.
	 */
	public void selectTwoLetterState(String ST) { 

		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

		getFieldIssuingState().clear();

		FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);

		getFieldIssuingState().sendKeys(ST);
		
		FieldValidationUtilities.sleepSecond(methodNameLocal);
	}
}
