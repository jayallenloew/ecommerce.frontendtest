	/*
 * Copyright Progressive Leasing LLC.
 */
package pages;

import java.util.HashSet;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import common.FieldValidationUtilities;
import common.ProgressiveLeasingConstants;
import utilities.WebElementGetter;

/**
 * Page object for the bank info page.
 * <p>
 * Version 1.0 2019-03-08
 * <p>
 * Version 1.1 2019-03-18 add expected messages
 * <p>
 * Version 1.2 2019-03-22 add happy math populate methods
 * <p>
 * TO DO: We don't yet have a test class for this page object. But we 
 * don't need one urgently, since most or all of the page is covered 
 * in our end-to-end tests.
 * <p>
 * @author <a href="mailto:Christopher.Rains@progleasing.com">CJ Rains</a>
// * @see BankInfoPageTests.java
 */
public class BankInfoPage {

    private WebDriver driverIn;

    public BankInfoPage(WebDriver driverIn) {
        this.driverIn = driverIn;
    }
    
    public static final String getMsgErrorRoutingInvalid() { 
    	return "Your bank routing number is invalid. Please double check and try again.";
    }

    public static final String getMsgErrorRoutingNull() { 
    	return "Your bank routing number is required to process the application.";
    }

    public static final String getMsgErrorCheckingNull() { 
    	return "Your checking account number is required to process the application.";
    }

    public static final String getMsgErrorIncomeNull() { 
    	return "Your monthly income is required to process the application.";
    }

    /**
     * Fetch a WebElement representing the Exit button, upper right.
     * <p>
     * @return An instance of org.openqa.selenium.WebElement
     */
    public WebElement getButtonExit() {

        String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

        Set<By> locators = new HashSet<By>();
        locators.add(By.xpath("//button[@id='exit-button'])[2]"));
        locators.add(By.cssSelector(".ng-star-inserted > #exit-button"));
        locators.add(By.cssSelector("div.exit-button-wrapper.ng-star-inserted > #exit-button"));
        locators.add(By.xpath("//div[2]/div[3]/button"));

        return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
    }


    /**
     * Fetch a WebElement representing the Routing Number field.
     * <p>
     * @return An instance of org.openqa.selenium.WebElement
     */
    public WebElement getFieldRoutingNumber() {

        String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

        Set<By> locators = new HashSet<By>();
        locators.add(By.id("input-routing-number"));
        locators.add(By.cssSelector("#input-routing-number"));
        locators.add(By.xpath("//input[@id='input-routing-number']"));

        return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
    }


    /**
     * Fetch a WebElement representing the Account Number field.
     * <p>
     * @return An instance of org.openqa.selenium.WebElement
     */
    public WebElement getFieldAccountNumber() {

        String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

        Set<By> locators = new HashSet<By>();
        locators.add(By.id("input-account-number"));
        locators.add(By.cssSelector("#input-account-number"));
        locators.add(By.xpath("//input[@id='input-account-number']"));

        return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
    }


    /**
     * Fetch a WebElement representing the Monthly Income field.
     * <p>
     * @return An instance of org.openqa.selenium.WebElement
     */
    public WebElement getFieldMonthlyIncome() {

        String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

        Set<By> locators = new HashSet<By>();
        locators.add(By.id("input-monthly-income"));
        locators.add(By.cssSelector("#input-monthly-income"));
        locators.add(By.xpath("//input[@id='input-monthly-income']"));

        return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
    }


    /**
     * Fetch an instance of org.openqa.selenium.WebElement representing the
     * primary button at bottom: Continue to apply. Not activated by
     * default.
     * <p>
     * @return An instance of org.openqa.selenium.WebElement
     */
    public WebElement getButtonContinue_to_apply() {

        String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("button.pg-button.positive"));
        locators.add(By.cssSelector(".positive"));
        locators.add(By.xpath("//pg-app-layout-footer/div/div/button"));

        return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
    }


    /**
     * Optional helper method: enter a routing number.
     * <p>
     * @param routingNumber
     */
    public void enterRoutingNumber(String routingNumber) {

        String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

        getFieldRoutingNumber().clear();
        getFieldRoutingNumber().sendKeys(routingNumber);
        FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);
    }


    /**
     * Optional helper method: enter an account number.
     * <p>
     * @param accountNumber
     */
    public void enterAccountNumber(String accountNumber) {

        String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

        getFieldAccountNumber().clear();
        getFieldAccountNumber().sendKeys(accountNumber);
        FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);
    }


    /**
     * Optional helper method: enter monthly income.
     * <p>
     * @param income
     */
    public void enterMonthlyIncome(String income) {

        String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();

        getFieldMonthlyIncome().clear();
        getFieldMonthlyIncome().sendKeys(income);
        FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);
    }
    
    
    /**
     * This first of two happy path helper methods assumes reasonable 
     * test values.
     * <p>
     * For testing purposes, the only requirement on this screen is 
     * that the routing number me 000000000 (nine zeros).
     */
    public void happyPathPopulate() { 
        happyPathPopulate("123456789","2500");
    }
    
    
    /**
     * This second of two happy path helper methods allows the calling 
     * test to specify an account number and a monthly income.
     * <p>
     * For testing purposes, the only requirement on this screen is 
     * that the routing number me 000000000 (nine zeros).
     */
    public void happyPathPopulate(String accountNumber, String monthlyIncome) { 
    	
        String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();
        
        getFieldRoutingNumber().sendKeys(ProgressiveLeasingConstants.ROUTING_NUMBER_FOR_TESTING);
        FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);

        getFieldAccountNumber().sendKeys(accountNumber);
        FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);

        getFieldMonthlyIncome().sendKeys(monthlyIncome);
        FieldValidationUtilities.sleepSecond(methodNameLocal);
    }

}
